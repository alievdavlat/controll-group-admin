/* eslint-disable jsx-a11y/anchor-is-valid */
import {FC} from 'react'
import {Link} from 'react-router-dom'
import {useLayout} from '../../../core'
import { useLang } from '../../../../i18n/Metronici18n'

const MegaMenu: FC = () => {

  const locale = useLang()
  
  const {setLayoutType, setToolbarType} = useLayout()
  return (
    <div className='row'>
      {/* begin:Col */}
      <div className='col-lg-6'>
        {/* begin:Row */}
        <div className='row'>
          {/* begin:Col */}
          <div className='col-lg-6 mb-3'>
            {/* begin:Heading */}
            <h4 className='fs-6 fs-lg-4 text-gray-800 fw-bold mt-3 mb-3 ms-4'>{locale == 'uz' ? 'Макеты' : 'Tartiblar'}</h4>
            {/* end:Heading */}
            {/* begin:Menu item */}
            <div className='menu-item p-0 m-0'>
              {/* begin:Menu link */}
              <a onClick={() => setLayoutType('light-sidebar')} className='menu-link'>
                <span className='menu-bullet'>
                  <span className='bullet bullet-dot bg-gray-300i h-6px w-6px'></span>
                </span>
                <span className='menu-title'>{locale === 'uz' ? 'Oq Sidebar' : 'светлая боковая панель'}</span>
              </a>
              {/* end:Menu link */}
            </div>
            {/* end:Menu item */}
            {/* begin:Menu item */}
            <div className='menu-item p-0 m-0'>
              {/* begin:Menu link */}
              <a onClick={() => setLayoutType('dark-sidebar')} className='menu-link'>
                <span className='menu-bullet'>
                  <span className='bullet bullet-dot bg-gray-300i h-6px w-6px'></span>
                </span>
                <span className='menu-title'>{locale === 'uz' ? 'Qora Sidebar' :"Темная боковая панель"}</span>
              </a>
              {/* end:Menu link */}
            </div>
            {/* end:Menu item */}
            {/* begin:Menu item */}
            <div className='menu-item p-0 m-0'>
              {/* begin:Menu link */}
              <a onClick={() => setLayoutType('light-header')} className='menu-link'>
                <span className='menu-bullet'>
                  <span className='bullet bullet-dot bg-gray-300i h-6px w-6px'></span>
                </span>
                <span className='menu-title'>{locale === 'uz' ?  'OQ Header' : 'Cветлая вершина'}</span>
              </a>
              {/* end:Menu link */}
            </div>
            {/* end:Menu item */}
            {/* begin:Menu item */}
            <div className='menu-item p-0 m-0'>
              {/* begin:Menu link */}
              <a onClick={() => setLayoutType('dark-header')} className='menu-link'>
                <span className='menu-bullet'>
                  <span className='bullet bullet-dot bg-gray-300i h-6px w-6px'></span>
                </span>
                <span className='menu-title'>{locale == 'uz' ? 'Header' : 'Cветлая'}</span>
              </a>
              {/* end:Menu link */}
            </div>
            {/* end:Menu item */}
          </div>
          {/* end:Col */}
         


        </div>
        {/* end:Row */}
        {/* begin:Layout Builder */}
        <div className='d-flex flex-stack flex-wrap flex-lg-nowrap gap-2 mb-5 mb-lg-0 mx-lg-5'>
          <div className='d-flex flex-column me-5'>
            <div className='fs-6 fw-bold text-gray-800'>{locale === 'uz' ? 'Ko`rinishni O`zgartirish' :'Изменить внешний вид'}</div>
            <div className='fs-7 fw-semibold text-muted'></div>
          </div>
          <Link to='/builder' className='btn btn-sm btn-primary fw-bold'>
            {locale === 'uz' ? "Ko'rinishni O'zgartirib ko'ring" : 'Попробуйте Изменить внешний вид'}
          </Link>
        </div>
        {/* end:Layout Builder */}
      </div>
     
    </div>
  )
}

export {MegaMenu}
