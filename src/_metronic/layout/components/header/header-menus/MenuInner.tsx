import {useIntl} from 'react-intl'
import {MenuItem} from './MenuItem'
import {MenuInnerWithSub} from './MenuInnerWithSub'
import {MegaMenu} from './MegaMenu'
import { SidebarMenuItemWithSub } from '../../sidebar/sidebar-menu/SidebarMenuItemWithSub'
import { SidebarMenuItem } from '../../sidebar/sidebar-menu/SidebarMenuItem'

export function MenuInner() {

  return (
    <>
      <MenuItem title={'Dashboard'} to='/dashboard' />
      <MenuItem title='Layout Builder' to='/builder' />
     

      
      <MenuInnerWithSub
        isMega={true}
        title='Mega menu'
        to='/mega-menu'
        menuPlacement='bottom-start'
        menuTrigger='click'
      >
        <MegaMenu />
      </MenuInnerWithSub>
    </>
  )
}
