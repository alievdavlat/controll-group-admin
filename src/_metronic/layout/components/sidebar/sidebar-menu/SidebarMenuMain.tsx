import {SidebarMenuItemWithSub} from './SidebarMenuItemWithSub'
import {SidebarMenuItem} from './SidebarMenuItem'
import { useLang } from '../../../../i18n/Metronici18n'

const SidebarMenuMain = () => {
  const locale = useLang()
  return (
    <>
      
      <SidebarMenuItem to='/builder' icon='element-11' title='Layout Builder' fontIcon='bi-layers' />
      
      <div className='menu-item'>
        <div className='menu-content pt-8 pb-2'>
          <span className='menu-section text-muted text-uppercase fs-8 ls-1'>{locale === 'uz' ?  'Yaratish' :'Создавать'}</span>
        </div>
      </div>

      <SidebarMenuItemWithSub
        to='/crafted/pages'
        title={locale == 'uz'  ? 'Moslashtirish' :'Кастомизация'}
        fontIcon='bi-archive'
        icon='element-plus'
      >
          <SidebarMenuItem to='/dashboard/hero' title='Hero' hasBullet={true} />
          <SidebarMenuItem to='/dashboard/video' title={locale === 'uz' ? 'Video vidget' :'Виджет видео'} hasBullet={true} />
          <SidebarMenuItem to='/dashboard/about' title={locale === 'uz' ? 'Biz Xaqimizda' : 'О нас'} hasBullet={true} />
          <SidebarMenuItem to='/dashboard/contact/edit' title={locale == 'uz' ? "Aloqa Formi " : 'Форма обратной связи'} hasBullet={true} />
          <SidebarMenuItem to='/dashboard/main-data' title={locale == 'uz' ? "Asosiy ma'lumotlar" : 'Oсновные данные'} hasBullet={true} />

      </SidebarMenuItemWithSub>


      <SidebarMenuItemWithSub
        to='create'
        title={locale === 'uz' ?  'Yaratish' :'Создавать'}
        icon='element-3'
        fontIcon='bi-person'
      >
        <SidebarMenuItem to='/dashboard/advantages' title={locale === 'uz'  ? 'Ustunliklar' :'преимущества'} hasBullet={true} />
        <SidebarMenuItem to='/dashboard/reviews' title={locale == 'uz' ? 'Sharhlar' :'отзывы'} hasBullet={true} />
        <SidebarMenuItem to='/dashboard/faq' title={locale == 'uz' ?  'Savol va Javoblar':'вопросы  и ответы'} hasBullet={true} />
        <SidebarMenuItem to='/dashboard/services' title={locale === 'uz' ?  "Xizmatlar":"услуги"} hasBullet={true} />
        <SidebarMenuItem to='/dashboard/stocks' title={locale === 'uz' ?  "Aksiyalar":"акции"} hasBullet={true} />
        <SidebarMenuItem to='/dashboard/projects' title={locale === 'uz' ?  "Proyekt":"проект"} hasBullet={true} />
      </SidebarMenuItemWithSub>
    
      

      <SidebarMenuItemWithSub
        to='/crafted/accounts'
        title={locale === 'uz' ?  'O`zgartirish' :'изменять'}
        icon='element-7'
        fontIcon='bi-person'
      >
        <SidebarMenuItem to='/dashboard/advantages-list' title={locale === 'uz' ? 'Ustunliklar' : 'преимущества'} hasBullet={true} />
        <SidebarMenuItem to='/dashboard/reviews-list' title={locale == 'uz' ? 'Izohlar' :' отзывы'} hasBullet={true} />
        <SidebarMenuItem to='/dashboard/faq-list' title={locale === 'uz' ? 'Savol va Javoblar' : 'вопросы  и ответы '} hasBullet={true} />
        <SidebarMenuItem to='/dashboard/services-list' title={locale === 'uz' ? 'Xizmatlar' : 'услуги'}  hasBullet={true} />
        <SidebarMenuItem to='/dashboard/stocks-list' title={locale === 'uz' ? 'Aksiyalar' : 'акции'}  hasBullet={true} />
        <SidebarMenuItem to='/dashboard/projects-list' title={locale === 'uz' ? 'Proyektlar' : 'проекты'}  hasBullet={true} />
      </SidebarMenuItemWithSub>

      <SidebarMenuItemWithSub
        to='/crafted/accounts'
        title={locale === 'uz' ? 'Sahifalar'  :'Страницы'}
        icon='element-3'
        fontIcon='bi-person'
      >
        <SidebarMenuItem to='/crafted/account/overview' title={locale == 'uz'  ? 'Profile' :'Обзор'} hasBullet={true} />
        <SidebarMenuItem to='/crafted/account/settings' title={locale === 'uz' ? 'Sozlamalar':'Настройки'} hasBullet={true} />
        <SidebarMenuItem to='/error/404' title='Error 404' hasBullet={true} />
        <SidebarMenuItem to='/error/500' title='Error 500' hasBullet={true} />
      </SidebarMenuItemWithSub>

      <div className='menu-item'>
        <div className='menu-content pt-8 pb-2'>
          <span className='menu-section text-muted text-uppercase fs-8 ls-1'>{locale === 'uz' ? 'Boshqarish':'Управлять'}</span>
        </div>
      </div>

    
      <SidebarMenuItem
        to='/dashboard/contact-list'
        icon='abstract-28'
        title={locale === 'uz' ? 'Foydalanuvchilar' :'Управление пользователями'}
        fontIcon='bi-layers'
      />
    
    </>
  )
}

export {SidebarMenuMain}
