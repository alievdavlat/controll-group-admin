import {FC, useEffect, useState} from 'react'
import {
  toAbsoluteUrl,
  useIllustrationsPath,
} from '../../../helpers'
import { useGetAllNotificationsQuery, useUpdateNotificationsStatusMutation } from '../../../../redux/features/notifications/notificationsApi'
import socketIO from 'socket.io-client'
import { format } from 'timeago.js'
import { useLang } from '../../../i18n/Metronici18n'
const ENDPOINT = process.env.REACT_APP_SOCKET_URL || ''
const socketId = socketIO(ENDPOINT, {transports:['websocket']})



const HeaderNotificationsMenu: FC = () =>  {

  const locale = useLang()
  const {data, refetch} = useGetAllNotificationsQuery(undefined, {refetchOnMountOrArgChange:true})
  const [updateNotificationsStatus, { isSuccess }] = useUpdateNotificationsStatusMutation()

  

  const [audio] = useState(
    new Audio(
      'https://firebasestorage.googleapis.com/v0/b/learning-platfrom-9b485.appspot.com/o/livechat-129007.mp3?alt=media&token=c3238efc-15b2-4c62-91e3-c0cd809a1eba&_gl=1*zfx25t*_ga*MTU4NjE2MDk5Ny4xNjk2NzU0MDMw*_ga_CW55HF8NVT*MTY5ODY0MzU2MC4zMy4xLjE2OTg2NDM3NjMuNTIuMC4w'
    )
  )

  const [notificationsUndred, setNotificationsUnread] = useState<any>([])
  const [notificationsRead, setNotificationsRead] = useState<any>([])

  const playerNotificationSound  = () => {
    audio.play()
  } 

  
  const handleNoteficationStatusChange =  async (id:string) => {
    await updateNotificationsStatus(id)
  }

  useEffect(() => {
    if (data) {
      setNotificationsUnread(
          data?.notifications?.filter((item:any) => item?.status === 'unread')
        )

        setNotificationsRead(
          data?.notifications?.filter((item:any) => item?.status !== 'unread')
        )
    }

    if (isSuccess) {
        refetch()
    }

    audio.load();

  }, [isSuccess, data])

  useEffect(() => {
    socketId.on('newNotification', (data) => {
      refetch()
      playerNotificationSound()
    })
  }, [])

  
   return  (

    <div
      className='menu menu-sub menu-sub-dropdown menu-column w-350px w-lg-375px'
      data-kt-menu='true'
    >
      <div
        className='d-flex flex-column bgi-no-repeat rounded-top'
        style={{backgroundImage: `url('${toAbsoluteUrl('/media/misc/menu-header-bg.jpg')}')`}}
      >
        <h3 className='text-white fw-bold px-9 mt-10 mb-6'>
          {locale === 'uz' ? 'Bildirishnomalar' :'Уведомления'} <span className='fs-8 opacity-75 ps-3'>{notificationsUndred?.length} {locale === 'uz' ? 'hisoblar' :'отчеты'}</span>
        </h3>
          <div className='px-10 mb-5'>
          <i>{locale === 'uz' ? 'Bildirishnomalar  30 kundan keyin avtomatik ravishda o\'chiriladi.' :' уведомлений автоматически удаляется через 30 дней.'}</i>
          </div>
        <ul className='nav nav-line-tabs nav-line-tabs-2x nav-stretch fw-bold px-9'>
          <li className='nav-item'>
            <a
              className='nav-link text-white opacity-75 opacity-state-100 pb-4'
              data-bs-toggle='tab'
              href='#kt_topbar_notifications_1'
            >
              {locale === 'uz' ? 'o`qilganlar' :'просмотрена'}
            </a>
          </li>
  
          <li className='nav-item'>
            <a
              className='nav-link text-white opacity-75 opacity-state-100 pb-4 active'
              data-bs-toggle='tab'
              href='#kt_topbar_notifications_2'
            >
              
              {locale === 'uz' ? 'O`qilmaganar'  :'не просмотрена'}
            </a>
          </li>
  
        </ul>
      </div>
  
      <div className='tab-content'>
        <div className='tab-pane fade' id='kt_topbar_notifications_1' role='tabpanel'>
          <div className='scroll-y mh-325px my-5 px-8'>

              {
               notificationsRead && notificationsRead?.map(item => (
                <div className='border mb-5 px-5'>
                  <div className='d-flex align-items-center mb-5 border px-2'>

                  <h4 style={{fontSize:'0.9rem'}} className='mt-2 text-primary'>
                    {locale === 'uz' ? item?.title_uz : item?.title_ru}
                  </h4>
                  <span className='badge badge-light-success fw-bolder fs-8 px-2 py-1 ms-2'>{item?.status}</span>

                  </div>

                  <p className='text-info'>
                    {locale == 'uz' ? item?.message_uz :item?.message_ru}
                  </p>

                  <b className='mb-3'>
                    {format(item?.createdAt)}
                  </b>
                </div>
               ))
              }
  
          </div>
        </div>
  
        <div className='tab-pane fade show active' id='kt_topbar_notifications_2' role='tabpanel'>
          <div className='d-flex flex-column  positon-relative scroll-y'>
          <div className='scroll-y mh-325px my-5 px-8'>

          {
               notificationsUndred && notificationsUndred?.map(item => (
                <div className='border mb-5 px-5'>
                  <div className='d-flex align-items-center mb-5 border px-2'>

                  <h4 style={{fontSize:'0.9rem'}} className='mt-2 text-primary'>
                  {locale === 'uz' ? item?.title_uz : item?.title_ru}
                  </h4>
                  <span className='badge badge-light-danger fw-bolder fs-8 px-2 py-1 ms-2'>{item?.status}</span>

                  </div>

                  <p className='text-info'>
                  {locale == 'uz' ? item?.message_uz :item?.message_ru}
                  </p>

                  <div>
                  <b className='mb-3'>
                    {format(item?.createdAt)}
                  </b>
                  <span onClick={() => handleNoteficationStatusChange(item?._id)} className='badge badge-light-primary fw-bolder fs-8 px-2 py-1 ms-2' style={{cursor:'pointer'}} >{locale == 'uz' ? 'o`qilgan qilib belgilash' :'пометить, как прочитанное' }</span>
                  </div>
                  <br />
                </div>
               ))
              }
          </div>
  
          </div>
        </div>
  
      </div>
    </div>
  )
}

export {HeaderNotificationsMenu}
