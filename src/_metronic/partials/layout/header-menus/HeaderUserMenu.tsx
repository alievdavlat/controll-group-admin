/* eslint-disable jsx-a11y/anchor-is-valid */
import {FC} from 'react'
import {Link} from 'react-router-dom'
import {Languages} from './Languages'
import {toAbsoluteUrl} from '../../../helpers'
import { useSelector } from 'react-redux'
import { useLang } from '../../../i18n/Metronici18n'

const HeaderUserMenu: FC = () => {
  const {user} = useSelector((state:any) => state.auth)
  const locale = useLang()
  const signOut = () => {
    localStorage.clear()
    window.location.reload()
  }
  

  return (
    <div
      className='menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px'
      data-kt-menu='true'
    >
      <div className='menu-item px-3'>
        <div className='menu-content d-flex align-items-center px-3'>
          <div className='symbol symbol-50px me-5'>
            <img alt='Logo' src={user?.avatar ? user?.avatar : toAbsoluteUrl('/media/avatars/300-3.jpg')} />
          </div>

          <div className='d-flex flex-column'>
            <div className='fw-bolder d-flex align-items-center fs-5'>
              {user?.name}
              <span className='badge badge-light-success fw-bolder fs-8 px-2 py-1 ms-2'>{user?.role}</span>
            </div>
            <a href='#' className='fw-bold text-muted text-hover-primary fs-7'>
              {/* {currentUser?.email} */}
            </a>
          </div>
        </div>
      </div>

      <div className='separator my-2'></div>

      <div className='menu-item px-5'>
        <Link to={'crafted/account/'} className='menu-link px-5'>
         {locale === 'uz' ?  'Profill' :'мой профиль'}
        </Link>
      </div>

     
      

      

      <div className='separator my-2'></div>

      <Languages />

      <div className='menu-item px-5 my-1'>
        <Link to='/crafted/account/settings' className='menu-link px-5'>
          {locale === 'uz' ? 'Profill sozlamalari' : 'Настройки учетной записи'}
        </Link>
      </div>

      <div className='menu-item px-5'>
        <a  className='menu-link px-5' onClick={() => signOut()}>
          {locale === 'uz' ? 'Tizimdan chiqish' : 'выход'}
        </a>
      </div>
    </div>
  )
}

export {HeaderUserMenu}
