import {FC, ReactNode} from 'react'
import {KTCard} from '../../helpers'

type Props = {
  setisOpen: (isOpen:boolean) => void

  children:any
}

const CustomModal: FC<Props> = ({ children, setisOpen}) => {

  const theme = localStorage.getItem('kt_theme_mode_menu')

  const hanldeClose = (e) => {
    if (e.target.id === 'customModal') {
      setisOpen(false)
    }
  }
  
  return (
     <div
     id='customModal'
     onClick={hanldeClose}
      style={{
        background: 'rgba(0, 0, 0, 0.5)',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        position:'fixed',
        width:'100%',
        height:'100vh',
        zIndex:'99999',
        top:'0',
        left:'0'
      }}
    >

      <div style={{padding:'20px 30px', background:`${theme === 'light' ? 'white' : 'black'}`, borderRadius:'20px'}} >
        {children}
      </div>

    </div>
  )
}

export default CustomModal
