import {Suspense, useEffect} from 'react'
import {Outlet} from 'react-router-dom'
import {I18nProvider} from '../_metronic/i18n/i18nProvider'
import {LayoutProvider, LayoutSplashScreen} from '../_metronic/layout/core'
import {MasterInit} from '../_metronic/layout/MasterInit'
import {ThemeModeProvider} from '../_metronic/partials'
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import socketIO from 'socket.io-client'
const ENDPOINT = process.env.REACT_APP_SOCKET_URL || ''
const socketId = socketIO(ENDPOINT, {transports:['websocket']})


const App = () => {
 useEffect(() => {
    socketId.on('connection', () => {})
  }, [])
  
  
  return (
    <Suspense fallback={<LayoutSplashScreen />}>
      <I18nProvider>
        <LayoutProvider>
          <ThemeModeProvider>
              <Outlet />
              <MasterInit />
          </ThemeModeProvider>
        </LayoutProvider>
      </I18nProvider>
      <ToastContainer/>
    </Suspense>
  )
}

export {App}
