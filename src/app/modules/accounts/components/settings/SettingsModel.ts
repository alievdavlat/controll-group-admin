export interface IProfileDetails {
  avatar: string
  name: string
  number: string
}

export interface IUpdateName {
  newName: string
}

export interface IUpdateNumber {
  oldNumber: string
  newNumber: string
}



export interface INotifications {
  notifications: {
    email: boolean
    phone: boolean
  }
  billingUpdates: {
    email: boolean
    phone: boolean
  }
  newTeamMembers: {
    email: boolean
    phone: boolean
  }
  completeProjects: {
    email: boolean
    phone: boolean
  }
  newsletters: {
    email: boolean
    phone: boolean
  }
}



export const profileDetailsInitValues: IProfileDetails = {
  avatar: '/media/avatars/300-1.jpg',
  name: '',
  number:''
}

export const updateName: IUpdateName = {
  newName: '',
}

export const updateNumber: IUpdateNumber = {
oldNumber:'',
newNumber:''
}





export const notifications: INotifications = {
  notifications: {
    email: true,
    phone: true,
  },
  billingUpdates: {
    email: true,
    phone: true,
  },
  newTeamMembers: {
    email: true,
    phone: false,
  },
  completeProjects: {
    email: false,
    phone: true,
  },
  newsletters: {
    email: false,
    phone: false,
  },
}


