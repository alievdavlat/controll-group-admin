import React, {useEffect, useState} from 'react'
import {toAbsoluteUrl} from '../../../../../../_metronic/helpers'
import {IProfileDetails} from '../SettingsModel'
import {useFormik} from 'formik'
import {useSelector} from 'react-redux'
import { useUpdateAvatarMutation } from '../../../../../../redux/features/user/userApi'
import axios from 'axios'
import { useLang } from '../../../../../../_metronic/i18n/Metronici18n'
import camerIcon from '../../../../../../_metronic/assets/camera.png'
const baseUrl = process.env.REACT_APP_API_URL


const ProfileDetails: React.FC = () => {
  const {user} = useSelector((state: any) => state.auth)
  const locale = useLang()

  const [updateAvatar , {isSuccess:success, error: isError, data:avatarData}] = useUpdateAvatarMutation()
  const [avatarPicture, setAvatarPicture] = React.useState({
    avatar: '',
  })
  

  const intval = {
    avatar: user?.avatar || '/media/avatars/300-1.jpg',
    name: user?.name,
    number: user?.number,
  }

  

  const handleChangeAvatar = async (e: any) => {
    setAvatarPicture({...avatarPicture, [e.target.name]: e.target.files[0]})
    window.location.reload()

  }

  
  useEffect(() => {
    const updateAvatar = async () => {
      try {
        const formData = new FormData();
        formData.append('avatar', avatarPicture.avatar);
  
        const response = await axios.put(`${baseUrl}update-avatar`, formData, {
          headers: {
            'Content-Type': 'multipart/form-data',
            'token': JSON.parse(localStorage.getItem('token') as any),
          },
        });
  
        
  
      } catch (error) {
        console.error('Error:', error);
      }
    };
  
    if (avatarPicture.avatar) {
      updateAvatar();
    }

  }, [handleChangeAvatar]);
  

  const [data, setData] = useState<IProfileDetails>(intval)

  const [loading, setLoading] = useState(false)

  const formik = useFormik<IProfileDetails>({
    initialValues: intval,
    onSubmit: (values) => {
      setLoading(true)

      setTimeout(() => {
        setLoading(false)
      }, 1000)
    },
  })

  return (
    <div className='card mb-5 mb-xl-10'>
      <div
        className='card-header border-0 cursor-pointer'
        role='button'
        data-bs-toggle='collapse'
        data-bs-target='#kt_account_profile_details'
        aria-expanded='true'
        aria-controls='kt_account_profile_details'
      >
        <div className='card-title m-0'>
          <h3 className='fw-bolder m-0'>{locale === 'uz' ? 'Mening malumotlarim' :'Детали профиля'}</h3>
        </div>
      </div>

      <div id='kt_account_profile_details' className='collapse show'>
        <form  noValidate className='form'>
          <div className='card-body border-top p-9'>
            <div className='row mb-6'>
              <label className='col-lg-4 col-form-label fw-bold fs-6'>Avatar</label>
              <div className='col-lg-8'>
                <div
                  className='image-input image-input-outline position-relative'
                  data-kt-image-input='true'
                  style={{backgroundImage: `url(${toAbsoluteUrl('/media/avatars/blank.png')})`}}
                >
                  <div
                    className='image-input-wrapper w-125px h-125px'
                    style={{backgroundImage: `url(${toAbsoluteUrl(data.avatar)})`}}
                  ></div>


                  <div className='position-absolute w-100 h-100 top-0 left-0 d-flex justify-content-end align-items-end'>
                    
                    <img src={camerIcon} alt="camera"  style={{margin: '0px 10px 10px 10px', cursor: 'pointer', width:'20px', height:"20px"}}/>
                    <input
                      type='file'
                      name='avatar'
                      accept='image/png,image/jpg,image/jpeg,image/webp'
                      onChange={handleChangeAvatar}
                      className='w-100 h-100 position-absolute opacity-0 cursor-pointer '
                      style={{zIndex: '10'}}
                    />
                  </div>
                </div>
              </div>
            </div>

            <div className='row mb-6'>
              <label className='col-lg-4 col-form-label required fw-bold fs-6'>{locale === 'uz' ?  'ISM':'ИМЯ'}</label>

              <div className='col-lg-8'>
                <div className='row'>
                  <div className='col-lg-6 fv-row'>
                    <input
                      type='text'
                      readOnly
                      className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
                      placeholder='First name'
                      {...formik.getFieldProps('name')}
                    />
                    {formik.touched.name && formik.errors.name && (
                      <div className='fv-plugins-message-container'>
                        <div className='fv-help-block'>{formik.errors.name}</div>
                      </div>
                    )}
                  </div>
                </div>
              </div>
            </div>

            <div className='row mb-6'>
              <label className='col-lg-4 col-form-label fw-bold fs-6'>
                <span className='required'>{locale === 'uz' ? 'Telifon Raqami' : 'Номер телефона'}</span>
              </label>

              <div className='col-lg-8 fv-row'>
                <input
                  type='tel'
                  readOnly
                  className='form-control form-control-lg form-control-solid'
                  placeholder='Phone number'
                  {...formik.getFieldProps('number')}
                />
                {formik.touched.number && formik.errors.number && (
                  <div className='fv-plugins-message-container'>
                    <div className='fv-help-block'>{formik.errors.number}</div>
                  </div>
                )}
              </div>
            </div>
          </div>

        </form>
      </div>
    </div>
  )
}

export {ProfileDetails}
