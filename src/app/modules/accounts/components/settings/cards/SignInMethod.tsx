/* eslint-disable jsx-a11y/anchor-is-valid */
import React, {useState} from 'react'

import * as Yup from 'yup'
import {useFormik} from 'formik'
import {IUpdateName, IUpdateNumber, updateName, updateNumber} from '../SettingsModel'
import { useSelector } from 'react-redux'
import { useEditPhoneNumberMutation, useEditProfileMutation } from '../../../../../../redux/features/user/userApi'
import { useLang } from '../../../../../../_metronic/i18n/Metronici18n'

const nameFormValidationSchema = Yup.object().shape({
  newName: Yup.string()
    .min(3, 'Minimum 3 symbols')
    .max(50, 'Maximum 50 symbols')
    .required('new name  is required'),
})



const numberFormValidationSchema = Yup.object().shape({
  oldNumber: Yup.string()
    .min(3, 'Minimum 3 symbols')
    .max(50, 'Maximum 50 symbols')
    .required('Password is required'),
  newNumber: Yup.string()
    .min(3, 'Minimum 3 symbols')
    .max(50, 'Maximum 50 symbols')
    .required('Password is required'),
})

const SignInMethod: React.FC = () => {
  const locale = useLang()

  const { user } = useSelector((state:any) => state.auth)
  const [editProfile ] =  useEditProfileMutation()
  const [editPhoneNumber] = useEditPhoneNumberMutation()


  const [nameUpdateData, setNameUpdateData] = useState<IUpdateName>(updateName)
  const [numberUpdateData, setNumberUpdateData] = useState<IUpdateNumber>(updateNumber)

  const [showNameForm, setshowNameForm] = useState<boolean>(false)
  const [showNumberForm, setShowNumberForm] = useState<boolean>(false)

  const [loading1, setLoading1] = useState(false)

  const formik1 = useFormik<IUpdateName>({
    initialValues: {
      ...nameUpdateData,
    },
    validationSchema: nameFormValidationSchema,
    onSubmit: async (values) => {
      setLoading1(true)

      
      await editProfile({name:values.newName})


      
      setTimeout((values) => {
        setNameUpdateData(values)
        setLoading1(false)
        setshowNameForm(false)
        window.location.reload()
      }, 1000)
    },
  })

  const [loading2, setLoading2] = useState(false)

  const formik2 = useFormik<IUpdateNumber>({
    initialValues: {
      ...numberUpdateData,
    },
    validationSchema: numberFormValidationSchema,
    onSubmit: async (values) => {
      setLoading2(true)

      await editPhoneNumber({...values})
      
      setTimeout((values) => {
        setNumberUpdateData(values)
        setLoading2(false)
        setShowNumberForm(false)
        window.location.reload()
      }, 1000)
    },
  })

  return (
    <div className='card mb-5 mb-xl-10'>
      <div
        className='card-header border-0 cursor-pointer'
        role='button'
        data-bs-toggle='collapse'
        data-bs-target='#kt_account_signin_method'
      >
        <div className='card-title m-0'>
          <h3 className='fw-bolder m-0'>{locale === 'uz' ? 'Kirish usuli':'Метод входа'}</h3>
        </div>
      </div>

      <div id='kt_account_signin_method' className='collapse show'>
        <div className='card-body border-top p-9'>
          <div className='d-flex flex-wrap align-items-center'>
            <div id='kt_signin_email' className={' ' + (showNameForm && 'd-none')}>
              <div className='fs-6 fw-bolder mb-1'>{locale === 'uz' ? 'ISM':'Имя'}</div>
              <div className='fw-bold text-gray-600'>{user?.name ? user?.name : 'John Doe'}</div>
            </div>

            <div
              id='kt_signin_email_edit'
              className={'flex-row-fluid ' + (!showNameForm && 'd-none')}
            >
              <form
                onSubmit={formik1.handleSubmit}
                id='kt_signin_change_email'
                className='form'
                noValidate
              >
                <div className='row mb-6'>
                  <div className='col-lg-6 mb-4 mb-lg-0'>
                    <div className='fv-row mb-0'>
                      <label htmlFor='name' className='form-label fs-6 fw-bolder mb-3'>
                       {locale === 'uz' ? 'Yangi ism kirgizing':"Введите новое имя"}
                      </label>
                      <input
                        type='text'
                        className='form-control form-control-lg form-control-solid'
                        id='name'
                        placeholder='John doe'
                        {...formik1.getFieldProps('newName')}
                      />
                      {formik1.touched.newName && formik1.errors.newName && (
                        <div className='fv-plugins-message-container'>
                          <div className='fv-help-block'>{formik1.errors.newName}</div>
                        </div>
                      )}
                    </div>
                  </div>
                  
                </div>
                <div className='d-flex'>
                  <button
                    id='kt_signin_submit'
                    type='submit'
                    className='btn btn-primary  me-2 px-6'
                  >
                    {!loading1 && 'Update Name'}
                    {loading1 && (
                      <span className='indicator-progress' style={{display: 'block'}}>
                        {locale === 'uz' ? "Iltimos kuting" :'Пожалуйста, подождите'}...{' '}
                        <span className='spinner-border spinner-border-sm align-middle ms-2'></span>
                      </span>
                    )}
                  </button>
                  <button
                    id='kt_signin_cancel'
                    type='button'
                    onClick={() => {
                      setshowNameForm(false)
                    }}
                    className='btn btn-color-gray-400 btn-active-light-primary px-6'
                  >
                    {locale === 'uz' ? 'Qoldirish' :'отмена'}
                  </button>
                </div>
              </form>
            </div>

            <div id='kt_signin_email_button' className={'ms-auto ' + (showNameForm && 'd-none')}>
              <button
                onClick={() => {
                  setshowNameForm(true)
                }}
                className='btn btn-light btn-active-light-primary'
              >
                {locale === 'uz' ? 'Ism o`zgartirish':'Изменить имя'}
              </button>
            </div>
          </div>

          <div className='separator separator-dashed my-6'></div>

          <div className='d-flex flex-wrap align-items-center mb-10'>
            <div id='kt_signin_password' className={' ' + (showNumberForm && 'd-none')}>
              <div className='fs-6 fw-bolder mb-1'>{locale === 'uz' ? "Telifon Raqami":"Номер телефона"}</div>
              <div className='fw-bold text-gray-600'>{user?.number ? user?.number : '998999331564'}</div>
            </div>

            <div
              id='kt_signin_password_edit'
              className={'flex-row-fluid ' + (!showNumberForm && 'd-none')}
            >
              <form
                onSubmit={formik2.handleSubmit}
                id='kt_signin_change_password'
                className='form'
                noValidate
              >
                <div className='row mb-1'>
                  <div className='col-lg-4'>
                    <div className='fv-row mb-0'>
                      <label htmlFor='oldNumber' className='form-label fs-6 fw-bolder mb-3'>
                       {locale === 'uz' ? "Xozirgi telifon raqam":"Текущий номер телефона"}
                      </label>
                      <input
                        type='text'
                        className='form-control form-control-lg form-control-solid '
                        id='oldNumber'
                        {...formik2.getFieldProps('oldNumber')}
                      />
                      {formik2.touched.oldNumber && formik2.errors.oldNumber && (
                        <div className='fv-plugins-message-container'>
                          <div className='fv-help-block'>{formik2.errors.oldNumber}</div>
                        </div>
                      )}
                    </div>
                  </div>

                  <div className='col-lg-4'>
                    <div className='fv-row mb-0'>
                      <label htmlFor='newNumber' className='form-label fs-6 fw-bolder mb-3'>
                        {locale === 'uz' ? 'Yangi Telifon Raqam' : 'Новый номер телефона'}
                      </label>
                      <input
                        type='text'
                        className='form-control form-control-lg form-control-solid '
                        id='newNumber'
                        {...formik2.getFieldProps('newNumber')}
                      />
                      {formik2.touched.newNumber && formik2.errors.newNumber && (
                        <div className='fv-plugins-message-container'>
                          <div className='fv-help-block'>{formik2.errors.newNumber}</div>
                        </div>
                      )}
                    </div>
                  </div>

                 
                </div>

                <div className='form-text mb-5'>
                  {locale === 'uz' ? 'Parol kamida 8 ta belgidan iborat bo\'lishi va belgilarni o\'z ichiga olishi kerak':'Пароль должен быть не менее 8 символов и содержать символы'}
                </div>

                <div className='d-flex'>
                  <button
                    id='kt_password_submit'
                    type='submit'
                    className='btn btn-primary me-2 px-6'
                  >
                    {!loading2 && 'Update Password'}
                    {loading2 && (
                      <span className='indicator-progress' style={{display: 'block'}}>
                        {locale === 'uz' ? "Iltimos kuting" :'Пожалуйста, подождите'}...{' '}
                        <span className='spinner-border spinner-border-sm align-middle ms-2'></span>
                      </span>
                    )}
                  </button>
                  <button
                    onClick={() => {
                      setShowNumberForm(false)
                    }}
                    id='kt_password_cancel'
                    type='button'
                    className='btn btn-color-gray-400 btn-active-light-primary px-6'
                  >
                    {locale === 'uz' ? 'Qoldirish' :'отмена'}
                  </button>
                </div>
              </form>
            </div>

            <div
              id='kt_signin_password_button'
              className={'ms-auto ' + (showNumberForm && 'd-none')}
            >
              <button
                onClick={() => {
                  setShowNumberForm(true)
                }}
                className='btn btn-light btn-active-light-primary'
              >
                {locale === 'uz' ? 'raqamni ozgartirish':'изменить номер'}
              </button>
            </div>
          </div>

          
        </div>
      </div>
    </div>
  )
}

export {SignInMethod}
