import React, {useEffect, useState} from 'react'
import { useCreateAdvantagesMutation } from '../../../redux/features/advantages/advantagesApi'
import { toast } from 'react-toastify';
import { useLang } from '../../../_metronic/i18n/Metronici18n';
import uploadimg from '../../../_metronic/assets/upload.png'


const AdvnatagesPage = () => {

  const [createAdvantages, {isSuccess, isError}] = useCreateAdvantagesMutation()

  const locale = useLang()


  const [preview, setPreviw] = useState({
    previewURL: '',
  })
  const [loading , setLoading ] = useState(false)

  const [advantagesSchema , setAdvantagesSchema] = useState({
    title_ru:'',
    title_uz:'',
    description_uz:'',
    description_ru:'',
    img:''
  })

  const handleChangeValue = (e) => {
    setAdvantagesSchema(p => ({...p, [e.target.name]:e.target.value}))
  }

  const hanldechangeImg = (e) => {
      const file = e.target.files[0]
      setPreviw({
        previewURL: file ? URL?.createObjectURL(file) : '',
      })
      setAdvantagesSchema(p => ({...p, [e.target.name]:file}))
  }

  const handleSubmit =  async (e) => {
    try {
      e.preventDefault()

    setLoading(true)
    const formData = new FormData();
      Object.keys(advantagesSchema).forEach((key) => {
        formData.append(key, advantagesSchema[key]);
      });

      await createAdvantages(formData)

      setTimeout(() => {
        setLoading(false)
        setAdvantagesSchema({
          title_ru:'',
          title_uz:'',
          description_uz:'',
          description_ru:'',
          img:''
        })

        setPreviw({
          previewURL: '',
        })
      }, 1000)
      
    } catch (err) {
     setLoading(false) 
    }

  }

  useEffect(() => {
    if (isSuccess) {
      toast.success(locale == 'uz' ? 'ustunliklar muovfaqiyatli yaratildi ' : 'преимущества успешно созданa')
    }

    if (isError) {
      toast.error(locale == 'uz' ? 'ustunliklar yaratilishida xatolik yuz berdi  ' : ' Произошла ошибка при создании преимущества ')
    }
    

  }, [isSuccess, isError])  

  return (
    <form  onSubmit={handleSubmit}>
      <h1 className='mb-20'>
        {
          locale === 'uz'
          ? 'Ustunliklar Yaratish'
          : 'Создавать преимущества'
        }
      </h1>
      <div className='row mb-10 px-3'>

        <div className='col-md-6 d-flex flex-column gap-2'>
          <label htmlFor='title_ru' className='fs-3'>
             {locale === 'uz'  ? '(Sarlovha Ruschada)' :'заголовок на русском'}
          </label>
          <input
            type='text'
            id='title_ru'
            name='title_ru'
            required
            value={advantagesSchema.title_ru}
            onChange={handleChangeValue}
            className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
          />
        </div>

        <div className='col-md-6 d-flex flex-column gap-2'>
          <label htmlFor='title_uz' className='fs-3'>
             {locale === 'uz'  ? '(Sarlovha Uzbekchada)' :'заголовок на Узбекский'}
          </label>
          <input
            type='text'
            id='title_uz'
            name='title_uz'
            required
            value={advantagesSchema.title_uz}
            onChange={handleChangeValue}
            className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
     
          />
        </div>
      </div>

      <div className='row mb-10 px-3'>
        <div className='col-md-6 d-flex flex-column gap-2'>
          <label htmlFor='description_ru' className='fs-3'>
              {locale === 'uz'  ? '(Tarif Uzbekchada)' :'описание  на Узбекский'}
          </label>
          <textarea
            id='description_ru'
            name='description_ru'
            required
            value={advantagesSchema.description_ru}
            onChange={handleChangeValue}
            className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
  
          />
        </div>

        <div className='col-md-6 d-flex flex-column gap-2'>
          <label htmlFor='description_uz' className='fs-3'>
              {locale === 'uz'  ? '(Tarifi Ruschada)' :'описание  на Русский'}
          </label>
          <textarea
            id='description_uz'
            value={advantagesSchema.description_uz}
            name='description_uz'
            required
            onChange={handleChangeValue}
            className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
          />
        </div>
      </div>

      <div className='row d-flex align-items-center  px-5'>
        <div
          className='col-md-6 d-flex align-items-center position-relative  justify-content-center form-control-solid form-control form-control-lg w-25'
          style={{height: '200px', cursor: 'pointer'}}
        >
          <input
            type='file'
            name='img'
            required
            onChange={hanldechangeImg}
            accept='image/*'
            className='opacity-0 position-absolute w-100 h-100'
            style={{cursor: 'pointer'}}
          />
          <img src={uploadimg} alt="upload" className='w-100 h-100' style={{objectFit:'contain'}} />
        </div>
        <div className='col-md-6'>
          {preview.previewURL && (
            <img
              src={preview.previewURL}
              alt='uploaded img'
              style={{maxWidth: '100%', maxHeight: '200px', marginTop: '10px'}}
            />
          )}
        </div>
      </div>

      {
        locale === 'uz'
        ?
    <button className={`btn ${loading ? 'btn-secondary' : 'btn-dark'} mt-10 ml-3 `} style={{padding:'1rem', width:"120px"}} disabled={loading}>
      {loading ? 'Jonatilmoqda...' : 'Jonatish'}
    </button>
    : 
    <button className={`btn ${loading ? 'btn-secondary' : 'btn-dark'} mt-10 ml-3 `} style={{padding:'1rem', width:"120px"}} disabled={loading}>
      {loading ? 'отправляться...' : 'отправлять'}
    </button>

    }
    </form>
  )
}

export default AdvnatagesPage
