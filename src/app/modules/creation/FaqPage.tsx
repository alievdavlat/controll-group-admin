import React, {useEffect, useState} from 'react'
import {useCreateFaqMutation} from '../../../redux/features/faq/faqApi'
import {toast} from 'react-toastify'
import {useLang} from '../../../_metronic/i18n/Metronici18n'

interface IFaqSchema {
  answer_uz: string
  answer_ru: string
  question_uz: string
  question_ru: string
}

const GalleryPage = () => {
  const [createFaq, {isError, isSuccess}] = useCreateFaqMutation()
  const locale = useLang()

  const [loading, setLoading] = useState(false)

  const [faqSchema, setFaqSchema] = useState<IFaqSchema>({
    answer_ru: '',
    answer_uz: '',
    question_ru: '',
    question_uz: '',
  })

  const handleChangeValue = (e: any) => {
    setFaqSchema((p) => ({...p, [e?.target?.name]: e?.target?.value}))
  }

  const handleSubmit = async (e) => {
    try {
      e.preventDefault()
      setLoading(true)

      await createFaq(faqSchema)

      setTimeout(() => {
        setLoading(false)
        setFaqSchema({
          answer_ru: '',
          answer_uz: '',
          question_ru: '',
          question_uz: '',
        })
      }, 1000)
    } catch (err) {
      setLoading(false)
    }
  }

  useEffect(() => {
    if (isSuccess) {
      toast.success(
        locale === 'uz' ? 'Savol va Javoblar yaratildi' : 'часто задаваемых вопросов создан'
      )
    }

    if (isError) {
      toast.error(locale === 'uz' ? 'Savol va Javoblar yaratilmadi' : 'Вопросы и ответы не созданы')
    }
  }, [isSuccess, isError])

  return (
    <form onSubmit={handleSubmit}>
      <h1 className='mb-10 mx-5'>
        {locale === 'uz' ? 'Savol va Javoblar yaratish' : 'Создавать вопросы и ответы'}
      </h1>
      <>
        <div className='row mb-10 px-3'>
          <div className='col-md-6 d-flex flex-column gap-2'>
            <label htmlFor='answer_ru' className='fs-3'>
               {locale ===  'uz'  ? "Javob Rus tilida":"ответь на русском"}
            </label>
            <input
              type='text'
              id='answer_ru'
              name='answer_ru'
              value={faqSchema.answer_ru}
              onChange={handleChangeValue}
              required
              className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
            />
          </div>

          <div className='col-md-6 d-flex flex-column gap-2'>
            <label htmlFor='answer_uz' className='fs-3'>
               {locale ===  'uz'  ? "Javob Uzbek tilida":"ответь на Узбекский"}
            </label>
            <input
              type='text'
              id='answer_uz'
              name='answer_uz'
              value={faqSchema.answer_uz}
              onChange={handleChangeValue}
              required
              className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
            />
          </div>
        </div>

        <div className='row mb-10 px-3'>
          <div className='col-md-6 d-flex flex-column gap-2'>
            <label htmlFor='question_ru' className='fs-3'>
               {locale === 'uz' ? "savol Ruschada":"вопрос Ha русски"}
            </label>
            <textarea
              id='question_ru'
              name='question_ru'
              value={faqSchema.question_ru}
              onChange={handleChangeValue}
              required
              cols={8}
              rows={10}
              className='form-control form-control-lg form-control-solid  mb-3 mb-lg-0'
            />
          </div>

          <div className='col-md-6 d-flex flex-column gap-2'>
            <label htmlFor='question_uz' className='fs-3'>
               {locale === 'uz' ? "savol o'zbek tilida" :"вопрос на узбекском"}
            </label>
            <textarea
              id='question_uz'
              name='question_uz'
              value={faqSchema.question_uz}
              onChange={handleChangeValue}
              required
              cols={8}
              rows={10}
              className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
            />
          </div>
        </div>
      </>

      {locale === 'uz' ? (
        <button
          className={`btn ${loading ? 'btn-secondary' : 'btn-dark'} mt-10 ml-3 `}
          style={{padding: '1rem', width: '120px'}}
          disabled={loading}
        >
          {loading ? 'Jonatilmoqda....' : 'Jonatish'}
        </button>
      ) : (
        <button
          className={`btn ${loading ? 'btn-secondary' : 'btn-dark'} mt-10 ml-3 `}
          style={{padding: '1rem', width: '120px'}}
          disabled={loading}
        >
          {loading ? 'отправляться....' : 'отправлять'}
        </button>
      )}
    </form>
  )
}

export default GalleryPage
