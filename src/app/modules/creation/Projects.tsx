import  { useEffect, useState } from 'react'
import { useLang } from '../../../_metronic/i18n/Metronici18n'
import uploadimg from '../../../_metronic/assets/upload.png'
import { useCreateProjectMutation } from '../../../redux/features/projects/projectsApi'
import { toast } from 'react-toastify'

const Projects = () => {
  const locale = useLang()
  const [preview, setPreviw] = useState({
    previewURL: '',
  })

  const [createProject, {isSuccess, isError}] = useCreateProjectMutation() 

  const [preview2, setPreviw2] = useState({
    previewURL: '',
  })

  const [preview3, setPreviw3] = useState({
    previewURL: '',
  })

  const [loading , setLoading ] = useState(false)


  const [projectSchema , setprojectSchema] = useState({
    address:'',
    measurment:'',
    deadline_uz:'',
    deadline_ru:'',
    img1:'',
    img2:'',
    img3:''
  })

  const handleChangeValue = (e) => {
    setprojectSchema(p => ({...p, [e.target.name]:e.target.value}))
  }

  const hanldechangeImg = (e) => {
      const file = e.target.files[0]
      setPreviw({
        previewURL: file ? URL?.createObjectURL(file) : '',
      })
     
      setprojectSchema(p => ({...p, [e.target.name]:file}))
  }

  const hanldechangeImg2 = (e) => {
    const file = e.target.files[0]
    setPreviw2({
      previewURL: file ? URL?.createObjectURL(file) : '',
    })
    setprojectSchema(p => ({...p, [e.target.name]:file}))
  }

  const hanldechangeImg3 = (e) => {
    const file = e.target.files[0]
    setPreviw3({
      previewURL: file ? URL?.createObjectURL(file) : '',
    })
    setprojectSchema(p => ({...p, [e.target.name]:file}))
  }


  useEffect(() => {
    if (isSuccess) {
      toast.success(locale === 'uz' ? 'proyekt yaratildi' : 'проект создан')
    }
    if (isError) {
      toast.error(locale === 'uz' ? 'Proyekt yaratishda xatolik yuz berdi' :"Произошла ошибка при создании проекта")
    }
  }, [isError, isSuccess])

  const handleSubmit =  async (e) => {
    try {
    e.preventDefault()

    setLoading(true)
    const formData = new FormData();
      Object.keys(projectSchema).forEach((key) => {
        formData.append(key, projectSchema[key]);
      });
      
      await createProject(formData)

      setTimeout(() => {
        setLoading(false)
        setprojectSchema({
          address:'',
          measurment:'',
          deadline_uz:'',
          deadline_ru:'',
          img1:'',
          img2:'',
          img3:''
        })

        setPreviw({
          previewURL: '',
        })
        setPreviw2({
          previewURL: '',
        })
        setPreviw3({
          previewURL: '',
        })
      }, 1000)
      
    } catch (err) {
     setLoading(false) 
    }

  }

  return (
    <form onSubmit={handleSubmit}>
        <h1 className='mb-20'>
        {
          locale === 'uz'
          ? 'Proyekt  Yaratish'
          : 'Создание проекта'
        }
      </h1>
      <div className='row mb-10 px-3'>

        <div className='col-md-6 d-flex flex-column gap-2'>
          <label htmlFor='address' className='fs-3'>
            {locale === 'uz' ? 'address':'адрес'}
          </label>
          <input
            type='text'
            id='address'
            name='address'
            required
            value={projectSchema.address}
            onChange={handleChangeValue}
            className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
            placeholder='address'
          />
        </div>

        <div className='col-md-6 d-flex flex-column gap-2'>
          <label htmlFor='measurment' className='fs-3'>
            {locale === 'uz' ? 'o\'lchov':'измерение'}
          </label>
          <input
            type='text'
            id='measurment'
            name='measurment'
            required
            value={projectSchema.measurment}
            onChange={handleChangeValue}
            className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
            placeholder='measurment'
          />
        </div>
      </div>

      <div className='row mb-10 px-3'>

        <div className='col-md-6 d-flex flex-column gap-2'>
          <label htmlFor='deadline_uz' className='fs-3'>
          {locale === 'uz' ? 'tugatilgan muddat Uzbekchada':'Срок выполнения Узбекский'}
          </label>
          <input
            type='text'
            id='deadline_uz'
            name='deadline_uz'
            required
            value={projectSchema.deadline_uz}
            onChange={handleChangeValue}
            className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
            placeholder='deadline_uz'
          />
        </div>

        <div className='col-md-6 d-flex flex-column gap-2'>
          <label htmlFor='deadline_ru' className='fs-3'>
          {locale === 'uz' ? 'tugatilgan muddat Ruschada':'Срок выполнения Русский'}
          </label>
          <input
            type='text'
            id='deadline_ru'
            name='deadline_ru'
            required
            value={projectSchema.deadline_ru}
            onChange={handleChangeValue}
            className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
            placeholder='deadline_ru'
          />
        </div>
      </div>


      <div className='row mt-10 d-flex align-items-center  px-5'>
        <div
          className='col-md-6 mr-5 d-flex align-items-center position-relative  justify-content-center form-control-solid form-control form-control-lg w-25'
          style={{height: '200px', cursor: 'pointer'}}
        >
          <input
            type='file'
            name='img1'
            required
            onChange={hanldechangeImg}
            accept='image/*'
            className='opacity-0 position-absolute w-100 h-100'
            style={{cursor: 'pointer'}}
          />
          <img src={uploadimg} alt="upload" className='w-100 h-100' style={{objectFit:'contain'}} />
        </div>

        <div className='col-md-6'>
          {preview.previewURL && (
            <img
              src={preview.previewURL}
              alt='uploaded img'
              style={{maxWidth: '100%', maxHeight: '200px', marginTop: '10px'}}
            />
          )}
        </div>

      </div>
      
      <div className='row mt-10 d-flex align-items-center  px-5'>
        <div
          className='col-md-6 mr-5 d-flex align-items-center position-relative  justify-content-center form-control-solid form-control form-control-lg w-25'
          style={{height: '200px', cursor: 'pointer'}}
        >
          <input
            type='file'
            name='img2'
            required
            multiple
            onChange={hanldechangeImg2}
            accept='image/*'
            className='opacity-0 position-absolute w-100 h-100'
            style={{cursor: 'pointer'}}
          />
          <img src={uploadimg} alt="upload" className='w-100 h-100' style={{objectFit:'contain'}} />
        </div>
        <div className='col-md-6'>
          {preview2.previewURL && (
            <img
              src={preview2.previewURL}
              alt='uploaded img'
              style={{maxWidth: '100%', maxHeight: '200px', marginTop: '10px'}}
            />
          )}
        </div>
      </div>

      <div className='row mt-10 d-flex align-items-center  px-5'>
      
        
        <div
          className='col-md-6 mr-5 d-flex align-items-center position-relative  justify-content-center form-control-solid form-control form-control-lg w-25'
          style={{height: '200px', cursor: 'pointer'}}
        >
          <input
            type='file'
            name='img3'
            required
            multiple
            onChange={hanldechangeImg3}
            accept='image/*'
            className='opacity-0 position-absolute w-100 h-100'
            style={{cursor: 'pointer'}}
          />
          <img src={uploadimg} alt="upload" className='w-100 h-100' style={{objectFit:'contain'}} />
        </div>
        <div className='col-md-6'>
          {preview3.previewURL && (
            <img
              src={preview3.previewURL}
              alt='uploaded img'
              style={{maxWidth: '100%', maxHeight: '200px', marginTop: '10px'}}
            />
          )}
        </div>
      </div>



      {
        locale === 'uz'
        ?
    <button className={`btn ${loading ? 'btn-secondary' : 'btn-dark'} mt-10 ml-3 `} style={{padding:'1rem', width:"120px"}} disabled={loading}>
      {loading ? 'Jonatilmoqda...' : 'Jonatish'}
    </button>
    : 
    <button className={`btn ${loading ? 'btn-secondary' : 'btn-dark'} mt-10 ml-3 `} style={{padding:'1rem', width:"120px"}} disabled={loading}>
      {loading ? 'отправляться...' : 'отправлять'}
    </button>
    }

    </form>
  )
}

export default Projects