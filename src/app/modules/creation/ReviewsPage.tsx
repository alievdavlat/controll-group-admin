import {useEffect, useState} from 'react'
import {useCreateReviewMutation} from '../../../redux/features/reviews/reviewsApi'
import {toast} from 'react-toastify'
import {useLang} from '../../../_metronic/i18n/Metronici18n'
import uploadimg from '../../../_metronic/assets/upload.png'

const ReviewsPage = () => {
  const [loading, setLoading] = useState(false)
  const [createReview, {isError, isSuccess}] = useCreateReviewMutation()
  const locale = useLang()

  const [reviewSchema, setReviewSchema] = useState({
    owner_ru: '',
    owner_uz: '',
    description_ru: '',
    description_uz: '',
    avatar: '',
  })

  const handleChangeValue = (e) => {
    setReviewSchema((p) => ({...p, [e.target.name]: e.target.value}))
  }

  const [preview, setPreviw] = useState({
    previewURL: '',
  })

  const hanldechangeImg = (e) => {
    const file = e.target.files[0]
    setPreviw({
      previewURL: file ? URL?.createObjectURL(file) : '',
    })

    setReviewSchema((p) => ({...p, [e.target.name]: file}))
  }

  
  
  
  useEffect(() => {
    if (isSuccess) {
      toast.success(
        locale === 'uz' ? 'Sharhlar muvaffaqiyatli yaratildi' : 'элемент отзывов успешно создан'
      )
    }

    if (isError) {
      toast.error(
        locale === 'uz'
          ? 'Sharhlarni yaratishda xatolik yuz berdi'
          : 'Произошла ошибка при создании комментариев'
      )
    }
  }, [isError, isSuccess])

  const handleSubmit = async (e) => {
    try {
      e.preventDefault();
      setLoading(true);
  
      let formData = new FormData();
  
      // Append values from reviewSchema state to FormData
      formData.append('owner_ru', reviewSchema.owner_ru);
      formData.append('owner_uz', reviewSchema.owner_uz);
      formData.append('description_ru', reviewSchema.description_ru);
      formData.append('description_uz', reviewSchema.description_uz);

      
      // Append the file object if avatar is set
      if (reviewSchema.avatar) {
        formData.append('avatar', reviewSchema.avatar);
      }
  
      // Call createReview function with the FormData
      await createReview(formData);
  
      
      // Reset form values after submission
      setTimeout(() => {
        setLoading(false);
        setReviewSchema({
          owner_ru: '',
          owner_uz: '',
          description_ru: '',
          description_uz: '',
          avatar: '',
        });
      }, 1000);
    } catch (err) {
      setLoading(false);
      console.error('Error submitting form:', err);
    }
  }


  return (
    <form onSubmit={handleSubmit} >
      <h1 className='mb-20'>{locale === 'uz' ? 'Sharhlar Yaratish' : 'Создание комментариев'}</h1>

      <div className='row mb-10 px-3'>
        <div className='col-md-6 d-flex flex-column gap-2'>
          <label htmlFor='owner_ru' className='fs-3'>
            {locale === 'uz' ? 'Ta`rif egasi ismi Ruchada' : 'Имя владельца  Русский.'}
          </label>
          <input
            type='text'
            id='owner_ru'
            name='owner_ru'
            value={reviewSchema.owner_ru}
            onChange={handleChangeValue}
            required
            className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
          />
        </div>

        <div className='col-md-6 d-flex flex-column gap-2'>
          <label htmlFor='owner_uz' className='fs-3'>
            {locale === 'uz' ? 'Ta`rif egasi ismi Uzbekchada' : 'Имя владельца  Узбекский.'}
          </label>
          <input
            type='text'
            id='owner_uz'
            name='owner_uz'
            required
            value={reviewSchema.owner_uz}
            onChange={handleChangeValue}
            className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
          />
        </div>
      </div>

      <div className='row mb-10 px-3'>
        <div className='col-md-6 d-flex flex-column gap-2'>
          <label htmlFor='description_ru' className='fs-3'>
            {locale === 'uz' ? 'Ta`rif  Ruschada' : 'описания на Русский'}
          </label>
          <textarea
            id='description_ru'
            name='description_ru'
            required
            value={reviewSchema.description_ru}
            onChange={handleChangeValue}
            className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
          />
        </div>

        <div className='col-md-6 d-flex flex-column gap-2'>
          <label htmlFor='description_uz' className='fs-3'>
            {locale === 'uz' ? 'Ta`rif Uzbekchada' : ' описания на узбекском.'}
          </label>
          <textarea
            id='description_uz'
            name='description_uz'
            required
            value={reviewSchema.description_uz}
            onChange={handleChangeValue}
            className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
          />
        </div>
      </div>

      <div className='row mt-10 px-3'>
        <div
          className='col-md-6 mr-5 d-flex align-items-center position-relative  justify-content-center form-control-solid form-control form-control-lg w-25'
          style={{height: '200px', cursor: 'pointer'}}
        >
          <input
            type='file'
            name='avatar'
            multiple
            onChange={hanldechangeImg}
            accept='image/*'
            className='opacity-0 position-absolute w-100 h-100'
            style={{cursor: 'pointer'}}
          />
          <img
            src={uploadimg}
            alt='upload'
            className='w-100 h-100'
            style={{objectFit: 'contain'}}
          />
        </div>
        <div className='col-md-6'>
          {preview.previewURL && (
            <img
              src={preview.previewURL}
              alt='uploaded img'
              style={{maxWidth: '100%', maxHeight: '200px', marginTop: '10px'}}
            />
          )}
        </div>
      </div>

      {locale === 'uz' ? (
        <button
          className={`btn ${loading ? 'btn-secondary' : 'btn-dark'} mt-10 ml-3 `}
          style={{padding: '1rem', width: '120px'}}
          disabled={loading}
        >
          {loading ? 'Jonatilmoqda....' : 'Jonatish'}
        </button>
      ) : (
        <button
          className={`btn ${loading ? 'btn-secondary' : 'btn-dark'} mt-10 ml-3 `}
          style={{padding: '1rem', width: '120px'}}
          disabled={loading}
        >
          {loading ? 'отправляться....' : 'отправлять'}
        </button>
      )}
    </form>
  )
}

export default ReviewsPage
