import  {useEffect, useState} from 'react'
import uploadimg from '../../../_metronic/assets/upload.png'
import { useCreateServicesMutation } from '../../../redux/features/services-api/servicesApi'
import { toast } from 'react-toastify'
import { useLang } from '../../../_metronic/i18n/Metronici18n'
import { useNavigate } from 'react-router-dom'

const ServicesPage = () => {
  const [loading , setLoading ] = useState(false)
  const [createServices , {isSuccess, isError}] = useCreateServicesMutation()
  const [preview, setPreviw] = useState({
    previewURL: '',
  })
const navigate = useNavigate()
  const locale = useLang()

const [servicesSchema, setServicesSchema] = useState({
  service_type_ru:'',
  service_type_uz:'',
  title_ru:'',
  title_uz:'',
  description_ru:'',
  description_uz:'',
  subtitle_one_ru:'',
  subtitle_one_uz:'',
  subtitle_two_uz:'',
  subtitle_two_ru:'',
  img:''
})

const handleChangeValue = (e) => {
  setServicesSchema(p => ({...p, [e.target.name]:e.target.value}))
}

const hanldechangeImg = (e) => {
    const file = e.target.files[0]
    setPreviw({
      previewURL: file ? URL?.createObjectURL(file) : '',
    })
    setServicesSchema(p => ({...p, [e.target.name]:file}))
}

const handleSubmit = async (e) => {
try {
  e.preventDefault()

  setLoading(true)

  const formData = new FormData();
    Object.keys(servicesSchema).forEach((key) => {
      formData.append(key, servicesSchema[key]);
    });

      

    await createServices(formData)

    setTimeout(() => {
      setLoading(false)
      setServicesSchema({
        service_type_ru:'',
        service_type_uz:'',
        title_ru:'',
        title_uz:'',
        description_ru:'',
        description_uz:'',
        subtitle_one_ru:'',
        subtitle_one_uz:'',
        subtitle_two_uz:'',
        subtitle_two_ru:'',
        img:''
      })

      setPreviw({
        previewURL: '',
      })

      setLoading(false)
      navigate('/dashboard/services-list')
    }, 1000)
    
} catch (err) {
 setLoading(false) 
}
}

useEffect(() => {
  if (isSuccess) {
    toast.success(locale === 'uz' ? "Xizmatlarni muovfaqiyatli yasaldi" : 'сервисы успешно созданы')
  }

  if (isError) {
    toast.error(locale === 'uz' ? 'xizmatlarni yasashda xatolik yuz berdi': "Произошла ошибка при создании услуг.")
  }
  

}, [isSuccess, isError])  


return (
    <form onSubmit={handleSubmit}>

        <h1 className='mb-20'>
          {
            locale === 'uz' 
            ? 'Xizmatlarni Yaratish'
            : 'Создание услуг'
          }
        </h1>

      <div className='row mb-10 px-3'>
      
        <div className='col-md-6 d-flex flex-column gap-2'>
          <label htmlFor='service_type_ru' className='fs-3'>
          
           {locale == 'uz' ? 'Xizmat turi Ruschada' :"вид услуги на  Русский"}
          </label>
          <input
            type='text'
            id='service_type_ru'
            name='service_type_ru'
            value={servicesSchema.service_type_ru}
            onChange={handleChangeValue}
            className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
            required
          />
        </div>

        <div className='col-md-6 d-flex flex-column gap-2'>
          <label htmlFor='service_type_uz' className='fs-3'>
            {locale == 'uz' ? 'Xizmat turi Uzbekchada' :"вид услуги на узбекском"}
          </label>
          <input
            type='text'
            id='service_type_uz'
            name='service_type_uz'
            value={servicesSchema.service_type_uz} 
            onChange={handleChangeValue}
            className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
            required
          />
        </div>
      </div>

      <div className='row mb-10 px-3'>
        <div className='col-md-6 d-flex flex-column gap-2'>
          <label htmlFor='title_ru' className='fs-3'>
             {locale === 'uz' ? 'Sarlovha Ruschada' :'заголовок на Русский '}
          </label>
          <input
            type='text'
            id='title_ru'
            name='title_ru'
            value={servicesSchema.title_ru}
            onChange={handleChangeValue}
            className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
            required
          />
        </div>

        <div className='col-md-6 d-flex flex-column gap-2'>
          <label htmlFor='title_uz' className='fs-3'>
             {locale === 'uz' ? 'Sarlovha uzbekchada' :'заголовок на узбекском '}
          </label>
          <input
            type='text'
            id='title_uz'
            name='title_uz'
            value={servicesSchema.title_uz}
            onChange={handleChangeValue}
            className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
            required
          />
        </div>
      </div>

      <div className='row mb-10 px-3'>
        <div className='col-md-6 d-flex flex-column gap-2'>
          <label htmlFor='subtitle_one_ru' className='fs-3'>
           {locale === 'uz' ? ' Sarlovha 1 Ruschada' :'Подзаголовок 1 на Русский '}
          </label>
          <input
            type='text'
            id='subtitle_one_ru'
            name='subtitle_one_ru'
            value={servicesSchema.subtitle_one_ru}
            onChange={handleChangeValue}
            className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
            required
          />
        </div>

        <div className='col-md-6 d-flex flex-column gap-2'>
          <label htmlFor='subtitle_one_uz' className='fs-3'>
             {locale === 'uz' ? 'Sarlovha 1 uzbekchada' :'Подзаголовок 1 на узбекском '}
          </label>
          <input
            type='text'
            id='subtitle_one_uz'
            name='subtitle_one_uz'
            value={servicesSchema.subtitle_one_uz}
            onChange={handleChangeValue}
            className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
            required
          />
        </div>
      </div>

      <div className='row mb-10 px-3'>
        <div className='col-md-6 d-flex flex-column gap-2'>
          <label htmlFor='subtitle_two_ru' className='fs-3'>
            {locale === 'uz' ? 'Sarlovha 2 Ruschada' :'Подзаголовок 2 на Русский '}
          </label>
          <input
            type='text'
            id='subtitle_two_ru'
            name='subtitle_two_ru'
            value={servicesSchema.subtitle_two_ru}
            onChange={handleChangeValue}
            className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
            required
          />
        </div>

        <div className='col-md-6 d-flex flex-column gap-2'>
          <label htmlFor='subtitle_two_uz' className='fs-3'>
             {locale === 'uz' ? 'Sarlovha 2 uzbekchada' :'Подзаголовок 2 на узбекском '}
          </label>
          <input
            type='text'
            id='subtitle_two_uz'
            name='subtitle_two_uz'
            value={servicesSchema.subtitle_two_uz}
            onChange={handleChangeValue}
            className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
            required
          />
        </div>
      </div>

      <div className='row mb-10 px-3'>
        <div className='col-md-6 d-flex flex-column gap-2'>
          <label htmlFor='description_ru' className='fs-3'>
             {locale === 'uz' ? ' tavsifi Ruschada' :'описание  на русском '}
          </label>
          <textarea
            id='description_ru'
            name='description_ru'
            value={servicesSchema.description_ru}
            onChange={handleChangeValue}
            className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
            required
          />
        </div>

        <div className='col-md-6 d-flex flex-column gap-2'>
          <label htmlFor='description_uz' className='fs-3'>
             {locale === 'uz' ? 'tavsifi Uzbekchada' :'описание  на Узбекский '}
          </label>
          <textarea
            id='description_uz'
            name='description_uz'
            value={servicesSchema.description_uz}
            onChange={handleChangeValue}
            className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
            required
          />
        </div>
      </div>

      <div className='row d-flex align-items-center  px-5'>
        <div
          className='col-md-6 d-flex align-items-center position-relative  justify-content-center form-control-solid form-control form-control-lg w-25'
          style={{height: '200px', cursor: 'pointer'}}
        >
          <input
            type='file'
            name='img'
            onChange={hanldechangeImg}
            accept='image/*'
            className='opacity-0 position-absolute w-100 h-100'
            style={{cursor: 'pointer'}}
            required
          />
          <img src={uploadimg} alt="upload" className='w-100 h-100' style={{objectFit:'contain'}} />
        </div>
        <div className='col-md-6'>
          {preview.previewURL && (
            <img
              src={preview.previewURL}
              alt='uploaded img'
              style={{maxWidth: '100%', maxHeight: '200px', marginTop: '10px'}}
            />
          )}
        </div>
      </div>
      {
        locale === 'uz'
        ?
    <button className={`btn ${loading ? 'btn-secondary' : 'btn-dark'} mt-10 ml-3 `} style={{padding:'1rem', width:"120px"}} disabled={loading}>
      {loading ? 'Jonatilmoqda....' : 'Jonatish'}
    </button>
    : 
    <button className={`btn ${loading ? 'btn-secondary' : 'btn-dark'} mt-10 ml-3 `} style={{padding:'1rem', width:"120px"}} disabled={loading}>
      {loading ? 'отправляться....' : 'отправлять'}
    </button>

    }
    </form>
  )
}

export default ServicesPage
