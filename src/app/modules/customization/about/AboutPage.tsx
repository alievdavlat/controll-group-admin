import  {useEffect, useState} from 'react'
import uploadimg from '../../../../_metronic/assets/upload.png'
import {toast} from 'react-toastify'
import {
  useEditLayoutMutation,
  useGetAboutDataQuery,
} from '../../../../redux/features/layout/layoutApi'
import {useLang} from '../../../../_metronic/i18n/Metronici18n'
import plusIcon from '../../../../_metronic/assets/plus.png'


const AboutPage = () => {
  const locale = useLang()
  const theme = localStorage.getItem('kt_theme_mode_menu')

  const [editLayout, {isSuccess: layoutSuccess, isError}] = useEditLayoutMutation()
  const {data, isSuccess, refetch} = useGetAboutDataQuery(undefined, {
    refetchOnMountOrArgChange: true,
  })

  const [preview, setPreviw] = useState({
    previewURL: '',
  })
  const [loading, setLoading] = useState(false)

  const [aboutSchema, setaboutSchema] = useState({
    image: '',
    title_uz: '',
    title_ru: '',
    subtitle_ru: '',
    subtitle_uz: '',
    counts: [
      {
        count: '',
        title_ru: '',
        title_uz: '',
      },
      
    ],
  })


  const handleChangeValue = (e) => {
    setaboutSchema((p) => ({...p, [e.target.name]: e.target.value}))
  }
  const handleCountChange = (e, index) => {
    const { name, value } = e.target;

    setaboutSchema((prevAboutSchema) => {
      const updatedCounts = [...prevAboutSchema.counts];
      updatedCounts[index] = {
        ...updatedCounts[index],
        [name]: value,
      };
  
      return {
        ...prevAboutSchema,
        counts: updatedCounts,
      };
    })
  }


  const hanldechangeImg = (e) => {
    const file = e.target.files[0]
    setPreviw({
      previewURL: file ? URL?.createObjectURL(file) : '',
    })
    setaboutSchema((p) => ({...p, [e.target.name]: file}))
  }

  const handleSubmit = async (e) => {
    try {
      e.preventDefault()

      setLoading(true)
      const formData = new FormData()

      formData.append('image', aboutSchema?.image)
      formData.append('title_ru', aboutSchema?.title_ru)
      formData.append('title_uz', aboutSchema?.title_uz)
      formData.append('subtitle_uz', aboutSchema?.subtitle_uz)
      formData.append('subtitle_ru', aboutSchema?.subtitle_ru)

      console.log(Object.keys(aboutSchema?.counts[0]));

      aboutSchema.counts.forEach((countItem, index) => {
        formData.append(`counts[${index}][count]`, countItem.count);
        formData.append(`counts[${index}][title_ru]`, countItem.title_ru);
        formData.append(`counts[${index}][title_uz]`, countItem.title_uz);
      });
  
      


      
      
      await editLayout({data: formData, type: 'about'})

      setTimeout(() => {
        setLoading(false)

        refetch()

        setPreviw({
          previewURL: '',
        })
      }, 1000)
    } catch (err) {
      setLoading(false)
    }
  }

  useEffect(() => {
    if (data?.layout?.about) {
      setaboutSchema({...data?.layout?.about})
    }
  }, [isSuccess, refetch, data])

  useEffect(() => {
    if (layoutSuccess) {
      toast.success(
        locale == 'uz'
          ? 'Bize Xaqimizda sahifasi  muofvaqiyatli yangilandi'
          : 'о нас раздел успешно обновлено '
      )
    }

    if (isError) {
      toast.error(
        locale === 'uz'
          ? 'bIz Xaqimizda Sahifasi yangilanishda Xato yuz Berdi'
          : 'раздел о нас не обновляется'
      )
    }
  }, [layoutSuccess, isError])

  return (
    <form onSubmit={handleSubmit}>
      <h1 style={{margin: '70px 10px'}}>
        {locale === 'uz' ? 'Biz xaqimizda sahifani o`zgartirish ' : 'Редактировать страница  о нас'}
      </h1>

      <div className='row mb-10 px-3'>
        <div className='col-md-6 d-flex flex-column gap-2'>
          <label htmlFor='title_ru' className='fs-3'>
          title_ru  {locale === 'uz' ? '(Biz Xaqimizda sahifasi Sarlovhosi Ruscha)' :'(заголовок страницы О нас Русский)  '}
          </label>
          <input
            type='text'
            id='title_ru'
            name='title_ru'
            value={aboutSchema.title_ru}
            onChange={handleChangeValue}
            className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
          />
        </div>

        <div className='col-md-6 d-flex flex-column gap-2'>
          <label htmlFor='title_uz' className='fs-3'>
            title_uz  {locale === 'uz' ? '(Biz Xaqimizda sahifasi Sarlovhosi Uzbekcha)' :'(заголовок страницы О нас Узбекский)  '}
          </label>
          <input
            type='text'
            id='title_uz'
            name='title_uz'
            value={aboutSchema.title_uz}
            onChange={handleChangeValue}
            className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
          />
        </div>
      </div>

      <div className='row mb-10 px-3'>
        <div className='col-md-6 d-flex flex-column gap-2'>
          <label htmlFor='subtitle_ru' className='fs-3'>
            subtitle_ru  {locale === 'uz' ? '(Biz Xaqimizda sahifasi 2 chi Sarlovhosi Ruscha)' :'(Название страницы О нас Подзаголовок Русский)  '}
          </label>
          <textarea
            id='subtitle_ru'
            name='subtitle_ru'
            value={aboutSchema.subtitle_ru}
            onChange={handleChangeValue}
            rows={10}
            className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
          />
        </div>

        <div className='col-md-6 d-flex flex-column gap-2'>
          <label htmlFor='subtitle_uz' className='fs-3'>
            subtitle_uz  {locale === 'uz' ? '(Biz Xaqimizda sahifasi 2chi Sarlovhosi Uzbekcha)' :'(Название страницы О нас Подзаголовок Узбекский)  '}
          </label>
          <textarea
            id='subtitle_uz'
            value={aboutSchema.subtitle_uz}
            name='subtitle_uz'
            onChange={handleChangeValue}
            rows={10}
            className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
          />
        </div>
      </div>
      <>
        {aboutSchema?.counts?.map((item, index) => (
          <div className='row mb-10' key={index}>
            <div className='col-md-4 d-flex flex-column gap-2'>
              <label htmlFor='count' className='fs-3'>
                count {locale === 'uz' ? '(Statistika Soni )' : '(Количество статистики  )'}
              </label>
              <input
                type='text'
                id='count'
                name='count'
                value={item?.count}
                onChange={(e) => handleCountChange(e, index)}
                className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
              />
            </div>

            <div className='col-md-4  d-flex flex-column gap-2'>
              <label htmlFor='title_uz' className='fs-3'>
                count title_uz {locale === 'uz' ? '(Statistika Sarlhovhasi  Uzbekcha)' : '(Название статистики на узбекском  )'}
              </label>
              <input
                type='text'
                id='title_uz'
                name='title_uz'
                value={item.title_uz}
                onChange={(e) => handleCountChange(e, index)}
                className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
              />
            </div>

            <div className='col-md-4  d-flex flex-column gap-2'>
              <label htmlFor='title_ru' className='fs-3'>
                count title_ru  {locale === 'uz' ? '(Statistika Sarlhovhasi  Ruscha)' : '(Название статистики на Русский  )'}
              </label>
              <input
                type='text'
                id='title_ru'
                name='title_ru'
                value={item.title_ru}
                onChange={(e) => handleCountChange(e, index)}
                className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
              />
            </div>
          </div>
        ))}

        <button
          onClick={() => {
            setaboutSchema(p => ({...p, counts:[...p.counts, {
              count: '',
              title_ru: '',
              title_uz: '',
            },]}))
          }}
          type='button'
          className='d-flex align-items-center justify-content-center '
          style={{width:"30px", height:'30px',border: 'none',borderRadius:"50%", outline: 'none', background: `${theme === 'light' ? '' :'white'}`, marginBottom: '20px'}}
        >
          <img src={plusIcon} alt="plus" width={30} height={30} style={{objectFit:'cover'}} />
        </button>
      </>

      <div className='row d-flex align-items-center  px-5'>
        <div
          className='col-md-4 d-flex align-items-center position-relative  justify-content-center form-control-solid form-control form-control-lg w-25'
          style={{height: '200px', cursor: 'pointer'}}
        >
          <input
            type='file'
            name='image'
            onChange={hanldechangeImg}
            accept='image/*'
            className='opacity-0 position-absolute w-100 h-100'
            style={{cursor: 'pointer'}}
          />
          <img src={uploadimg} alt="upload" className='w-100 h-100' style={{objectFit:'contain'}} />
        </div>
        {preview.previewURL && (
          <div className='col-md-4'>
            <img
              src={preview.previewURL}
              alt='uploaded img'
              style={{maxWidth: '100%', maxHeight: '200px', marginTop: '10px'}}
            />
          </div>
        )}

        {aboutSchema?.image && !preview.previewURL && (
          <div className='col-md-4'>
            <img width={200} height={200} src={aboutSchema.image} alt='about' />
          </div>
        )}
      </div>

      {locale === 'uz' ? (
        <button
          className={`btn ${loading ? 'btn-secondary' : 'btn-dark'} mt-10 ml-3 `}
          style={{padding: '1rem', width: '120px'}}
          disabled={loading}
        >
          {loading ? 'Jonatilmoqda....' : 'Jonatish'}
        </button>
      ) : (
        <button
          className={`btn ${loading ? 'btn-secondary' : 'btn-dark'} mt-10 ml-3 `}
          style={{padding: '1rem', width: '120px'}}
          disabled={loading}
        >
          {loading ? 'отправляться....' : 'отправлять'}
        </button>
      )}
    </form>
  )
}

export default AboutPage
