import {useEffect, useState} from 'react'
import {toast} from 'react-toastify'
import {
  useEditLayoutMutation,
  useGetContactDataQuery,
} from '../../../../redux/features/layout/layoutApi'
import uploadimg from '../../../../_metronic/assets/upload.png'
import {useLang} from '../../../../_metronic/i18n/Metronici18n'

const ContactFormPage = () => {
  const [editLayout, {isSuccess, isError, isLoading}] = useEditLayoutMutation()
  const locale = useLang()

  const {data, refetch} = useGetContactDataQuery(undefined, {refetchOnMountOrArgChange: true})

  const [loading, setLoading] = useState(false)

  const [preview, setPreviw] = useState({
    previewURL: '',
  })

  const [contact, setContact] = useState({
    image: '',
    title_ru: '',
    title_uz: '',
    description_uz: '',
    description_ru: '',
  })

  const handleChangeValue = (e) => {
    setContact(p => ({...p, [e.target.name]:e.target.value}))
  }

  const handleSubmit = async (e) => {
    try {
      e.preventDefault()
      setLoading(true)
      const formData = new FormData()

      formData.append('image', contact.image)
      formData.append('title_ru', contact.title_ru)
      formData.append('title_uz', contact.title_uz)
      formData.append('description_uz', contact.description_uz)
      formData.append('description_ru', contact.description_ru)

      await editLayout({data: formData, type: 'contact'})
      
      setTimeout(() => {
        setContact({
          image: '',
          title_ru: '',
          title_uz: '',
          description_uz: '',
          description_ru: '',
        })
        setPreviw({
          previewURL: '',
        })
        setLoading(false)
        refetch()
      }, 1000)
    } catch (err) {
      setLoading(false)
    }
  }

  const hanldechangeImg = (e) => {
    const file = e.target.files[0]
    setPreviw({
      previewURL: file ? URL?.createObjectURL(file) : '',
    })
    setContact((p) => ({...p, [e.target.name]: file}))
  }

  useEffect(() => {
    setContact((p) => ({...data?.layout?.contact}))
  }, [data])

  useEffect(() => {
    if (isSuccess) {
      toast.success(
        locale == 'uz'
          ? 'Qayta Aloqq Forma rasmi yangilandi'
          : 'Обновлено изображение контактной формы.'
      )
    }

    if (isError) {
      toast.error(
        locale == 'uz'
          ? 'Qayta Aloqa Forma rasmi yangilanishda xatolik yuz berdi'
          : 'Произошла ошибка при обновлении изображения контактной формы.'
      )
    }
  }, [isSuccess, isError])

  return (
    <>
      {isLoading ? (
        <h1>loading...</h1>
      ) : (
        <form onSubmit={handleSubmit}>
          <h1 className='mb-20'>
            {locale === 'uz' ? 'Qayta aloqa forma rasmi' : 'Изображение формы обратной связи'}
          </h1>

          <div className='row mb-10 px-3'>
            <div className='col-md-6 d-flex flex-column gap-2'>
              <label htmlFor='title_ru' className='fs-3'>
                title_ru {locale === 'uz' ? '(Qayta aloqa forma Sarlhovhasi  Ruscha)' : '(Форма обратной связи Название на русском языке )'}
              </label>
              <input
                type='text'
                id='title_ru'
                name='title_ru'
                value={contact.title_ru}
                onChange={handleChangeValue}
                className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
              />
            </div>

            <div className='col-md-6 d-flex flex-column gap-2'>
              <label htmlFor='title_uz' className='fs-3'>
                title_uz {locale === 'uz' ? '(Qayta aloqa forma Sarlhovhasi  Uzbekcha)' : '(Форма обратной связи Название на Узбекский)'}
              </label>
              <input
                type='text'
                id='title_uz'
                name='title_uz'
                required
                value={contact.title_uz}
                onChange={handleChangeValue}
                className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
              />
            </div>
          </div>

          
          <div className='row mb-10 px-3'>
            <div className='col-md-6 d-flex flex-column gap-2'>
              <label htmlFor='description_uz' className='fs-3'>
                description_uz  {locale === 'uz' ? '(Qayta aloqa forma tarifi uzbekchada)' : '(описание  формы обратной связи Узбекский)'}
              </label>
              <input
                type='text'
                id='description_uz'
                name='description_uz'
                value={contact.description_uz}
                onChange={handleChangeValue}
                className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
              />
            </div>

            <div className='col-md-6 d-flex flex-column gap-2'>
              <label htmlFor='description_ru' className='fs-3'>
                description_ru  {locale === 'uz' ? '(Qayta aloqa forma tarifi ruschada)' : '(описание формы обратной  связи  русском)'}
              </label>
              <input
                type='text'
                id='description_ru'
                name='description_ru'
                value={contact.description_ru}
                onChange={handleChangeValue}
                className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
              />
            </div>
          </div>

          <div className='row d-flex px-5'>
            <div
              className='col-md-6 d-flex align-items-center position-relative  justify-content-center form-control-solid form-control form-control-lg w-25'
              style={{height: '200px', cursor: 'pointer'}}
            >
              <input
                type='file'
                name='image'
                onChange={hanldechangeImg}
                accept='image/*'
                className='opacity-0 position-absolute w-100 h-100'
                style={{cursor: 'pointer'}}
              />
          <img src={uploadimg} alt="upload" className='w-100 h-100' style={{objectFit:'contain'}} />
            </div>

            <div className='col-md-6'>
              {preview?.previewURL && (
                <div className='col-md-4'>
                  <img
                    src={preview.previewURL}
                    alt='uploaded img'
                    style={{width: '100%', height: '200px', objectFit: 'cover', marginTop: '10px'}}
                  />
                </div>
              )}

              {data?.layout?.contact?.image && !preview.previewURL && (
                <div className='col-md-4'>
                  <img width={200} height={200} src={data?.layout?.contact?.image} alt='contact-form' />
                </div>
              )}
            </div>
          </div>

          {locale === 'uz' ? (
            <button
              className={`btn ${loading ? 'btn-secondary' : 'btn-dark'} mt-10 ml-3 `}
              style={{padding: '1rem', width: '120px'}}
              disabled={loading}
            >
              {loading ? 'Jonatilmoqda....' : 'Jonatish'}
            </button>
          ) : (
            <button
              className={`btn ${loading ? 'btn-secondary' : 'btn-dark'} mt-10 ml-3 `}
              style={{padding: '1rem', width: '120px'}}
              disabled={loading}
            >
              {loading ? 'отправляться....' : 'отправлять'}
            </button>
          )}
        </form>
      )}
    </>
  )
}

export default ContactFormPage
