import {useEffect, useState} from 'react'
import {
  useEditLayoutMutation,
  useGetHeroLayoutByTypeQuery,
} from '../../../../redux/features/layout/layoutApi'
import './style.css'
import plusIcon from '../../../../_metronic/assets/plus.png'
import uploadimg from '../../../../_metronic/assets/upload.png'
import {toast} from 'react-toastify'
import { useLang } from '../../../../_metronic/i18n/Metronici18n'
import trashIcon from '../../../../_metronic/assets/trash.png'


type Props = {}

const HeroPage = (props: Props) => {
  const locale = useLang()

  const theme = localStorage.getItem('kt_theme_mode_menu')
  const [loading, setLoading] = useState(false)
  const {data, isSuccess, isLoading, refetch} = useGetHeroLayoutByTypeQuery(undefined, {
    refetchOnMountOrArgChange: true,
  })

  const [editLayout, {isSuccess: layoutSuccess, isError}] = useEditLayoutMutation()

  const [heroschema, setHeroSchema] = useState({
    image: '',
    title_ru: '',
    title_uz: '',
    subtitle_ru: '',
    subtitle_uz: '',
    hero_services_uz: [''],
    hero_services_ru: [''],
  })

  const [preview, setPreviw] = useState({
    previewURL: '',
  })


  

  useEffect(() => {
    if (data?.layout?.hero) {
      setHeroSchema({...data?.layout?.hero})
    }
  }, [isSuccess, refetch])

  const handlechange = (e) => {
    setHeroSchema((p) => ({...p, [e.target.name]: e.target.value}))
  }
  const hanldechangeImg = (e) => {
    const file = e.target.files[0]
    setPreviw({
      previewURL: file ? URL?.createObjectURL(file) : '',
    })
    setHeroSchema((p) => ({...p, [e.target.name]: file}))
  }
  const hanldeSubmit = async (e) => {
    e.preventDefault()
    setLoading(true)
    const formData = new FormData()

    Object.keys(heroschema).forEach((key) => {
      if (Array.isArray(heroschema[key])) {
        heroschema[key].forEach((item, index) => {
          formData.append(`${key}[${index}]`, item)
        })
      } else {
        formData.append(key, heroschema[key])
      }
    })

    await editLayout({data: formData, type: 'hero'})

    setTimeout(() => {
      setLoading(false)
      refetch()
      // setPreviw({
      //   previewURL: '',
      // })
      // window.location.reload()
    }, 1000)
  }

  const handleSubmitItem = async (e) => {
    e.preventDefault()
    setLoading(true)
    const formData = new FormData()

    Object.keys(heroschema).forEach((key) => {
      if (Array.isArray(heroschema[key])) {
        heroschema[key].forEach((item, index) => {
          formData.append(`${key}[${index}]`, item)
        })
      } else {
        formData.append(key, heroschema[key])
      }
    })

    await editLayout({data: formData, type: 'hero'})

    setTimeout(() => {
      setLoading(false)
      refetch()
      setPreviw({
        previewURL: '',
      })
      window.location.reload()
    }, 1000)
  }

  

  useEffect(() => {
    if (layoutSuccess) {
      toast.success(locale === 'uz' ? 'Hero Sahifasi muovfaqiyatli yangilandi':'Hero успешно обновлен')
    }

    if (isError) {
      toast.error(locale === 'uz' ? "Hero Sahifani yanglishada Xatolik Yuz Berdi": "Произошла ошибка обновления")
    }
  }, [layoutSuccess, isError])

  const hanldeDelete = (idx, type) => {
    if (type === 'serv_uz') {
      const newServ = heroschema.hero_services_uz.filter((item, index) => index !== idx)
      setHeroSchema({
        ...heroschema,
        hero_services_uz: newServ,
      })
    }

    if (type === 'serv_ru') {
      const newServ = heroschema.hero_services_ru.filter((item, index) => index !== idx)
      setHeroSchema({
        ...heroschema,
        hero_services_ru: newServ,
      })
    }
  }

  return (
    <>
      {isLoading ? (
        <h1>Loading...</h1>
      ) : (
        <>
          <h1 style={{margin: '70px 10px'}}>{locale === 'uz' ? 'Heroni sahifani o`zgartirish ' :'Редактировать страницy hero'}</h1>
          <form className='mt-6' onSubmit={hanldeSubmit}>
            <div className='row mb-10 px-3'>
              <div className='col-md-6 d-flex flex-column gap-2'>
                <label htmlFor='title_ru' className='fs-3'>
                  title_ru {locale === 'uz' ? '(Heroni sarlovhasi Ruscha)' :'(Hero заголовок)'}
                </label>
                <input
                  type='text'
                  id='title_ru'
                  name='title_ru'
                  value={heroschema.title_ru}
                  onChange={handlechange}
                  required
                  className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
                />
              </div>

              <div className='col-md-6 d-flex flex-column gap-2'>
                <label htmlFor='title_uz' className='fs-3'>
                  title_uz {locale === 'uz' ? '(Heroni sarlovhasi Uzbekcha)' :'(Hero заголовок Узбекский)'}
                </label>
                <input
                  type='text'
                  id='title_uz'
                  name='title_uz'
                  value={heroschema.title_uz}
                  onChange={handlechange}
                  required
                  className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
                />
              </div>
            </div>

            <div className='row mb-10 px-3'>
              <div className='col-md-6 d-flex flex-column gap-2'>
                <label htmlFor='subtitle_ru' className='fs-3'>
                  subtitle_ru {locale === 'uz' ? '(Heroni sarlovhasi 2 Ruscha)' :'(Hero Подзаголовок Русский)'}
                </label>
                <textarea
                  id='subtitle_ru'
                  name='subtitle_ru'
                  value={heroschema.subtitle_ru}
                  onChange={handlechange}
                  required
                  className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
                />
              </div>

              <div className='col-md-6 d-flex flex-column gap-2'>
                <label htmlFor='subtitle_uz' className='fs-3'>
                  subtitle_uz {locale === 'uz' ? '(Heroni sarlovhasi 2 Uzbekcha)' :'(Hero Подзаголовок Узбекский)'}
                </label>
                <textarea
                  id='subtitle_uz'
                  name='subtitle_uz'
                  value={heroschema.subtitle_uz}
                  onChange={handlechange}
                  required
                  className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
                />
              </div>
            </div>

            <div className='row'>
              <div className='col-md-6'>
                {heroschema?.hero_services_ru?.map((item, index) => (
                  <div className='col-md-6 mt-6 d-flex flex-column gap-2' key={index}>
                    <label htmlFor='subtitle_uz' className='fs-3'>
                      hero services ru {index + 1} {locale === 'uz' ? '(Hero xizmatlar)' :'(Hero услуги)'}
                    </label>
                    <input
                      type='text'
                      id='subtitle_uz'
                      name='subtitle_uz'
                      value={item}
                      onChange={(e) => {
                        setHeroSchema((prevServices: any) => {
                          return {
                            ...prevServices,
                            hero_services_ru: prevServices.hero_services_ru?.map(
                              (el: any, i: number) => {
                                if (i === index) {
                                  return e.target.value
                                } else {
                                  return el
                                }
                              }
                            ),
                          }
                        })
                      }}
                      required
                      className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
                    />
                  </div>
                ))}
              </div>

              <div className='col-md-6'>
                {heroschema?.hero_services_uz?.map((item, index) => (
                  <div className='col-md-6 mt-6 d-flex flex-column gap-2' key={index}>
                    <label htmlFor='subtitle_uz' className='fs-3'>
                      hero services uz {index + 1} {locale === 'uz' ? '(Hero xizmatlar)' :'(Hero услуги)'}
                    </label>
                    <input
                      id='subtitle_uz'
                      // name='subtitle_uz'
                      type='text'
                      value={item}
                      onChange={(e) => {
                        setHeroSchema((prevServices: any) => {
                          return {
                            ...prevServices,
                            hero_services_uz: prevServices.hero_services_uz?.map(
                              (el: any, i: number) => {
                                if (i === index) {
                                  return e.target.value
                                } else {
                                  return el
                                }
                              }
                            ),
                          }
                        })
                      }}
                      required
                      className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
                    />
                  </div>
                ))}
              </div>
            </div>

            <div className='row d-flex align-items-center  mt-10 px-5'>
              <div
                className='col-md-4 d-flex align-items-center position-relative  justify-content-center form-control-solid form-control form-control-lg w-25'
                style={{height: '200px', cursor: 'pointer'}}
              >
                <input
                  type='file'
                  name='image'
                  accept='image/*'
                  onChange={hanldechangeImg}
                  className='opacity-0 position-absolute w-100 h-100'
                  style={{cursor: 'pointer'}}
                />
          <img src={uploadimg} alt="upload" className='w-100 h-100' style={{objectFit:'contain'}} />
              </div>
              {preview.previewURL && (
                <div className='col-md-4'>
                  <img
                    src={preview.previewURL}
                    alt='uploaded img'
                    style={{maxWidth: '100%', maxHeight: '200px', marginTop: '10px'}}
                  />
                </div>
              )}

              {heroschema?.image && !preview.previewURL && (
                <div className='col-md-8'>
                  <img width={'300px'} height={'300px'} src={heroschema?.image} alt='advantages' />
                </div>
              )}
            </div>

            <button
              className={`btn ${loading ? 'btn-secondary' : 'btn-dark'} mt-10 ml-3 `}
              style={{padding: '1rem', width: '120px'}}
              disabled={loading}
            >
              {loading ? 'Sending....' : 'Send'}
            </button>
          </form>

          <div
            style={{
              width: '100%',
              height: '1px',
              background: 'white',
              opacity: '0.5',
              margin: '30px 0px',
            }}
          ></div>

          <h1 style={{margin: '70px 10px'}}>{locale === 'uz' ? 'Xizmatlarni o`chirish va yaratish' :'Создание и удаление сервисов '}</h1>

          <form className='mb-16' onSubmit={handleSubmitItem}>
            <div className='row'>
              <div className='col-md-6'>
                {heroschema?.hero_services_ru?.map((item, index) => (
                  <div className=' d-flex align-items-center'>
                    <div className='mt-6 d-flex flex-column gap-2' key={index}>
                      <input
                        type='text'
                        id='subtitle_uz'
                        name='subtitle_uz'
                        value={item}
                        onChange={(e) => {
                          setHeroSchema((prevServices: any) => {
                            return {
                              ...prevServices,
                              hero_services_ru: prevServices?.hero_services_ru?.map(
                                (el: any, i: number) => {
                                  if (i === index) {
                                    return e.target.value
                                  } else {
                                    return el
                                  }
                                }
                              ),
                            }
                          })
                        }}
                        required
                        className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
                      placeholder='Add Some text...'

                      />
                    </div>
                    <button
                      onClick={() => hanldeDelete(index, 'serv_ru')}
                      type='button'
                      className='mt-4 ml-4'
                      style={{outline: 'none', background: `${theme === 'light' ? 'transparent': 'white'}`, border: 'none', borderRadius:'50%', padding:'5px'}}
                    >
                      <img src={trashIcon} alt="trash" style={{width:'30px', height:"30px"}} />
                    </button>
                  </div>
                ))}
              </div>

              <div className='col-md-6'>
                {heroschema?.hero_services_uz?.map((item, index) => (
                  <div className=' d-flex align-items-center'>
                    <div className='mt-6 d-flex flex-column gap-2' key={index}>
                      <input
                        type='text'
                        id='subtitle_uz'
                        name='subtitle_uz'
                        value={item}
                        onChange={(e) => {
                          setHeroSchema((prevServices: any) => {
                            return {
                              ...prevServices,
                              hero_services_uz: prevServices.hero_services_uz?.map(
                                (el: any, i: number) => {
                                  if (i === index) {
                                    return e.target.value
                                  } else {
                                    return el
                                  }
                                }
                              ),
                            }
                          })
                        }}
                        required
                        className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
                      placeholder='Add Some text...'

                      />
                    </div>
                    <button
                      onClick={() => hanldeDelete(index, 'serv_uz')}
                      type='button'
                      className='mt-4'
                      style={{outline: 'none', background: `${theme === 'light' ? 'transparent': 'white'}`, border: 'none', borderRadius:'50%', padding:'5px'}}
                    >
                      <img src={trashIcon} alt="trash" style={{width:'30px', height:"30px"}} />
                    </button>
                  </div>
                ))}
              </div>
              <button
                type='button'
                className='d-flex align-items-center justify-content-center '
                style={{width:"30px", height:'30px',border: 'none',borderRadius:"50%", outline: 'none', background: `${theme === 'light' ? '' :'white'}`,  margin: '30px 10px',}}
      
                onClick={() => {
                  setHeroSchema((prev: any) => {
                    return {
                      ...prev,
                      hero_services_ru: [...heroschema.hero_services_ru, ''],
                      hero_services_uz: [...heroschema.hero_services_uz, ''],
                    }
                  })
                }}
              >
              <img src={plusIcon} alt="plus" width={30} height={30} style={{objectFit:'cover'}} />

              </button>
              
            </div>

            {
          locale === 'uz'
          ?
          <button className={`btn ${loading ? 'btn-secondary' : 'btn-dark'} mt-10 ml-3 `} style={{padding:'1rem', width:"120px"}} disabled={loading}>
          {loading ? 'Jonatilmoqda....' : 'Jonatish'}
          </button>
          : 
          <button className={`btn ${loading ? 'btn-secondary' : 'btn-dark'} mt-10 ml-3 `} style={{padding:'1rem', width:"120px"}} disabled={loading}>
          {loading ? 'отправляться....' : 'отправлять'}
          </button>

            }
          </form>
        </>
      )}
    </>
  )
}

export default HeroPage

/*
 */
