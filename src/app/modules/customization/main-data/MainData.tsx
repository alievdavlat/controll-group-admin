import  { useEffect, useState } from 'react'
import { useLang } from '../../../../_metronic/i18n/Metronici18n'
import uploadimg from '../../../../_metronic/assets/upload.png'
import { useEditLayoutMutation, useGetMainDataLayoutByTypeQuery, useGetTitlesLayoutByTypeQuery } from '../../../../redux/features/layout/layoutApi'
import { toast } from 'react-toastify'
import plusIcon from '../../../../_metronic/assets/plus.png'
import trashIcon from '../../../../_metronic/assets/trash.png'


const MainData = () => {
  const locale = useLang()
  const theme = localStorage.getItem('kt_theme_mode_menu')


  const {data:mainData, isSuccess:MainDataSuccess, isLoading:mainDataLoading, refetch:mainDataRefetch} = useGetMainDataLayoutByTypeQuery(undefined, {
    refetchOnMountOrArgChange: true,
  })

  const {data:titlesData, isSuccess:titlesSucess, isLoading:titlesLoading, refetch:titlesRefetch} = useGetTitlesLayoutByTypeQuery(undefined, {
    refetchOnMountOrArgChange: true,
  })
  const [editLayout, {isSuccess, isError}] = useEditLayoutMutation()


  const [loading, setLoading] = useState(false)


  const [mainSchema, setMainSchema] = useState({
    primary_phone:'',
    secondary_phone:'',
    address_uz:'',
    address_ru:'',
    socials:[
      {
        icon:'',
        path:'',
        name:''
      },

    ]

  }) 

  const [titlesSchema, setTitleschema] = useState({
    hero: {
      title_ru: '',
      title_uz: '',
      label_1_uz: 'Asosiy sarlovha ruschda',
      label_1_ru:'главное заголовокa на русском',
      label_2_uz: 'Asosiy sarlovha uzbekchada',
      label_2_ru:'главное заголовокa на узбекском ',
    },
    about: {
      title_ru: '',
      title_uz: '',
      label_1_uz: 'Biz xaqimizda qismining sarlovhasi ruschada',
      label_1_ru:'заголовокa раздела о нас на русском .',
      label_2_uz: 'Biz xaqimizda qismining sarlovhasi uzbekchada',
      label_2_ru:'заголовокa раздела о нас на узбекском',
    },
    services: {
      title_ru: '',
      title_uz: '',
      label_1_uz: 'Xizmatlar qismining sarlovhasi ruschada',
      label_1_ru:'заголовокa раздела услуг на русском',
      label_2_uz: 'Xizmatlar qismining sarlovhasi uzbekchada',
      label_2_ru:'заголовокa раздела услуг на узбекском ',
    },
    advantage: {
      title_ru: '',
      title_uz: '',
      label_1_uz: 'Ustunliklar qismining sarlovhasi ruschada',
      label_1_ru:'заголовокa раздела преимуществ на русском',
      label_2_uz: 'Ustunliklar qismining sarlovhasi uzbekchada',
      label_2_ru:'заголовокa раздела преимуществ узбекском ',
    },
    projects: {
      title_ru: '',
      title_uz: '',
      label_1_uz: 'Loyihalar qismining sarlovhasi ruschada',
      label_1_ru:'заголовокa раздела проекта на русском .',
      label_2_uz: 'Loyihalar qismining sarlovhasi uzbekchada',
      label_2_ru:'заголовокa раздела проекта на узбекском .',
    },
    stocks: {
      title_ru: '',
      title_uz: '',
      label_1_uz: 'Aksiyalar qismining sarlovhasi ruschada',
      label_1_ru:'заголовокa раздела акции на русском ',
      label_2_uz: 'Loyihalar qismining sarlovhasi uzbekchada',
      label_2_ru:'заголовокa раздела акции на узбекском',
    },
    faqs: {
      title_ru: '',
      title_uz: '',
      label_1_uz: 'Savol va javoblar  qismining sarlovhasi ruschada',
      label_1_ru:'заголовокa раздела вопросов и ответов на русском.',
      label_2_uz: 'Savol va javoblar  qismining sarlovhasi uzbekchada',
      label_2_ru:'заголовокa раздела вопросов и ответов на узбекском.',
    },
    reviws: {
      title_ru: '',
      title_uz: '',
      label_1_uz: 'Izohlar qismining sarlovhasi ruschada',
      label_1_ru:'заголовокa раздела отзывы на русском.',
      label_2_uz: 'Izohlar qismining sarlovhasi uzbekchada',
      label_2_ru:'заголовокa раздела отзывы на узбекском.',
    }
  }
  )

  const [preview, setPreviw] = useState({
    previewURL: '',
  })

  const handleAddSocialItem = () => {
    const newSocialItem = {
      icon: '',
      path: '',
      name: '',
    };

    setMainSchema((prevState) => ({
      ...prevState,
      socials: [...prevState.socials, newSocialItem],
    }));
  };

  const handleDeleteSocialItem = (index:number) => {
    setMainSchema((prevState) => {
      const updatedSocials = [...prevState.socials];
      updatedSocials.splice(index, 1);
      return {
        ...prevState,
        socials: updatedSocials,
      };
    });
  };

  const handleChange = (e:any) => {

    setMainSchema((prevState) => ({
      ...prevState,
      [e.target.name]: e.target.value,
    }));
  };

  const handleChangeTitles = (section:any, field:any, value:any) => {
    setTitleschema((prevState) => ({
      ...prevState,
      [section]: {
        ...prevState[section],
        [field]: value,
      },
    }));
  };


  const handleSocialItemChange = (e:any, index:number) => {
    setMainSchema((prevState) => {
      const updatedSocials = [...prevState.socials];
      updatedSocials[index] = {
        ...updatedSocials[index],
        [e.target.name]: e.target.value,
      };
      return {
        ...prevState,
        socials: updatedSocials,
      };
    });
  };

  const handleSocialItemIconChange = (e:any, index:number) => {
    setMainSchema((prevState) => {
      const file = e.target.files[0]
      setPreviw({
        previewURL: file ? URL?.createObjectURL(file) : '',
      })
      const updatedSocials = [...prevState.socials];
      updatedSocials[index] = {
        ...updatedSocials[index],
        [e.target.name]: file,
      };
      return {
        ...prevState,
        socials: updatedSocials,
      };
    });
  };


  const hadnleSubmitMainData  = async (e:any) => {
    e.preventDefault()
    try {
      await editLayout({data:mainSchema, type: 'main'})
      
      setLoading(true)
      setTimeout(() => {
        setLoading(false)
        mainDataRefetch()
      }, 1000)
      
    } catch (err) {
      setLoading(false)
    }

  }

  const hadnleSubmitTitles  = async (e:any) => {
    e.preventDefault()
    
    try {
      await editLayout({data:titlesSchema, type: 'titles'})
      
      
      setLoading(true)
      setTimeout(() => {
        setLoading(false)
        titlesRefetch()
      }, 1000)
      
    } catch (err) {
      setLoading(false)
    }

  }

  useEffect(() => {
      setMainSchema(mainData?.layout?.main)
      setTitleschema(titlesData?.layout?.titles)
  }, [mainData, mainDataRefetch, MainDataSuccess, titlesSucess, titlesData, titlesRefetch])


  useEffect(() => {
    if (isSuccess) {
      toast.success(locale === 'uz' ? 'malumotlar muovfaqiyatli yangilandi':' успешно обновлен')
    }

    if (isError) {
      toast.error(locale === 'uz' ? "malumotlarni yanglishada Xatolik Yuz Berdi": "Произошла ошибка обновления")
    }
    
  }, [isSuccess, isError])


  return (
    <div className='row' >


      {
      !mainDataLoading && 
      <form  className='col-md-12' onSubmit={hadnleSubmitMainData}>
      
        <h1 style={{margin:'20px 0px 40px 10px '}}>
          {locale == 'uz' ? 'Asosiy ma\'lumotlar':'Основные данные'}
        </h1>

        <div className='row mb-10 px-3'>
                
                <div className='col-md-6 d-flex flex-column gap-2'>
                  <label htmlFor='primary_phone' className='fs-3'>
                    {locale === 'uz' ? 'Asosiy telefon raqami':'Основной номер телефона'}
                  </label>
                  <input
                    type='text'
                    id='primary_phone'
                    name='primary_phone'
                    required
                    value={mainSchema?.primary_phone}
                    onChange={handleChange}
                    placeholder={locale === 'uz' ? 'Asosiy telefon raqami':'Основной номер телефона'}
                    className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
                  />
                </div>

                <div className='col-md-6 d-flex flex-column gap-2'>
                  <label htmlFor='secondary_phone' className='fs-3'>
                  {locale === 'uz' ? 'Ikkinchi telifon raqam':'Второй номер телефона'}
                  </label>
                  <input
                    type='text'
                    id='secondary_phone'
                    name='secondary_phone'
                    required
                    value={mainSchema?.secondary_phone}
                    onChange={handleChange}
                    placeholder = {locale === 'uz' ? 'Ikkinchi telifon raqam':'Второй номер телефона'}
                    className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
                  />
                </div>

        </div>
       
        <div className='row mb-10 px-3'>
                
              <div className='col-md-6 d-flex flex-column gap-2'>
                  <label htmlFor='address_ru' className='fs-3'>
                    {locale === 'uz' ? 'Manzil ruschada':'Адрес на русском языке..'}
                  </label>
                  <textarea
                    id='address_ru'
                    name='address_ru'
                    required
                    value={mainSchema?.address_ru}
                    onChange={handleChange}
                    rows={10}
                    placeholder={locale === 'uz' ? 'Manzil ruschada':'Адрес на русском языке..'}
                    className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
                  />
              </div>
              
              <div className='col-md-6 d-flex flex-column gap-2'>
                  <label htmlFor='address_uz' className='fs-3'>
                  {locale === 'uz' ? 'Manzil uzbekchada..':'Адрес на узбекском языке..'}
                  </label>
                  <textarea
                    id='address_uz'
                    name='address_uz'
                    required
                    value={mainSchema?.address_uz}
                    onChange={handleChange}
                    rows={10}
                    placeholder={locale === 'uz' ? 'Manzil uzbekchada..':'Адрес на узбекском языке..'}
                    className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
                  />
              </div>
        </div>
        
        <div>
          {
            mainSchema?.socials?.map((item, index:number) => (
              <div className='row mb-10 px-3'>
                
                  <div className='col-md-6 d-flex flex-column gap-2'>

                    {
                      item?.name 
                      ? <label htmlFor='path' className='fs-3'>
                        {locale === 'uz' ? `${item?.name} linki`:`Ссылка на ${item?.name}`}
                      </label>
                      : <label htmlFor='path' className='fs-3'>
                      {locale === 'uz' ? 'Ijtmoiy tarmoq linki':'Ссылка на соцсети'}
                      </label>
                    }

                    <input
                      type='text'
                      id='path'
                      name='path'
                      required
                      value={item?.path}
                      onChange={(e) => handleSocialItemChange(e, index)}
                      placeholder = {locale === 'uz' ? 'Ijtmoiy tarmoq linki':'Ссылка на соцсети'}
                      className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
                    />
                  </div>
                
                  <div className='col-md-6 d-flex flex-column gap-2'>
                  
                  {
                        item?.name 
                        ?  <label htmlFor='name' className='fs-3'>
                          {locale === 'uz' ? `${item?.name}` : `${item?.name}`}
                        </label>
                        : <label htmlFor='name' className='fs-3'>
                          {locale === 'uz' ? 'Ijtmoiy tarmoq nomi':'Название социальной сети'}
                        </label>
                      }

                    
                    <input
                      type='text'
                      id='name'
                      name='name'
                      required
                      value={item?.name}
                      onChange={(e) => handleSocialItemChange(e, index)}
                      placeholder = {locale === 'uz' ? 'Ijtmoiy tarmoq nomi':'Название социальной сети'}
                      className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
                    />
                  </div>
                  
                  {/* <div className='row d-flex align-items-center  mt-10 px-5'>
                      <div
                        className='col-md-4 d-flex align-items-center position-relative  justify-content-center form-control-solid form-control form-control-lg w-25'
                        style={{height: '200px', cursor: 'pointer'}}
                      >
                        <input
                          type='file'
                          name='icon'
                          accept='image/*'
                          onChange={(e) => handleSocialItemIconChange(e,index)}
                          className='opacity-0 position-absolute w-100 h-100'
                          style={{cursor: 'pointer'}}
                        />
                      <img src={uploadimg} alt="upload" className='w-100 h-100' style={{objectFit:'contain'}} />
                      
                      </div>
                      {preview.previewURL && (
                        <div className='col-md-4'>
                          <img
                            src={preview.previewURL}
                            alt='uploaded img'
                            style={{maxWidth: '100%', maxHeight: '200px', marginTop: '10px'}}
                          />
                        </div>
                      )}

                      {item.icon && !preview.previewURL && (
                        <div className='col-md-8'>
                          <img width={'300px'} height={'300px'} src={item?.icon} alt='main-icon' />
                        </div>
                      )}
                    {
                      index !== 0 &&
                    <button
                    onClick={() => handleDeleteSocialItem(index)}
                    type='button'
                    className='mt-4 d-flex align-items-center justify-content-center'
                    style={{outline: 'none', background: `${theme === 'light' ? 'transparent': 'white'}`, border: 'none', borderRadius:'50%', padding:'5px', width:'30px', height:"30px",margin:'0px 0px 0px 30px'}}
                  >
                    <img src={trashIcon} alt="trash" style={{width:'20px', height:"20px"}} />
                  </button> 
                    }
                  </div> */}
                
          </div>
            ))
          }

              <button
                type='button'
                className='d-flex align-items-center justify-content-center '
                style={{width:"30px", height:'30px',border: 'none',borderRadius:"50%", outline: 'none', background: `${theme === 'light' ? '' :'white'}`,  margin: '30px 10px',}}
                onClick={handleAddSocialItem}
                >
                <img src={plusIcon} alt="plus" width={30} height={30} style={{objectFit:'cover'}} />
            </button>
        </div>


        <button
          className={`btn ${loading ? 'btn-secondary' : 'btn-dark'} mt-10 mb-10 ml-10 `}
          style={{padding: '1rem', width: '120px'}}
          disabled={loading}
        >
          {loading ? 'Sending....' : 'Send'}
        </button>
      </form>
      }

        <br />
        <hr />
        <br />
      {
      !titlesLoading &&

      <form  className='col-md-12' onSubmit={hadnleSubmitTitles}>
     
      <h1 style={{margin:'20px 0px 40px 10px '}}>
        {locale == 'uz' ? 'Sarlovhalar':'Заголовоки'}
      </h1>

        <div className='row mb-10 px-3'>

        { titlesSchema && Object.keys(titlesSchema).map((section) => (
        <>
        <div className='col-md-6 d-flex flex-column gap-2 mt-6' key={section}>
        <label htmlFor='hero_title' className='fs-3'>
         {locale === 'uz' ? `${titlesSchema[section]?.label_1_uz}`:`${titlesSchema[section]?.label_1_ru}`} 
        </label>
        <input
          type='text'
          id='hero_title'
          name='hero_title'
          required
          value={titlesSchema[section].title_ru}
          onChange={(e) => handleChangeTitles(section, 'title_ru', e.target.value)}
          placeholder={locale === 'uz' ? `${titlesSchema[section]?.label_1_uz}`:`${titlesSchema[section]?.label_1_ru}`}
          className='form-control form-control-lg form-control-solid mt-3 mb-6 mb-lg-0'
          />
      </div>

      <div className='col-md-6 d-flex flex-column gap-2 mt-6' key={section}>
        <label htmlFor='hero_title' className='fs-3'>
        {locale === 'uz' ? `${titlesSchema[section]?.label_2_uz}`:`${titlesSchema[section]?.label_2_ru}`} 
        </label>
        <input
          type='text'
          id='hero_title'
          name='hero_title'
          required
          value={titlesSchema[section].title_uz}
          onChange={(e) => handleChangeTitles(section, 'title_uz', e.target.value)}
          placeholder={locale === 'uz' ? `${titlesSchema[section]?.label_2_uz}`:`${titlesSchema[section]?.label_2_ru}`}
          className='form-control form-control-lg form-control-solid mt-3 mb-6 mb-lg-0'
          />
      </div>
        </>
         ))}
        
        </div>

        <button
              className={`btn ${loading ? 'btn-secondary' : 'btn-dark'} mt-10 mb-10 ml-10 `}
              style={{padding: '1rem', width: '120px'}}
              disabled={loading}
            >
              {loading ? 'Sending....' : 'Send'}
        </button>
      </form>
      }

      
    </div>
  )
}

export default MainData