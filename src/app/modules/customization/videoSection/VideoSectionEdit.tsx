import { useEffect, useState } from 'react'
import { useDeleteLayoutMutation, useEditLayoutMutation, useGetVideoLayoutByTypeQuery } from '../../../../redux/features/layout/layoutApi'
import { useLang } from '../../../../_metronic/i18n/Metronici18n'
import uploadimg from '../../../../_metronic/assets/upload.png'
import { toast } from 'react-toastify'
import trashIcon from '../../../../_metronic/assets/trash.png'


const VideoSection = () => {
  const [deleteLayout, {isSuccess:deleteSuccess, isError:deleteError}] = useDeleteLayoutMutation()
  const locale = useLang()
  const [loading , setLoading ] = useState(false)
  const [preview, setPreviw] = useState({
    previewURL: '',
  })


  const [videSchema , setVideSchema] = useState({
    image:''
  })
  const {data , isSuccess, refetch} = useGetVideoLayoutByTypeQuery(undefined,{ refetchOnMountOrArgChange:true})
  const [editLayout, {isSuccess:editLayoutSuccess, isError}] = useEditLayoutMutation()
    
  const hanldechangeImg = (e) => {
      const file = e.target.files[0]
      setPreviw({
        previewURL: file ? URL?.createObjectURL(file) : '',
      })
      setVideSchema(p => ({...p, [e.target.name]:file}))
  }


  const hanldeSubmit = async (e) => {
    try {
      e.preventDefault()
      setLoading(true)     
      
      const fd = new FormData()

      fd.append('image', videSchema?.image)


      await editLayout({data:fd, type:'video'})

      setTimeout(() => {
        setLoading(false)
        setPreviw({
          previewURL: '',
        })
        refetch()
      }, 1000)
      

    } catch (err) {
      setLoading(false)
    }
  }

  const handleDeleteVideo = async (e) => {
   await deleteLayout({type:'video'})
   refetch()
  }

  useEffect(() => {

    if (data?.layout?.video && isSuccess) {
      setVideSchema(p => ({...p,image:data?.layout?.video}))
    }

  }, [isSuccess, data, refetch])

  
  useEffect(() => {
    if (editLayoutSuccess) {
      toast.success(locale === 'uz' ? 'Video muovfaqiyatli yangilandi' : 'видео успешно обновлено')
    }

    if (isError) {
      toast.error(locale === 'uz' ? 'Video yangilanishida xatolik yuz berdi' :'Произошла ошибка при обновлении видео.')
    }

    if (deleteSuccess) {
      toast.success(locale === 'uz' ? 'Video muovfaqiyatli o\'chirildi' : 'Видео успешно удалено')
    }
    if (deleteError) {
      toast.error(locale === 'uz' ? 'Videoni o\'chirishda  xatolik yuz berdi' :'Произошла ошибка при удалении видео.') 
    }
  } , [editLayoutSuccess, isError, deleteError, deleteSuccess])


  return (
    <form onSubmit={hanldeSubmit}>

    <h1 className='mb-10 fs-1'>
      {
        locale === 'uz' 
        ? 'Video Vidgetni yangilash '
        : 'Обновить виджет видео'
      }
    </h1>

    <div className="row">
    <div
        className='col-md-6 d-flex align-items-center position-relative  justify-content-center form-control-solid form-control form-control-lg w-25'
        style={{height: '300px', cursor: 'pointer'}}
      >
        <input
          type='file'
          name='image'
          onChange={hanldechangeImg}
          accept='video/mp4'
          className='opacity-0 position-absolute w-100 h-100'
          style={{cursor: 'pointer'}}
        />
          <img src={uploadimg} alt="upload" className='w-100 h-100' style={{objectFit:'contain'}} />
    </div>
      
        {
          preview?.previewURL && 
          <div className="col-md-6 position-relative" style={{height: '300px', width:"400px", overflow: 'hidden',backgroundSize: 'cover',backgroundRepeat: 'no-repeat',backgroundPosition: 'center center'}} > 
              {/* <div className='position-absolute px-1 py-1' style={{zIndex:'999', width:"30px", height:'30px', right:10, top:10, background:'white', borderRadius:'50%', cursor:'pointer'}}>
              <img src={trashIcon} alt="trash"  className='w-100 h-100'/>
              </div> */}
          <video controls  src={preview.previewURL}  style={{objectFit:'cover', width:"100%", height:'100%', position:'absolute'}}>
          </video>
        </div>
        }

        {
        data?.layout?.video && !preview.previewURL &&
          <div className="col-md-6 position-relative" style={{height: '300px', width:"400px", overflow: 'hidden',backgroundSize: 'cover',backgroundRepeat: 'no-repeat',backgroundPosition: 'center center'}} > 
              <div onClick={handleDeleteVideo} className='position-absolute px-1 py-1' style={{zIndex:'999', width:"30px", height:'30px', right:10, top:10, background:'white', borderRadius:'50%', cursor:'pointer'}}>
              <img src={trashIcon} alt="trash"  className='w-100 h-100'/>
            </div>
              <video controls  src={videSchema?.image}  style={{objectFit:'cover', width:"100%", height:'100%', position:'absolute'}}>
              </video>
          </div>
        }
    </div>

      {
        locale === 'uz'
        ?
    <button className={`btn ${loading ? 'btn-secondary' : 'btn-dark'} mt-10 ml-3 `} style={{padding:'1rem', width:"120px"}} disabled={loading}>
      {loading ? 'Jonatilmoqda....' : 'Jonatish'}
    </button>
    : 
    <button className={`btn ${loading ? 'btn-secondary' : 'btn-dark'} mt-10 ml-3 `} style={{padding:'1rem', width:"120px"}} disabled={loading}>
      {loading ? 'отправляться....' : 'отправлять'}
    </button>

    }

    </form>
  )
}

export default VideoSection