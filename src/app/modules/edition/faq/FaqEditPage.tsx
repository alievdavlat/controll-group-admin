import React, {useEffect, useState} from 'react'
import {toast} from 'react-toastify'
import {useNavigate, useParams} from 'react-router-dom'
import {useLang} from '../../../../_metronic/i18n/Metronici18n'
import {
  useEditFaqsMutation,
  useGetAllFaqsQuery,
  useGetOneFaqQuery,
} from '../../../../redux/features/faq/faqApi'
interface IFaqSchema {
  answer_uz: string
  answer_ru: string
  question_uz: string
  question_ru: string
}

const GalleryEditPage = () => {
  const [editFaqs, {isError, isSuccess}] = useEditFaqsMutation()
  const {refetch} = useGetAllFaqsQuery(undefined, {refetchOnMountOrArgChange: true})
  const locale = useLang()
  const [faqSchema, setFaqSchema] = useState<IFaqSchema>({
    answer_ru: '',
    answer_uz: '',
    question_ru: '',
    question_uz: '',
  })
  const navigate = useNavigate()
  const {id} = useParams()

  const {data, isLoading, isSuccess: faqSuccess} = useGetOneFaqQuery(id)

  const [loading, setLoading] = useState(false)

  console.log(faqSchema);
  

  const handleSubmit = async (e) => {
    try {
      e.preventDefault()
      setLoading(true)

      await editFaqs({data:faqSchema, id})

      setTimeout(() => {
        setTimeout(() => {
          setLoading(false)
          setFaqSchema({
            answer_ru: '',
            answer_uz: '',
            question_ru: '',
            question_uz: '',
          })
        }, 1000)
        setLoading(false)
        refetch()
        navigate('/dashboard/faq-list')
      }, 1000)
    } catch (err) {
      setLoading(false)
    }
  }

  const handleChangeValue = (e: any) => {
    setFaqSchema((p) => ({...p, [e?.target?.name]: e?.target?.value}))
  }

  useEffect(() => {
    setFaqSchema({...data?.faq})
  }, [data, faqSuccess])

  useEffect(() => {
    if (isSuccess) {
      toast.success(locale === 'uz' ? 'savol va javob  muovfaqiyatli o\'zgardi' : 'элемент Вопрось и ответь отредактирован')
    }

    if (isError) {
      toast.error(locale === 'uz' ? 'savol va javob  o`zgartirishda xatolik yuz berdi' : 'элемент Вопрось и ответь не редактируется')
    }
  }, [isSuccess, isError])

  return (
    <>
      {isLoading ? (
        <h1>loading...</h1>
      ) : (
        <form onSubmit={handleSubmit}>
          <h1 className='mb-10 mx-5'>
            {locale === 'uz' ? 'Savol va Javoblar yaratish' : 'Создавать вопросы и ответы'}
          </h1>
          <>
            <div className='row mb-10 px-3'>
              <div className='col-md-6 d-flex flex-column gap-2'>
                <label htmlFor='answer_ru' className='fs-3'>
                {locale ===  'uz'  ? "Javob Rus tilida":"ответь на русском"}
                </label>
                <input
                  type='text'
                  id='answer_ru'
                  name='answer_ru'
                  value={faqSchema.answer_ru}
                  onChange={handleChangeValue}
                  required
                  className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
                />
              </div>

              <div className='col-md-6 d-flex flex-column gap-2'>
                <label htmlFor='answer_uz' className='fs-3'>
                {locale ===  'uz'  ? "Javob Uzbek tilida":"ответь на Узбекский"}
                </label>
                <input
                  type='text'
                  id='answer_uz'
                  name='answer_uz'
                  value={faqSchema.answer_uz}
                  onChange={handleChangeValue}
                  required
                  className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
                />
              </div>
            </div>

            <div className='row mb-10 px-3'>
              <div className='col-md-6 d-flex flex-column gap-2'>
                <label htmlFor='question_ru' className='fs-3'>
                {locale === 'uz' ? "savol Ruschada":"вопрос Ha русски"}
                </label>
                <textarea
                  id='question_ru'
                  name='question_ru'
                  value={faqSchema.question_ru}
                  onChange={handleChangeValue}
                  required
                  cols={8}
                  rows={10}
                  className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
                />
              </div>

              <div className='col-md-6 d-flex flex-column gap-2'>
                <label htmlFor='question_uz' className='fs-3'>
                {locale === 'uz' ? "savol o'zbek tilida" :"вопрос на узбекском"}
                </label>
                <textarea
                  id='question_uz'
                  name='question_uz'
                  value={faqSchema.question_uz}
                  onChange={handleChangeValue}
                  required
                  cols={8}
                  rows={10} 
                  className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
                />
              </div>
            </div>
          </>

          {locale === 'uz' ? (
            <button
              className={`btn ${loading ? 'btn-secondary' : 'btn-dark'} mt-10 ml-3 `}
              style={{padding: '1rem', width: '120px'}}
              disabled={loading}
            >
              {loading ? 'Jonatilmoqda....' : 'Jonatish'}
            </button>
          ) : (
            <button
              className={`btn ${loading ? 'btn-secondary' : 'btn-dark'} mt-10 ml-3 `}
              style={{padding: '1rem', width: '120px'}}
              disabled={loading}
            >
              {loading ? 'отправляться....' : 'отправлять'}
            </button>
          )}
        </form>
      )}
    </>
  )
}

export default GalleryEditPage
