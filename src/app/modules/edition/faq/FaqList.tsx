import React, {useEffect, useState} from 'react'
import './style.css'
import CustomModal from '../../../../_metronic/partials/modals/CustomModal'
import {toast} from 'react-toastify'
import {Link} from 'react-router-dom'
import {useLang} from '../../../../_metronic/i18n/Metronici18n'
import { useDeleteFaqMutation, useGetAllFaqsQuery } from '../../../../redux/features/faq/faqApi'
import { format } from 'timeago.js'
import closeicon from '../../../../_metronic/assets/close.png'

const GalleryList = () => {
  const [open, setOpen] = useState(false)
  const [faqId, setFaqId] = useState('')
  const {data, refetch} = useGetAllFaqsQuery(undefined, {refetchOnMountOrArgChange: true})
  const theme = localStorage.getItem('kt_theme_mode_menu')
  const [deleteFaq, {isSuccess, isError}] = useDeleteFaqMutation()
  const locale = useLang()

  const openModal = (id: string) => {
    setFaqId(id)
    setOpen(true)
  }

  const closeModal = (e: any) => {
    setOpen(false)
  }

  const hadleDeleteFaq = async () => {
    try {
      await deleteFaq(faqId)
      refetch()
      setOpen(false)
    } catch (err) {
      setOpen(false)
    }
  }

  

  useEffect(() => {
    if (isSuccess) {
      toast.success(locale === 'uz' ?  'savol va javob muvaffaqiyatli oʻchirildi' : 'вопрось и ответь успешно удален')
    }

    if (isError) {
      toast.error( locale === 'uz' ?  'savol va javobni   o\'chirishda xatolik yuz berdi ':  'вопрось и ответь не удалено')
    }
  }, [isSuccess, isError])

  return (
    <>
      {open && (
        <CustomModal setisOpen={setOpen}>
          <div style={{width: '300px'}} className='rounded-md'>
            <span style={{cursor: 'pointer'}} onClick={closeModal}>
            <span style={{cursor:'pointer' , background:`${theme == 'light' ? 'black' : 'white'}`, width:'30px', height:'30px', borderRadius:'50%', padding:'5px'}} onClick={closeModal}> <img src={closeicon} alt="close"  style={{objectFit:'contain' , width:"20px", height:'20px'}} /></span>
            </span>
            <h2 style={{margin: '40px', fontSize: '13px'}}>
              {locale == 'uz' ? 'O`chirishga ishonchingiz komilmi' : 'ты уверен удалить'}
            </h2>

            <div className='d-flex align-items-center justify-content-between mt-5'>
              <button onClick={() => hadleDeleteFaq()} className='btn btn-danger'>
                {locale === 'uz' ? 'O`chirish' :'удалить'}
              </button>
              <button onClick={closeModal} className='btn btn-primary'>
                {locale === 'uz' ? "Bekor qilish" :"Отмена"}
              </button>
            </div>
          </div>
        </CustomModal>
      )}


      <h1 className='mb-5'>{locale === 'uz' ? 'Savol va Javoblar Royhati' : 'Список Вопросы и ответы'}</h1>

      <div className='table-responsive-sm table-responsive-md table-responsive-lg'>
        <table className='table  table-bordered table-striped'>
          <thead className='thead-dark'>
            <tr>
              <th scope='col'>#</th>
              <th scope='col'>ANSWER_UZ</th>
              <th scope='col'>ANSWER_RU</th>
              <th scope='col'>QUESTION_RU</th>
              <th scope='col'>QUESTION_UZ</th>
              <th scope='col'>CREATED_AT</th>
              <th scope='col'>UPDATED_AT</th>
              <th scope='col'>{locale == 'uz' ? 'O`chirish' : 'Удалить'}</th>
              <th scope='col'>{locale === 'uz' ?  'O`zgartirish' :'редактировать'}
              </th>
            </tr>
          </thead>
          <tbody>
            {data?.faqs?.map((item, index) => (
              <tr>
                <th scope='row'>{index + 1}</th>
                <td>{item?.answer_ru}</td>
                <td>{item?.answer_uz}</td>
                <td>{item?.question_uz}</td>
                <td>{item?.question_ru}</td>
                
                <td>{format(item?.createdAt)}</td>
                <td>{format(item?.updatedAt)}</td>
                <td style={{width: '70px'}} onClick={() => openModal(item?._id)}>
                  <span className='w-100 h-100 btn btn-danger'>{locale == 'uz' ? 'O`chirish' : 'Удалить'}</span>
                </td>
                <td style={{width: '70px'}}>
                  <Link to={`/dashboard/edit-faq/${item?._id}`}>
                    <span className='btn btn-primary h-100 w-100'>{locale === 'uz' ?  'O`zgartirish' :'редактировать'}</span>
                  </Link>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </>
  )
}

export default GalleryList
