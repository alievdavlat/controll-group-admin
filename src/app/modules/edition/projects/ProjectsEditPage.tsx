import {useEffect, useState} from 'react'
import uploadimg from '../../../../_metronic/assets/upload.png'
import {toast} from 'react-toastify'
import {useLang} from '../../../../_metronic/i18n/Metronici18n'
import {
  useEditProjectImageMutation,
  useEditProjectMutation,
  useGetOnePorjectQuery,
} from '../../../../redux/features/projects/projectsApi'
import {useNavigate, useParams} from 'react-router-dom'

const ProjectsEditPage = () => {
  const locale = useLang()

  const {id} = useParams()

  const navigate = useNavigate()

  const {data, isLoading} = useGetOnePorjectQuery(id)

  const [preview, setPreviw] = useState({
    previewURL: '',
  })

  const [editProject, {isSuccess, isError}] = useEditProjectMutation()
  const [editProjectImage, {isSuccess:imageUpdateSuccess, isError:updateImageErr}] = useEditProjectImageMutation()

  const [preview2, setPreviw2] = useState({
    previewURL: '',
  })

  const [preview3, setPreviw3] = useState({
    previewURL: '',
  })

  const [loading, setLoading] = useState(false)

  const [projectSchema, setprojectSchema] = useState({
    address: '',
    measurment: '',
    deadline_uz: '',
    deadline_ru: '',
    img1: '',
    img2: '',
    img3: '',
  })

  const handleChangeValue = (e) => {
    setprojectSchema((p) => ({...p, [e.target.name]: e.target.value}))
  }

  const hanldechangeImg = (e) => {
    const file = e.target.files[0]
    setPreviw({
      previewURL: file ? URL?.createObjectURL(file) : '',
    })

    setprojectSchema((p) => ({...p, [e.target.name]: file}))
  }

  const hanldechangeImg2 = (e) => {
    const file = e.target.files[0]
    setPreviw2({
      previewURL: file ? URL?.createObjectURL(file) : '',
    })
    setprojectSchema((p) => ({...p, [e.target.name]: file}))
  }

  const hanldechangeImg3 = (e) => {
    const file = e.target.files[0]
    setPreviw3({
      previewURL: file ? URL?.createObjectURL(file) : '',
    })
    setprojectSchema((p) => ({...p, [e.target.name]: file}))
  }

  useEffect(() => {
    if (isSuccess || imageUpdateSuccess) {
      toast.success(locale === 'uz' ? 'Proyekt O`zgardi' : 'проект  изменилось')
    }
    if (isError || updateImageErr) {
      toast.error(
        locale === 'uz'
          ? 'Proyekt o`zgarishda xatolik yuz berdi'
          : 'Произошла ошибка при  изменении проекта'
      )
    }
  }, [isError, isSuccess, imageUpdateSuccess, updateImageErr])

  useEffect(() => {
    const dataa = {
      address: data?.project?.address,
      measurment: data?.project?.measurment,
      deadline_ru: data?.project?.deadline_ru,
      deadline_uz: data?.project?.deadline_uz,
      img1: data?.project?.images[0]?.original,
      img2: data?.project?.images[1]?.original,
      img3: data?.project?.images[2]?.original,
    }
    setprojectSchema({...dataa})
  }, [isLoading, data])


  const handleSubmit = async (e) => {
    try {
      e.preventDefault()

      setLoading(true)
     

        const newData = {
          address: projectSchema?.address,
          measurment: projectSchema?.measurment,
          deadline_ru: projectSchema?.deadline_ru,
          deadline_uz: projectSchema?.deadline_uz,
        }
        
      await editProject({data:newData, id})

      setTimeout(() => {
        setLoading(false)
        navigate('/dashboard/projects-list')
      }, 1000)
    } catch (err) {
      setLoading(false)
    }
  }
  
  const handleUpdateImages  = async (e) => {
    e.preventDefault()
    setLoading(true)
    try {
      const formData = new FormData()
     
      formData.append('img1', projectSchema.img1)
      formData.append('img2', projectSchema.img2)
      formData.append('img3', projectSchema.img3)


      
      await editProjectImage({data:formData, id })

      setTimeout(() => {
        setLoading(false)
        navigate('/dashboard/projects-list')
      }, 1000)
    } catch (err) {
      setLoading(false)
    }

  }

  return (
    <>
      {isLoading ? (
        <h1>Loading...</h1>
      ) : (
        <>
          <form onSubmit={handleSubmit}>
            <h1 className='mb-20'>
              {locale === 'uz' ? 'Proyekt  O`zgartirish' : 'редактировать проекта'}
            </h1>

            <div className='row mb-10 px-3'>
              <div className='col-md-6 d-flex flex-column gap-2'>
                <label htmlFor='address' className='fs-3'>
                  {locale === 'uz' ? 'address' : 'адрес'}
                </label>
                <input
                  type='text'
                  id='address'
                  name='address'
                  value={projectSchema.address}
                  onChange={handleChangeValue}
                  className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
                  placeholder='address'
                />
              </div>

              <div className='col-md-6 d-flex flex-column gap-2'>
                <label htmlFor='measurment' className='fs-3'>
                  {locale === 'uz' ? "o'lchov" : 'измерение'}
                </label>
                <input
                  type='text'
                  id='measurment'
                  name='measurment'
                  value={projectSchema.measurment}
                  onChange={handleChangeValue}
                  className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
                  placeholder='measurment'
                />
              </div>
            </div>

            <div className='row mb-10 px-3'>
              <div className='col-md-6 d-flex flex-column gap-2'>
                <label htmlFor='deadline_uz' className='fs-3'>
                {locale === 'uz' ? 'tugatilgan muddat Uzbekchada':'Срок выполнения Узбекский'}
                </label>
                <input
                  type='text'
                  id='deadline_uz'
                  name='deadline_uz'
                  value={projectSchema.deadline_uz}
                  onChange={handleChangeValue}
                  className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
                  placeholder='deadline_uz'
                />
              </div>

              <div className='col-md-6 d-flex flex-column gap-2'>
                <label htmlFor='deadline_ru' className='fs-3'>
                {locale === 'uz' ? 'tugatilgan muddat Ruschada':'Срок выполнения Русский'}
                </label>
                <input
                  type='text'
                  id='deadline_ru'
                  name='deadline_ru'
                  value={projectSchema.deadline_ru}
                  onChange={handleChangeValue}
                  className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
                  placeholder='deadline_ru'
                />
              </div>
            </div>

            {locale === 'uz' ? (
              <button
                className={`btn ${loading ? 'btn-secondary' : 'btn-dark'} mt-10 ml-3 `}
                style={{padding: '1rem', width: '120px'}}
                disabled={loading}
              >
                {loading ? 'Jonatilmoqda...' : 'Jonatish'}
              </button>
            ) : (
              <button
                className={`btn ${loading ? 'btn-secondary' : 'btn-dark'} mt-10 ml-3 `}
                style={{padding: '1rem', width: '120px'}}
                disabled={loading}
              >
                {loading ? 'отправляться...' : 'отправлять'}
              </button>
            )}
          </form>

          <form className='mt-20' onSubmit={handleUpdateImages}>
            <h1 className='mb-20'>
              {locale === 'uz' ? 'Proyekt Rasmini  O`zgartirish' : 'изменять изображения проекта'}
            </h1>

            <div className='row mt-10 gap-10 d-flex align-items-center  px-5'>
              <div
                className='col-md-6 mr-5 d-flex align-items-center position-relative  justify-content-center form-control-solid form-control form-control-lg w-25'
                style={{height: '200px', cursor: 'pointer'}}
              >
                <input
                  type='file'
                  name='img1'
                  onChange={hanldechangeImg}
                  accept='image/*'
                  className='opacity-0 position-absolute w-100 h-100'
                  style={{cursor: 'pointer'}}
                />
                <img
                  src={uploadimg}
                  alt='upload'
                  className='w-100 h-100'
                  style={{objectFit: 'contain'}}
                />
              </div>

              <div
                className='col-md-6 mr-5 d-flex align-items-center position-relative  justify-content-center form-control-solid form-control form-control-lg w-25'
                style={{height: '200px', cursor: 'pointer'}}
              >
                <input
                  type='file'
                  name='img2'
                  multiple
                  onChange={hanldechangeImg2}
                  accept='image/*'
                  className='opacity-0 position-absolute w-100 h-100'
                  style={{cursor: 'pointer'}}
                />
                <img
                  src={uploadimg}
                  alt='upload'
                  className='w-100 h-100'
                  style={{objectFit: 'contain'}}
                />
              </div>

              <div
                className='col-md-6 mr-5 d-flex align-items-center position-relative  justify-content-center form-control-solid form-control form-control-lg w-25'
                style={{height: '200px', cursor: 'pointer'}}
              >
                <input
                  type='file'
                  name='img3'
                  multiple
                  onChange={hanldechangeImg3}
                  accept='image/*'
                  className='opacity-0 position-absolute w-100 h-100'
                  style={{cursor: 'pointer'}}
                />
                <img
                  src={uploadimg}
                  alt='upload'
                  className='w-100 h-100'
                  style={{objectFit: 'contain'}}
                />
              </div>
            </div>

            <div className='row mt-10 gap-10 d-flex align-items-center  px-5'>
              {preview.previewURL && (
                <div className='col-md-6 w-25'>
                  <img
                    src={preview.previewURL}
                    alt='uploaded img'
                    style={{maxWidth: '100%', maxHeight: '200px', marginTop: '10px'}}
                  />
                </div>
              )}

              {projectSchema?.img1 && !preview.previewURL && (
                <div className='col-md-6 w-25' style={{height: '200px', cursor: 'pointer'}}>
                  <img width={'100%'} height={'100%'} src={projectSchema?.img1} alt='project' />
                </div>
              )}

              {preview2.previewURL && (
                <div className='col-md-6 w-25'>
                  <img
                    src={preview2.previewURL}
                    alt='uploaded img'
                    style={{maxWidth: '100%', maxHeight: '200px', marginTop: '10px'}}
                  />
                </div>
              )}

              {projectSchema?.img2 && !preview2.previewURL && (
                <div className='col-md-6 w-25' style={{height: '200px', cursor: 'pointer'}}>
                  <img width={'100%'} height={'100%'} src={projectSchema?.img2} alt='project' />
                </div>
              )}

              {preview3.previewURL && (
                <div className='col-md-6 w-25'>
                  <img
                    src={preview3.previewURL}
                    alt='uploaded img'
                    style={{maxWidth: '100%', maxHeight: '200px', marginTop: '10px'}}
                  />
                </div>
              )}

              {projectSchema?.img3 && !preview3.previewURL && (
                <div className='col-md-6 w-25' style={{height: '200px', cursor: 'pointer'}}>
                  <img width={'100%'} height={'100%'} src={projectSchema?.img3} alt='project' />
                </div>
              )}
            </div>

            {locale === 'uz' ? (
              <button
                className={`btn ${loading ? 'btn-secondary' : 'btn-dark'} mt-10 ml-3 `}
                style={{padding: '1rem', width: '120px'}}
                disabled={loading}
              >
                {loading ? 'Jonatilmoqda...' : 'Jonatish'}
              </button>
            ) : (
              <button
                className={`btn ${loading ? 'btn-secondary' : 'btn-dark'} mt-10 ml-3 `}
                style={{padding: '1rem', width: '120px'}}
                disabled={loading}
              >
                {loading ? 'отправляться...' : 'отправлять'}
              </button>
            )}
          </form>
        </>
      )}
    </>
  )
}

export default ProjectsEditPage
