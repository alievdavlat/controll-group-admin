import  { useEffect, useState } from 'react'
import { useLang } from '../../../../_metronic/i18n/Metronici18n'
import { useDeleteProjectMutation, useGetAllProjectQuery } from '../../../../redux/features/projects/projectsApi'
import { toast } from 'react-toastify'
import CustomModal from '../../../../_metronic/partials/modals/CustomModal'
import { format } from 'timeago.js'
import closeicon from '../../../../_metronic/assets/close.png'
import { Link } from 'react-router-dom'


const ProjectsListPage = () => {
  const locale = useLang()
  const theme = localStorage.getItem('kt_theme_mode_menu')
  const {data, refetch} = useGetAllProjectQuery(undefined, {refetchOnMountOrArgChange: true})
  const [isOpen, setisOpen] = useState(false)
  const [deleteProject, {isSuccess, isError}] = useDeleteProjectMutation()

  const [projectId , setProjectId] = useState('')

  const handleDeleteProject = async () => {
    await deleteProject(projectId)
    refetch()
    setisOpen(false)
  }

  
  useEffect(() => {
    if (isSuccess) {
      toast.success(locale === 'uz' ? 'proyekt muvaffaqiyatli  o`chirildi' : 'проект успешно удален')
    }

    if (isError) {
      toast.error(locale === 'uz' ?  'proyektni ochirishda xatolik yuz berdi':'проект не удаленa')
    }
  }, [isSuccess, isError])

  
  const openModal = (id) => {
    setProjectId(id)
    setisOpen(true)
  }

  const closeModal = (e) => {

    setisOpen(false)

  }
  return (
    <>
    {
     isOpen &&  
     <CustomModal  setisOpen={setisOpen}>
       <div  style={{width:'300px'}} className='rounded-md'>
       <span style={{cursor:'pointer' , background:`${theme == 'light' ? 'black' : 'white'}`, width:'30px', height:'30px', borderRadius:'50%', padding:'5px'}} onClick={closeModal}> <img src={closeicon} alt="close"  style={{objectFit:'contain' , width:"20px", height:'20px'}} /></span>
         <h2  style={{margin:'40px', fontSize:'13px'}}>{locale == 'uz' ? 'O`chirishga ishonchingiz komilmi' : 'ты уверен удалить'}</h2>
 
         <div className='d-flex align-items-center justify-content-between mt-5'>
 
           <button onClick={() => handleDeleteProject()} className='btn btn-danger'>{locale === 'uz' ? 'O`chirish' : 'Удалить'}</button>
           <button onClick={closeModal} className='btn btn-primary'>{locale === 'uz' ? 'Qoldirish' : 'отменить'}</button>
 
         </div>
       </div>
     </CustomModal>
    }
     
       <h1 className='mb-5'>{locale === 'uz' ? 'Ustunliklar Royhati': 'Список проектoв'}</h1>
       <div className='table-responsive-sm table-responsive-md table-responsive-lg'>
         <table className='table  table-bordered table-striped'>
           <thead className='thead-dark'>
             <tr>
               <th scope='col'>#</th>
               <th scope='col'>ADDRESS</th>
               <th scope='col'>MEASURMENT</th>
               <th scope='col'>DEADLINE_RU</th>
               <th scope='col'>DEADLINE_UZ</th>
               <th scope='col'>CREATED_AT</th>
               <th scope='col'>UPDATED_AT</th>
               <th scope='col'>{locale == 'uz' ? 'O`chirish' : 'Удалить'}</th>
               <th scope='col'>{locale === 'uz' ?  'O`zgartirish' :'редактировать'}</th>
             </tr>
           </thead>
           <tbody>
             {data?.projects?.map((item, index) => (
               <tr>
                 <th scope='row'>{index + 1}</th>
                 <td>{item?.address}</td>
                 <td>{item?.measurment}</td>
                 <td>{item?.deadline_ru}</td>
                 <td>{item?.deadline_uz}</td>
                 <td>{format(item?.createdAt)}</td>
                 <td>{format(item?.updatedAt)}</td>
                 <td style={{width: '70px'}} onClick={() => openModal(item?._id)}>
                   <span className='w-100 h-100 btn btn-danger'>{locale == 'uz' ? 'O`chirish' : 'Удалить'}</span>
                 </td>
                 <td style={{width: '70px'}}>
                  <Link to={`/dashboard/edit-project/${item?._id}`}>
                    <span className='btn btn-primary h-100 w-100'>{locale === 'uz' ?  'O`zgartirish' :'редактировать'}</span>
                  </Link>
                </td>
               </tr>
             ))}
           </tbody>
         </table>
       </div>
     </>  )
}

export default ProjectsListPage