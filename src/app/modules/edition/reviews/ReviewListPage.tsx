import React, { useEffect, useState } from 'react'
import { useDeleteReviewMutation, useGetAllReviewsQuery } from '../../../../redux/features/reviews/reviewsApi'
import { toast } from 'react-toastify';
import CustomModal from '../../../../_metronic/partials/modals/CustomModal';
import closeicon from '../../../../_metronic/assets/close.png'
import { format } from 'timeago.js';
import { Link } from 'react-router-dom';
import { useLang } from '../../../../_metronic/i18n/Metronici18n';

const ReviewListPage = () => {
  const locale = useLang()

  const { data , refetch} = useGetAllReviewsQuery(undefined, {refetchOnMountOrArgChange:true})
  
  const theme = localStorage.getItem('kt_theme_mode_menu')
  const [isOpen, setisOpen] = useState(false)
  const [deleteReview, {isSuccess, isError }] = useDeleteReviewMutation()

  const [revvId , setRevId] = useState('')

  

  const handleDeleterev = async () => {
    await deleteReview(revvId)
    refetch()
    setisOpen(false)
  }

  useEffect(() => {
    if (isSuccess) {
      toast.success(locale === 'uz' ? 'sharh muvaffaqiyatli oʻchirildi':"отзыв успешно удален")
    }

    if (isError) {
      toast.error(locale === 'uz' ? 'sharh o\'chirilmagan':"комментарий не удален")
    }
  }, [isSuccess, isError])

  
  const openModal = (id) => {
    setRevId(id)
    setisOpen(true)
  }

  const closeModal = (e) => {

    setisOpen(false)

  }


  return (
    <>
    {
     isOpen &&  
     <CustomModal  setisOpen={setisOpen}>
          <div  style={{width:'300px'}} className='rounded-md'>
          <span style={{cursor:'pointer' , background:`${theme == 'light' ? 'black' : 'white'}`, width:'30px', height:'30px', borderRadius:'50%', padding:'5px'}} onClick={closeModal}> <img src={closeicon} alt="close"  style={{objectFit:'contain' , width:"20px", height:'20px'}} /></span>
        <h2  style={{margin:'40px', fontSize:'13px'}}>{locale == 'uz' ? 'O`chirishga ishonchingiz komilmi' : 'ты уверен удалить'}</h2>

         <div className='d-flex align-items-center justify-content-between mt-5'>
 
           <button onClick={() => handleDeleterev()} className='btn btn-danger'>{locale === 'uz' ? 'O`chirish' : 'Удалить'}</button>
           <button onClick={closeModal} className='btn btn-primary'>{locale === 'uz' ? 'Qoldirish' : 'отменить'}</button>
 
         </div>
       </div>
     </CustomModal>
    }
     
       <h1 className='mb-5'>{locale === 'uz' ? 'Izohlar Ro`yhati' :'список отзывов  '}</h1>
       <div className='table-responsive-sm table-responsive-md table-responsive-lg'>
         <table className='table  table-bordered table-striped'>
           <thead className='thead-dark'>
             <tr>
               <th scope='col'>#</th>
               <th scope='col'>OWNER_RU</th>
               <th scope='col'>OWNER_UZ</th>
               <th scope='col'>DESCRIPTION_RU</th>
               <th scope='col'>DESCRIPTION_UZ</th>
               <th scope='col'>CREATED_AT</th>
               <th scope='col'>UPDATED_AT</th>
               <th scope='col'>{locale == 'uz' ? 'O`chirish' : 'Удалить'}</th>
              <th scope='col'>{locale === 'uz' ?  'O`zgartirish' :'редактировать'} </th>
             </tr>
           </thead>
           <tbody>
             {data?.reviews?.map((item, index) => (
               <tr>
                 <th scope='row'>{index + 1}</th>
                 <td>{item?.owner_ru}</td>
                 <td>{item?.owner_uz}</td>
                 <td>{item?.description_ru}</td>
                 <td>{item?.description_uz}</td>
                 <td>{format(item?.createdAt)}</td>
                 <td>{format(item?.updatedAt)}</td>
                 <td style={{width: '70px'}} onClick={() => openModal(item?._id)}>
                   <span className='w-100 h-100 btn btn-danger'>{locale === 'uz' ? "O`chirish" :"Удалить"}</span>
                 </td>
                 <td style={{width: '70px'}}>
                   <Link to={`/dashboard/edit-reviews/${item?._id}`}>
                     <span className='btn btn-primary h-100 w-100'>{locale === 'uz' ? 'O`zgartirish':"изменять"}</span>
                   </Link>
                 </td>
               </tr>
             ))}
           </tbody>
         </table>
       </div>
     </>
  )
}

export default ReviewListPage