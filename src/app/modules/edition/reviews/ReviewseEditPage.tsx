import React, { useEffect, useState } from 'react'
import { toast } from 'react-toastify'
import { useEditReviewMutation, useGetOneReviewQuery,  } from '../../../../redux/features/reviews/reviewsApi'
import { useNavigate, useParams } from 'react-router-dom'
import { useLang } from '../../../../_metronic/i18n/Metronici18n'
import uploadimg from '../../../../_metronic/assets/upload.png'

const ReviewseEditPage = () => {
  const [loading , setLoading ] = useState(false)
  const { id } = useParams()
  const navigate =  useNavigate()
  const locale = useLang()
  const [editReview, {isError, isSuccess, data:revidata}] = useEditReviewMutation()
  const { data , refetch} = useGetOneReviewQuery( id , {refetchOnMountOrArgChange:true})


  const [reviewSchema , setReviewSchema ] = useState({
    owner_ru:'',
    owner_uz:'',
    description_ru:'',
    description_uz:'',
    avatar:''

  })

  const [preview, setPreviw] = useState({
    previewURL: '',
  })

  

  const handleChangeValue = (e) => {
    setReviewSchema(p => ({...p, [e.target.name]:e.target.value}))
  }

  const hanldechangeImg = (e) => {
    const file = e.target.files[0]
    setPreviw({
      previewURL: file ? URL?.createObjectURL(file) : '',
    })
   
    setReviewSchema(p => ({...p, [e.target.name]:file}))
}
  
  useEffect(() => {
    setReviewSchema({...data?.review})
  }, [data, revidata])


  const handleSubmit = async (e) => {
    try {
      e.preventDefault()
      setLoading(true)

      const formData = new FormData();
      formData.append('owner_ru', reviewSchema.owner_ru);
      formData.append('owner_uz', reviewSchema.owner_uz);
      formData.append('description_ru', reviewSchema.description_ru);
      formData.append('description_uz', reviewSchema.description_uz);
      formData.append('avatar', reviewSchema.avatar);
    
      
      await editReview({data:formData, id})
      
    setTimeout(() => {
      setLoading(false)
      setReviewSchema({
        owner_ru:'',
        owner_uz:'',
        description_ru:'',
        description_uz:'',
        avatar:''
      })
      refetch()
      navigate('/dashboard/reviews-list')
    }, 1000);

    } catch (err) {
      setLoading(false)
    }
  }

  useEffect(() => {

    if (isSuccess) {
      toast.success(locale === 'uz'? "Sharxlar o`zgardi":'отзывы обновлены')
    }

    if (isError) {
      toast.error(locale === 'uz' ? "Sharxlar o`zgarishda xatolik yuz berdi":'отзывы не обновляются')
    }
    

  }, [isError, isSuccess])


  return (
    <form onSubmit={handleSubmit}>
    <div className='row mb-10 px-3'>
      
      <div className="col-md-6 d-flex flex-column gap-2">
      <label htmlFor="owner_ru" className='fs-3'>
      {locale === 'uz' ? "Ta`rif egasi ismi Ruchada" :'Имя владельца  Русский.'}
        </label>
      <input
        type='text'
        id='owner_ru'
        name='owner_ru'
        value={reviewSchema.owner_ru}
        onChange={handleChangeValue}
        required
        className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
      />
      </div>

      <div className="col-md-6 d-flex flex-column gap-2">
      <label htmlFor="owner_uz" className='fs-3'>
      {locale === 'uz' ? "Ta`rif egasi ismi Uzbekchada" :'Имя владельца  Узбекский.'}
      </label>
      <input
        type='text'
        id='owner_uz'
        name='owner_uz'
        required
        value={reviewSchema.owner_uz}
        onChange={handleChangeValue}
        className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
      />
      </div>
    </div>

    <div className='row mb-10 px-3'>
      
      <div className="col-md-6 d-flex flex-column gap-2">
      <label htmlFor="description_ru" className='fs-3'>
      {locale === 'uz' ? "Ta`rif  Ruschada" :'описания на Русский'} 
       </label>      
       <textarea
        id='description_ru'
        name='description_ru'
        required
        value={reviewSchema.description_ru}
        onChange={handleChangeValue}
        className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
      />
      </div>

      <div className="col-md-6 d-flex flex-column gap-2">
      <label htmlFor="description_uz" className='fs-3'>
      {locale === 'uz' ? "Ta`rif Uzbekchada" :' описания на узбекском.'}
      </label>
      <textarea
        id='description_uz'
        name='description_uz'
        required
        value={reviewSchema.description_uz}
        onChange={handleChangeValue}
        className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
      />
      </div>
    </div>
    <div className="row mt-10 px-3">
       <div
        className='col-md-6 mr-5 d-flex align-items-center position-relative  justify-content-center form-control-solid form-control form-control-lg w-25'
        style={{height: '200px', cursor: 'pointer'}}
      >
        <input
          type='file'
          name='avatar'
          multiple
          onChange={hanldechangeImg}
          accept='image/*'
          className='opacity-0 position-absolute w-100 h-100'
          style={{cursor: 'pointer'}}
        />
        <img src={uploadimg} alt="upload" className='w-100 h-100' style={{objectFit:'contain'}} />
      </div>
      <div className='col-md-6'>
        {preview.previewURL && (
          <img
            src={preview.previewURL}
            alt='uploaded img'
            style={{maxWidth: '100%', maxHeight: '200px', marginTop: '10px'}}
          />
        )}
      </div>
    </div>

      <button className={`btn ${loading ? 'btn-secondary' : 'btn-dark'} mt-10 ml-5 `} style={{padding:'1rem', width:"120px"}} disabled={loading}>
        {loading ? 'Sending....' : 'Send'}
      </button>
      
    </form>
  )

}

export default ReviewseEditPage