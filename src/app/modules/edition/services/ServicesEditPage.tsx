import  { useEffect, useState } from 'react'
import uploadimg from '../../../../_metronic/assets/upload.png'
import { toast } from 'react-toastify'
import { useEditServicesImgMutation, useEditServicesInfoMutation, useGetAllServicesQuery, useGetOneServicesQuery } from '../../../../redux/features/services-api/servicesApi'
import { useNavigate, useParams } from 'react-router-dom'
import { useLang } from '../../../../_metronic/i18n/Metronici18n'

const ServicesEditPage = () => {
  const locale = useLang()

  const { id } = useParams()
  const [loading , setLoading ] = useState(false)
  const [editServicesImg , {isSuccess:EditIMgSuesses, isError:EditImgError, data:servicesImgData}] = useEditServicesImgMutation()
  const [EditServicesInfo , {isSuccess, isError}] = useEditServicesInfoMutation()
  const { data , isLoading, isSuccess:singleServicesSuccess, refetch:onseServiceRefetch}  = useGetOneServicesQuery(id, { refetchOnMountOrArgChange:true})
  const { refetch} = useGetAllServicesQuery(undefined, {refetchOnMountOrArgChange: true})

  const navigate = useNavigate()

  const [preview, setPreviw] = useState({
    previewURL: '',
  })


  
const [servicesSchema, setServicesSchema] = useState({
  service_type_ru:'',
  service_type_uz:'',
  title_ru:'',
  title_uz:'',
  description_ru:'',
  description_uz:'',
  subtitle_one_ru:'',
  subtitle_one_uz:'',
  subtitle_two_uz:'',
  subtitle_two_ru:'',
  img:''
})

const handleChangeValue = (e) => {
  setServicesSchema(p => ({...p, [e.target.name]:e.target.value}))
}

const hanldechangeImg = (e) => {
    const file = e.target.files[0]
    setPreviw({
      previewURL: file ? URL?.createObjectURL(file) : '',
    })
    setServicesSchema(p => ({...p, [e.target.name]:file}))
}

const handleSubmit = async (e) => {
  try {
    e.preventDefault()
    setLoading(true)
    const formData = new FormData();

    formData.append('image', servicesSchema?.img);

    await editServicesImg({data: formData, id})
    const dataInfo = {
      service_type_ru:servicesSchema.service_type_ru,
      service_type_uz:servicesSchema.service_type_uz,
      title_ru:servicesSchema.title_ru,
      title_uz:servicesSchema.title_uz,
      description_ru:servicesSchema.description_ru,
      description_uz:servicesSchema.description_uz,
      subtitle_one_ru:servicesSchema.subtitle_one_ru,
      subtitle_one_uz:servicesSchema.subtitle_one_uz,
      subtitle_two_uz:servicesSchema.subtitle_two_uz,
      subtitle_two_ru:servicesSchema.subtitle_two_ru,
      }

  await EditServicesInfo({data:dataInfo, id})
    

  setTimeout(() => {
    setLoading(false)
    setServicesSchema({
      service_type_ru:'',
      service_type_uz:'',
      title_ru:'',
      title_uz:'',
      description_ru:'',
      description_uz:'',
      subtitle_one_ru:'',
      subtitle_one_uz:'',
      subtitle_two_uz:'',
      subtitle_two_ru:'',
      img:''
    })

    setPreviw({
      previewURL: '',
    })

    refetch()
    onseServiceRefetch()
    navigate('/dashboard/services-list')
  }, 1000)

  } catch (err) {
    setLoading(false)
  }
}

useEffect(() => {
  if (isSuccess || EditIMgSuesses ) {
    toast.success(locale === 'uz'? 'Ximatlar o`zgardi':'пункт услуг обновлен')
  }

  if (isError || EditImgError) {
    toast.error(locale === 'uz' ?"Xizmatlar o`zgarishda xatolik yuz berdi" :'услуги не обновлены')
  }
  

}, [isSuccess, isError])  

useEffect(() => {

  setServicesSchema({...data?.service})

}, [singleServicesSuccess, data])


  return (
   <>
   {
    isLoading 
    ? <h1>loading...</h1>
    :  <form onSubmit={handleSubmit}>
    <div className='row mb-10 px-3'>

      <div className='col-md-6 d-flex flex-column gap-2'>
        <label htmlFor='service_type_ru' className='fs-3'>
        {locale == 'uz' ? 'Xizmat turi Ruschada' :"вид услуги на  Русский"}
        </label>
        <input
          type='text'
          id='service_type_ru'
          name='service_type_ru'
          value={servicesSchema.service_type_ru}
          onChange={handleChangeValue}
          className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
          placeholder='remont'
          
        />
      </div>

      <div className='col-md-6 d-flex flex-column gap-2'>
        <label htmlFor='service_type_uz' className='fs-3'>
        {locale == 'uz' ? 'Xizmat turi Uzbekchada' :"вид услуги на узбекском"}
        </label>
        <input
          type='text'
          id='service_type_uz'
          name='service_type_uz'
          value={servicesSchema.service_type_uz}
          onChange={handleChangeValue}
          className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
          placeholder='remont'
          
        />
      </div>
    </div>

    <div className='row mb-10 px-3'>
      <div className='col-md-6 d-flex flex-column gap-2'>
        <label htmlFor='title_ru' className='fs-3'>
        {locale === 'uz' ? 'Sarlovha Ruschada' :'заголовок на Русский '}
        </label>
        <input
          type='text'
          id='title_ru'
          name='title_ru'
          value={servicesSchema.title_ru}
          onChange={handleChangeValue}
          className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
          placeholder='remont'
          
        />
      </div>

      <div className='col-md-6 d-flex flex-column gap-2'>
        <label htmlFor='title_uz' className='fs-3'>
        {locale === 'uz' ? 'Sarlovha uzbekchada' :'заголовок на узбекском '}
        </label>
        <input
          type='text'
          id='title_uz'
          name='title_uz'
          value={servicesSchema.title_uz}
          onChange={handleChangeValue}
          className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
          placeholder='remont'
          
        />
      </div>
    </div>

    <div className='row mb-10 px-3'>
      <div className='col-md-6 d-flex flex-column gap-2'>
        <label htmlFor='subtitle_one_ru' className='fs-3'>
        {locale === 'uz' ? ' Sarlovha 1 Ruschada' :'Подзаголовок 1 на Русский '}
        </label>
        <input
          type='text'
          id='subtitle_one_ru'
          name='subtitle_one_ru'
          value={servicesSchema.subtitle_one_ru}
          onChange={handleChangeValue}
          className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
          placeholder='remont'
          
        />
      </div>

      <div className='col-md-6 d-flex flex-column gap-2'>
        <label htmlFor='subtitle_one_uz' className='fs-3'>
        {locale === 'uz' ? 'Sarlovha 1 uzbekchada' :'Подзаголовок 1 на узбекском '}
        </label>
        <input
          type='text'
          id='subtitle_one_uz'
          name='subtitle_one_uz'
          value={servicesSchema.subtitle_one_uz}
          onChange={handleChangeValue}
          className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
          placeholder='remont'
          
        />
      </div>
    </div>

    <div className='row mb-10 px-3'>
      <div className='col-md-6 d-flex flex-column gap-2'>
        <label htmlFor='subtitle_two_ru' className='fs-3'>
        {locale === 'uz' ? 'Sarlovha 2 Ruschada' :'Подзаголовок 2 на Русский '}
        </label>
        <input
          type='text'
          id='subtitle_two_ru'
          name='subtitle_two_ru'
          value={servicesSchema.subtitle_two_ru}
          onChange={handleChangeValue}
          className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
          placeholder='remont'
          
        />
      </div>

      <div className='col-md-6 d-flex flex-column gap-2'>
        <label htmlFor='subtitle_two_uz' className='fs-3'>
        {locale === 'uz' ? 'Sarlovha 2 uzbekchada' :'Подзаголовок 2 на узбекском '}
        </label>
        <input
          type='text'
          id='subtitle_two_uz'
          name='subtitle_two_uz'
          value={servicesSchema.subtitle_two_uz}
          onChange={handleChangeValue}
          className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
          placeholder='remont'
          
        />
      </div>
    </div>

    <div className='row mb-10 px-3'>
      <div className='col-md-6 d-flex flex-column gap-2'>
        <label htmlFor='description_ru' className='fs-3'>
        {locale === 'uz' ? ' tavsifi Ruschada' :'описание  на русском '}
        </label>
        <textarea
          id='description_ru'
          name='description_ru'
          value={servicesSchema.description_ru}
          onChange={handleChangeValue}
          className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
          placeholder='Add Some text...'
          
        />
      </div>

      <div className='col-md-6 d-flex flex-column gap-2'>
        <label htmlFor='description_uz' className='fs-3'>
        {locale === 'uz' ? 'tavsifi Uzbekchada' :'описание  на Узбекский '}
        </label>
        <textarea
          id='description_uz'
          name='description_uz'
          value={servicesSchema.description_uz}
          onChange={handleChangeValue}
          className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
          placeholder='Add Some text...'
          
        />
      </div>
    </div>

    <div className='row d-flex align-items-center  px-5'>
    <div
      className='col-md-4 d-flex align-items-center position-relative  justify-content-center form-control-solid form-control form-control-lg w-25'
      style={{height: '200px', cursor: 'pointer'}}
    >
      <input
        type='file'
        name='img'
        onChange={hanldechangeImg}
        accept='image/*'
        className='opacity-0 position-absolute w-100 h-100'
        style={{cursor: 'pointer'}}
      />
          <img src={uploadimg} alt="upload" className='w-100 h-100' style={{objectFit:'contain'}} />
    </div>
      {preview.previewURL && (
    <div className='col-md-4'>
        <img
          src={preview.previewURL}
          alt='uploaded img'
          style={{maxWidth: '100%', maxHeight: '200px', marginTop: '10px'}}
        />
    </div>
      )}

      {
        servicesSchema?.img &&  !preview.previewURL &&
      <div className="col-md-4">
        <img width={200} height={200} src={servicesSchema?.img} alt="services" />
      </div>
      }

    
  </div>
  {
        locale === 'uz'
        ?
    <button className={`btn ${loading ? 'btn-secondary' : 'btn-dark'} mt-10 ml-3 `} style={{padding:'1rem', width:"120px"}} disabled={loading}>
      {loading ? 'Jonatilmoqda....' : 'Jonatish'}
    </button>
    : 
    <button className={`btn ${loading ? 'btn-secondary' : 'btn-dark'} mt-10 ml-3 `} style={{padding:'1rem', width:"120px"}} disabled={loading}>
      {loading ? 'отправляться....' : 'отправлять'}
    </button>

    }
  </form>
   }
   </>
    )
}

export default ServicesEditPage