import  { useEffect, useState } from 'react'
import { toast } from 'react-toastify'
import { useDeleteServicesMutation, useGetAllServicesQuery } from '../../../../redux/features/services-api/servicesApi'
import CustomModal from '../../../../_metronic/partials/modals/CustomModal'
import { Link } from 'react-router-dom'
import { format } from 'timeago.js'
import { useLang } from '../../../../_metronic/i18n/Metronici18n'
import closeicon from '../../../../_metronic/assets/close.png'

const ServicesList = () => {

  const theme = localStorage.getItem('kt_theme_mode_menu')
  const {data, refetch} = useGetAllServicesQuery(undefined, {refetchOnMountOrArgChange: true})
  const [isOpen, setisOpen] = useState(false)
  const [deleteServices, {isSuccess, isError}] = useDeleteServicesMutation()
  const locale = useLang()

  const [servId , setServId] = useState('')

  const handleDeleteServices = async () => {
    await deleteServices(servId)
    refetch()
    setisOpen(false)
  }

  
  useEffect(() => {
    if (isSuccess) {
      toast.success('Services deleted successfully')
    }

    if (isError) {
      toast.error('Services not deleted')
    }
  }, [isSuccess, isError])

  
  const openModal = (id) => {
    setServId(id)
    setisOpen(true)
  }

  const closeModal = (e) => {

    setisOpen(false)

  }


  return (
    <>
    {
     isOpen &&  
     <CustomModal  setisOpen={setisOpen}>
       <div  style={{width:'300px'}} className='rounded-md'>
       <span style={{cursor:'pointer' , background:`${theme == 'light' ? 'black' : 'white'}`, width:'30px', height:'30px', borderRadius:'50%', padding:'5px'}} onClick={closeModal}> <img src={closeicon} alt="close"  style={{objectFit:'contain' , width:"20px", height:'20px'}} /></span>
        <h2  style={{margin:'40px', fontSize:'13px'}}>{locale == 'uz' ? 'O`chirishga ishonchingiz komilmi' : 'ты уверен удалить'}</h2>

         <div className='d-flex align-items-center justify-content-between mt-5'>
 

         <button onClick={() => handleDeleteServices()} className='btn btn-danger'>{locale === 'uz' ? 'O`chirish' : 'Удалить'}</button>
          <button onClick={closeModal} className='btn btn-primary'>{locale === 'uz' ? 'Qoldirish' : 'отменить'}</button>
 
         </div>
       </div>
     </CustomModal>
    }
     
       <h1 className='mb-5'>{locale === 'uz' ? 'Xizmatlar Royhati' : ''}</h1>
       <div className='table-responsive-sm table-responsive-md table-responsive-lg'>
         <table className='table  table-bordered table-striped'>
           <thead className='thead-dark'>
             <tr>
               <th scope='col' style={{textTransform:'uppercase'}}>#</th>
               <th scope='col' style={{textTransform:'uppercase'}}>service_type_ru</th>
               <th scope='col' style={{textTransform:'uppercase'}}>title_ru</th>
               <th scope='col' style={{textTransform:'uppercase'}}>subtitle_one_ru</th>
               <th scope='col' style={{textTransform:'uppercase'}}>subtitle_two_ru</th>
               <th scope='col' style={{textTransform:'uppercase'}}>description_ru</th>
               <th scope='col' style={{textTransform:'uppercase'}}>created_at</th>
               <th scope='col' style={{textTransform:'uppercase'}}>updated_at</th>
               <th scope='col' style={{textTransform:'uppercase'}}>{locale == 'uz' ? 'O`chirish' : 'Удалить'}</th>
              <th scope='col' style={{textTransform:'uppercase'}}>{locale === 'uz' ?  'O`zgartirish' :'редактировать'}</th>
             </tr>
           </thead>
           <tbody>
             {data?.services?.map((item, index) => (
               <tr>
                 <th scope='row'>{index + 1}</th>
                 <td>{item?.service_type_ru}</td>
                 <td>{item?.title_ru}</td>
                 <td>{item?.subtitle_one_ru}</td>
                 <td>{item?.subtitle_two_ru}</td>
                 <td>{item?.description_ru}</td>
                 <td>{format(item?.createdAt)}</td>
                 <td>{format(item?.updatedAt)}</td>
                 <td style={{width: '70px'}} onClick={() => openModal(item?._id)}>
                 <span className='w-100 h-100 btn btn-danger'>{locale === 'uz' ? "O`chirish" :"Удалить"}</span>
                 </td>
                 <td style={{width: '70px'}}>
                   <Link to={`/dashboard/edit-services/${item?._id}`}>
                   <span className='btn btn-primary h-100 w-100'>{locale === 'uz' ? 'O`zgartirish':"изменять"}</span>
                   </Link>
                 </td>
               </tr>
             ))}
           </tbody>
         </table>
       </div>
     </>
  )
}

export default ServicesList