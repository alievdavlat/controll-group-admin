import  { useEffect, useState } from 'react'
import { toast } from 'react-toastify'
import { useLang } from '../../../../_metronic/i18n/Metronici18n'
import { useEditStockMutation, useGetOneStockQuery } from '../../../../redux/features/stocks/stocksApi'
import { useNavigate, useParams } from 'react-router-dom'
import uploadimg from '../../../../_metronic/assets/upload.png'

const StocksEditPage = () => {

  const { id } = useParams()
  const navigate = useNavigate()
  const [editStock, { isSuccess, isError }] = useEditStockMutation()

  const {data, isSuccess:stockDataSuccess, isLoading} = useGetOneStockQuery(id)

  

  const locale = useLang()

  const [preview, setPreviw] = useState({
    previewURL: '',
  })

  const [loading , setLoading ] = useState(false)

  const [stocksSchema , setStocksSchema] = useState({
    description_uz:'',
    description_ru:'',
    image:''
  })

  const handleChangeValue = (e) => {
    setStocksSchema(p => ({...p, [e.target.name]:e.target.value}))
  }

  const hanldechangeImg = (e) => {
      const file = e.target.files[0]
      setPreviw({
        previewURL: file ? URL?.createObjectURL(file) : '',
      })
      setStocksSchema(p => ({...p, [e.target.name]:file}))
  }

  const handleSubmit =  async (e) => {
    try {
      e.preventDefault()

      setLoading(true)
      const formData = new FormData();
    

      Object.keys(stocksSchema).forEach((key) => {
        formData.append(key, stocksSchema[key]);
      });

      await editStock({data:formData, id})

      setTimeout(() => {
        setLoading(false)
        setStocksSchema({
          description_uz:'',
          description_ru:'',
          image:''
        })

        setPreviw({
          previewURL: '',
        })

        navigate('/dashboard/stocks-list')
      }, 1000)
      
    } catch (err) {
     setLoading(false) 
    }

  }

  useEffect(() => {
    setStocksSchema(data?.stock)
  }, [stockDataSuccess, data?.stock])

  useEffect(() => {
    if (isSuccess) {
      toast.success(locale === 'uz' ? 'aksiya muovfaqiyatli o`zgardi ' :'аксии успешно обновлено')
    }

    if (isError) {
      toast.error(locale === 'uz' ? 'akiya o`zgartrishda  xatolik yuz berdi' : 'Произошла ошибка при обновления аксии')
    }
    

  }, [isSuccess, isError])  

  return (
    <>
  {
    isLoading && stockDataSuccess  ? 
    <h1>Loading...</h1>  
    : <form  onSubmit={handleSubmit}>

    <h1 className='mx-5 mb-10'>{locale === 'uz' ? 'Akisyalar o`zgartish' : 'Редактирование акции'}</h1>

    <div className='row mb-10 px-3'>
      <div className='col-md-6 d-flex flex-column gap-2'>
        <label htmlFor='description_ru' className='fs-3'>
        {locale === 'uz' ? ' Ta`rif  Ruschada' :'Описание на Русский'}
        </label>
        <textarea
          id='description_ru'
          name='description_ru'
          value={stocksSchema?.description_ru}
          required
          onChange={handleChangeValue}
          className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
          placeholder='Add Some text...'
        />
      </div>

      <div className='col-md-6 d-flex flex-column gap-2'>
        <label htmlFor='description_uz' className='fs-3'>
        {locale === 'uz' ? 'Ta`vsifi Uzbekchada' :'Описание на Узбекский'}
        </label>
        <textarea
          id='description_uz'
          name='description_uz'
          required
          value={stocksSchema?.description_uz}
          onChange={handleChangeValue}
          className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
          placeholder='Add Some text...'
        />
      </div>
    </div>

    <div className='row d-flex align-items-center  px-5'>
      <div
        className='col-md-4 d-flex align-items-center position-relative  justify-content-center form-control-solid form-control form-control-lg w-25'
        style={{height: '200px', cursor: 'pointer'}}
      >
        <input
          type='file'
          name='image'
          onChange={hanldechangeImg}
          accept='image/*'
          className='opacity-0 position-absolute w-100 h-100'
          style={{cursor: 'pointer'}}
        />
          <img src={uploadimg} alt="upload" className='w-100 h-100' style={{objectFit:'contain'}} />
      </div>
        {preview.previewURL && (
      <div className='col-md-4'>
          <img
            src={preview.previewURL}
            alt='uploaded img'
            style={{maxWidth: '100%', maxHeight: '200px', marginTop: '10px'}}
          />
      </div>
        )}

        {
          stocksSchema?.image &&  !preview.previewURL &&
        <div className="col-md-4">
          <img width={200} height={200} src={stocksSchema?.image} alt="stocks" />
        </div>
        }

      
    </div>

  {
    locale === 'uz'
    ?
  <button className={`btn ${loading ? 'btn-secondary' : 'btn-dark'} mt-10 ml-3 `} style={{padding:'1rem', width:"120px"}} disabled={loading}>
    {loading ? 'Jonatilmoqda...' : 'Jonatish'}
  </button>
  : 
  <button className={`btn ${loading ? 'btn-secondary' : 'btn-dark'} mt-10 ml-3 `} style={{padding:'1rem', width:"120px"}} disabled={loading}>
    {loading ? 'отправляться...' : 'отправлять'}
  </button>

  }
  </form>
  } 
  </>
  )
}

export default StocksEditPage