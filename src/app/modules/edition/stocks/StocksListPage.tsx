import React, { useEffect, useState } from 'react'
import { toast } from 'react-toastify'
import { useDeleteStockMutation, useGetAllStocksQuery } from '../../../../redux/features/stocks/stocksApi'
import { useLang } from '../../../../_metronic/i18n/Metronici18n'
import CustomModal from '../../../../_metronic/partials/modals/CustomModal'
import { format } from 'timeago.js'
import { Link } from 'react-router-dom'
import closeicon from '../../../../_metronic/assets/close.png'


const StocksListPage = () => {
  const locale = useLang()
  const theme = localStorage.getItem('kt_theme_mode_menu')
  const {data, refetch} = useGetAllStocksQuery(undefined, {refetchOnMountOrArgChange: true})
  const [isOpen, setisOpen] = useState(false)
  const [deleteStock, {isSuccess, isError}] = useDeleteStockMutation()

  const [stockId , setstockId] = useState('')

  console.log(data);
  

  const handleDeleteStock = async () => {
    await deleteStock(stockId)
    refetch()
    setisOpen(false)
  }

  useEffect(() => {
    if (isSuccess) {
      toast.success(locale === 'uz' ? 'Akisya muovfaqiyatli o`chirildi' :'акции успешно удалены')
    }

    if (isError) {
      toast.error(locale === 'uz' ? 'Aksiyani o`chirishda xatolik yuz berdi' :'акции не удалены')
    }
  }, [isSuccess, isError])

  
  const openModal = (id) => {
    setstockId(id)
    setisOpen(true)
  }

  const closeModal = (e) => {

    setisOpen(false)

  }

  return (
    <>
    {
     isOpen &&  
     <CustomModal  setisOpen={setisOpen}>
       <div  style={{width:'300px'}} className='rounded-md'>
       <span style={{cursor:'pointer' , background:`${theme == 'light' ? 'black' : 'white'}`, width:'30px', height:'30px', borderRadius:'50%', padding:'5px'}} onClick={closeModal}> <img src={closeicon} alt="close"  style={{objectFit:'contain' , width:"20px", height:'20px'}} /></span>
         <h2  style={{margin:'40px', fontSize:'13px'}}>{locale == 'uz' ? 'O`chirishga ishonchingiz komilmi' : 'ты уверен удалить'}</h2>
 
         <div className='d-flex align-items-center justify-content-between mt-5'>
 
           <button onClick={() => handleDeleteStock()} className='btn btn-danger'>{locale === 'uz' ? 'O`chirish' : 'Удалить'}</button>
           <button onClick={closeModal} className='btn btn-primary'>{locale === 'uz' ? 'Qoldirish' : 'отменить'}</button>
 
         </div>
       </div>
     </CustomModal>
    }
     
       <h1 className='mb-5'>{locale === 'uz' ? 'Ustunliklar akisyalar': 'Список акции'}</h1>
       <div className='table-responsive-sm table-responsive-md table-responsive-lg'>
         <table className='table  table-bordered table-striped'>
           <thead className='thead-dark'>
             <tr>
               <th scope='col'>#</th>
               <th scope='col'>DESCRIPTION_RU</th>
               <th scope='col'>DESCRIPTION_UZ</th>
               <th scope='col'>CREATED_AT</th>
               <th scope='col'>UPDATED_AT</th>
               <th scope='col'>{locale == 'uz' ? 'O`chirish' : 'Удалить'}</th>
               <th scope='col'>{locale === 'uz' ?  'O`zgartirish' :'редактировать'}
               </th>
             </tr>
           </thead>
           <tbody>
             {data?.stocks?.map((item, index) => (
               <tr>
                 <th scope='row'>{index + 1}</th>
                 <td>{item?.description_ru}</td>
                 <td>{item?.description_uz}</td>
                 <td>{format(item?.createdAt)}</td>
                 <td>{format(item?.updatedAt)}</td>
                 <td style={{width: '70px'}} onClick={() => openModal(item?._id)}>
                   <span className='w-100 h-100 btn btn-danger'>{locale  === 'uz' ? "O`chirish" : 'удалить'}</span>
                 </td>
                 <td style={{width: '70px'}}>
                   <Link to={`/dashboard/edit-stocks/${item?._id}`}>
                     <span className='btn btn-primary h-100 w-100'>{locale === 'uz' ? 'O`zgartirish' : 'Редактировать'}</span>
                   </Link>
                 </td>
               </tr>
             ))}
           </tbody>
         </table>
       </div>
     </>
  )
}

export default StocksListPage