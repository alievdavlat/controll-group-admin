import { useEffect, useState } from 'react'
import { format } from 'timeago.js'
import CustomModal from '../../../_metronic/partials/modals/CustomModal'
import { toast } from 'react-toastify'
import { useDeleteContactMutation, useGetContactsQuery } from '../../../redux/features/contact/contactApi'
import { useLang } from '../../../_metronic/i18n/Metronici18n'
import closeicon from '../../../_metronic/assets/close.png'


const UserManagemant = () => {
  const { data , refetch} = useGetContactsQuery(undefined, {refetchOnMountOrArgChange:true})
  const locale = useLang()
  const theme = localStorage.getItem('kt_theme_mode_menu')
  const [isOpen, setisOpen] = useState(false)
  const [deleteContact, {isSuccess, isError }] = useDeleteContactMutation()

  const [contactId , setcontactId] = useState('')

  

  const handleDeleterev = async () => {
    await deleteContact(contactId)
    refetch()
    setisOpen(false)
  }

  useEffect(() => {
    if (isSuccess) {
      toast.success('contact deleted successfully')
    }

    if (isError) {
      toast.error('contact not deleted')
    }
  }, [isSuccess, isError])

  
  const openModal = (id) => {
    setcontactId(id)
    setisOpen(true)
  }

  const closeModal = (e) => {
    setisOpen(false)
  }

  

  return (
    <>
    {
     isOpen &&  
     <CustomModal  setisOpen={setisOpen}>
       <div  style={{width:'250px'}} className='rounded-md'>
       <span style={{cursor:'pointer' , background:`${theme == 'light' ? 'black' : 'white'}`, width:'30px', height:'30px', borderRadius:'50%', padding:'5px'}} onClick={closeModal}> <img src={closeicon} alt="close"  style={{objectFit:'contain' , width:"20px", height:'20px'}} /></span>
       <h2 style={{margin:'40px'}}>{locale == 'uz' ? 'O`chirishga ishonchingiz komilmi' : 'ты уверен удалить'}</h2>
 
         <div className='d-flex align-items-center justify-content-between mt-5'>
 
         <button onClick={() => handleDeleterev()} className='btn btn-danger'>{locale === 'uz' ? 'O`chirish' : 'Удалить'}</button>
           <button onClick={closeModal} className='btn btn-primary'>{locale === 'uz' ? 'Qoldirish' : 'отменить'}</button>
 
         </div>
       </div>
     </CustomModal>
    }
     
       <h1 className='mb-5'>{locale === 'uz' ? 'Qayta Aloqa Royhati' :' список обратная связы'}</h1>
       <div className='table-responsive-sm table-responsive-md table-responsive-lg'>
         <table className='table  table-bordered table-striped'>
           <thead className='thead-dark'>
             <tr>
               <th scope='col'>#</th>
               <th scope='col'>{locale === 'uz' ? 'ISM' :'ИМЯ'}</th>
               <th scope='col'>{locale == 'uz' ? 'TELIFON RAQAMI' :'номер телефона'}</th>
               <th scope='col'>{locale == 'uz' ? 'YARATILGAN SANA' :'СОЗДАН В'}</th>
               <th scope='col'>{locale == 'uz' ? 'O`ZGARTIRILGAN SANA' : "ОБНОВЛЕНО В"}</th>
               <th scope='col'>{locale === 'uz' ? 'O`CHIRISH' : 'Удалить'}</th>
             </tr>
           </thead>
           <tbody>
             {data?.contacts?.map((item, index) => (
               <tr>
                 <th scope='row'>{index + 1}</th>
                 <td>{item?.name}</td>
                 <td>{item?.number}</td>
                 <td>{format(item?.createdAt)}</td>
                 <td>{format(item?.updatedAt)}</td>
                 <td style={{width: '70px'}} onClick={() => openModal(item?._id)}>
                 <span className='w-100 h-100 btn btn-danger'>{locale === 'uz' ? 'O`chirish' : 'Удалить'}</span>
                 </td>
               </tr>
             ))}
           </tbody>
         </table>
       </div>
     </>
  )
}

export default UserManagemant