/* eslint-disable jsx-a11y/anchor-is-valid */
import {FC, useEffect, useState} from 'react'
import {useIntl} from 'react-intl'
import {PageTitle} from '../../../_metronic/layout/core'
import { format } from 'timeago.js'
import { toast } from 'react-toastify'
import { useDeleteContactMutation, useGetContactsQuery } from '../../../redux/features/contact/contactApi'
import CustomModal from '../../../_metronic/partials/modals/CustomModal'
import {
  ResponsiveContainer,
  XAxis,
  YAxis,
  Tooltip,
  AreaChart,
  Area
} from 'recharts'
import { useGetAnalyticsQuery } from '../../../redux/features/analytics/analyticsApi'
import { useLang } from '../../../_metronic/i18n/Metronici18n'
import closeicon from '../../../_metronic/assets/close.png'


const DashboardPage: FC = () => {
  const locale = useLang()
  const { data , refetch} = useGetContactsQuery(undefined, {refetchOnMountOrArgChange:true})
  const theme = localStorage.getItem('kt_theme_mode_menu')
  const [isOpen, setisOpen] = useState(false)
  const [deleteContact, {isSuccess, isError }] = useDeleteContactMutation()
  const [contactId , setcontactId] = useState('')
  const { data:analyticsData } = useGetAnalyticsQuery(undefined)

  

  const handleDeleterev = async () => {
    await deleteContact(contactId)
    refetch()
    setisOpen(false)
  }

  useEffect(() => {
    if (isSuccess) {
      toast.success('contact deleted successfully')
    }

    if (isError) {
      toast.error('contact not deleted')
    }
  }, [isSuccess, isError])

  
  const openModal = (id) => {
    setcontactId(id)
    setisOpen(true)
  }

  const closeModal = (e) => {
    setisOpen(false)
  }

  const analytics:any = []

  

  analyticsData &&
  analyticsData?.analytics?.last12Months?.forEach((item:any) => {
    analytics.push({name:item.month, count:item.count})
  });

  

  return (
    <div>
    <div className="row" style={{height:'450px'}}>
      <h2>{locale === 'uz' ? 'Qayta Aloqa Statistikasi' :'Статистика отзывов'}</h2>
      <ResponsiveContainer width={'90%'} height={'50%' }>
                  <AreaChart 
                  data={analytics}
                  margin={{
                    top:20,
                    right:30,
                    left:0,
                    bottom:0
                  }}
                  > 
                    <XAxis dataKey={'name'}/>
                    <YAxis/>
                    <Tooltip/>
                    <Area
                      type="monotone"
                      dataKey={'count'}
                      stroke='#4d62d9'
                      fill='#4d62d9'
                    />
                  </AreaChart>
              </ResponsiveContainer>
    </div>

    
    <div className="row ">
    {
     isOpen &&  
     
     <CustomModal  setisOpen={setisOpen}>
       <div  style={{width:'250px'}} className='rounded-md'>
       <span style={{cursor:'pointer' , background:`${theme == 'light' ? 'black' : 'white'}`, width:'30px', height:'30px', borderRadius:'50%', padding:'5px'}} onClick={closeModal}> <img src={closeicon} alt="close"  style={{objectFit:'contain' , width:"20px", height:'20px'}} /></span>
         <h2 style={{margin:'40px'}}>{locale == 'uz' ? 'O`chirishga ishonchingiz komilmi' : 'ты уверен удалить'}</h2>
 
         <div className='d-flex align-items-center justify-content-between mt-5'>
 
           <button onClick={() => handleDeleterev()} className='btn btn-danger'>{locale === 'uz' ? 'O`chirish' : 'Удалить'}</button>
           <button onClick={closeModal} className='btn btn-primary'>{locale === 'uz' ? 'Qoldirish' : 'отменить'}</button>
 
         </div>
       </div>
     </CustomModal>
    }
     
       <h1 className='mb-5'>{locale === 'uz' ? 'Qayta Aloqa Ro`yhati ' : 'список обратный связи'}</h1>
       <div className='table-responsive-sm table-responsive-md table-responsive-lg'>
         <table className='table  table-bordered table-striped'>
           <thead className='thead-dark'>
             <tr>
               <th scope='col'>#</th>
               <th scope='col'>{locale === 'uz' ? 'ISM' :'ИМЯ'}</th>
               <th scope='col'>{locale == 'uz' ? 'TELIFON RAQAMI' :'номер телефона'}</th>
               <th scope='col'>{locale == 'uz' ? 'YARATILGAN SANA' :'СОЗДАН В'}</th>
               <th scope='col'>{locale == 'uz' ? 'O`ZGARTIRILGAN SANA' : "ОБНОВЛЕНО В"}</th>
               <th scope='col'>{locale === 'uz' ? 'O`CHIRISH' : 'Удалить'}</th>
             </tr>
           </thead>
           <tbody>
             {data?.contacts?.map((item, index) => (
               <tr>
                 <th scope='row'>{index + 1}</th>
                 <td>{item?.name}</td>
                 <td>{item?.number}</td>
                 <td>{format(item?.createdAt)}</td>
                 <td>{format(item?.updatedAt)}</td>
                 <td style={{width: '70px'}} onClick={() => openModal(item?._id)}>
                   <span className='w-100 h-100 btn btn-danger'>{locale === 'uz' ? 'O`chirish' : 'Удалить'}</span>
                 </td>
               </tr>
             ))}
           </tbody>
         </table>
       </div>
    </div>
    
    </div>
  )
}

const DashboardWrapper: FC = () => {
  const intl = useIntl()




  return (
    <>
      <PageTitle breadcrumbs={[]}>{intl.formatMessage({id: 'MENU.DASHBOARD'})}</PageTitle>
      <DashboardPage />
    </>
  )
}

export {DashboardWrapper}
