import React, {FC} from 'react'
import {PageTitle} from '../../../_metronic/layout/core'
import {BuilderPage} from './BuilderPage'
import { useLang } from '../../../_metronic/i18n/Metronici18n'

const BuilderPageWrapper: FC = () => {
  const locale = useLang()

  return (
    <>
      <PageTitle breadcrumbs={[]}>{locale === 'uz' ? 'maket dizayni o1zgartirish' : "конструктор макетов"}</PageTitle>
      <BuilderPage />
    </>
  )
}

export default BuilderPageWrapper
