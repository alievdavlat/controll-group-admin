import {lazy, FC, Suspense} from 'react'
import {Route, Routes, Navigate} from 'react-router-dom'
import {MasterLayout} from '../../_metronic/layout/MasterLayout'
import TopBarProgress from 'react-topbar-progress-indicator'
import {DashboardWrapper} from '../pages/dashboard/DashboardWrapper'
import {getCSSVariableValue} from '../../_metronic/assets/ts/_utils'
import {WithChildren} from '../../_metronic/helpers'
import BuilderPageWrapper from '../pages/layout-builder/BuilderPageWrapper'

const PrivateRoutes = () => {
  const AccountPage = lazy(() => import('../modules/accounts/AccountPage'))
  const HeroPage = lazy(() => import('../modules/customization/hero/HeroPage'))
  const AboutPage = lazy(() => import('../modules/customization/about/AboutPage'))
  const ContactFormPage = lazy(() => import('../modules/customization/contact-form/ContactFormPage'))
  const CreateAdvanatgesFormPage = lazy(() => import('../modules/creation/AdvnatagesPage'))
  const CreateFaqFormPage = lazy(() => import('../modules/creation/FaqPage'))
  const CreateReviewFormPage = lazy(() => import('../modules/creation/ReviewsPage'))
  const CreateServicesFormPage  = lazy(() => import('../modules/creation/ServicesPage'))
  const CreateStocksFormPage = lazy(() => import('../modules/creation/StocksCreatePage'))
  const CreateProjectFormPage = lazy(() => import('../modules/creation/Projects'))
  const VideoSection = lazy(() => import('../modules/customization/videoSection/VideoSectionEdit'))
  const MainData = lazy(() => import('../modules/customization/main-data/MainData'))

  const AdvantagesEditPage = lazy(() => import('../modules/edition/advantages/AdvantagesEditPage'))
  const FaqEditPage = lazy(() => import('../modules/edition/faq/FaqEditPage'))
  const ReviewseEditPage = lazy(() => import('../modules/edition/reviews/ReviewseEditPage'))
  const ServicesEditPage = lazy(() => import('../modules/edition/services/ServicesEditPage'))
  const StocksEditPage = lazy(() => import('../modules/edition/stocks/StocksEditPage'))
  const ProjectsEditPage = lazy(() => import('../modules/edition/projects/ProjectsEditPage'))


  const AdvantagesList = lazy(() => import('../modules/edition/advantages/AdvantageTable'))
  const ReviewsList = lazy(() => import('../modules/edition/reviews/ReviewListPage'))
  const FaqList = lazy(() => import('../modules/edition/faq/FaqList'))
  const ServicesList = lazy(() => import('../modules/edition/services/ServicesList'))
  const ContactsList = lazy(() => import('../modules/userManagement/UserManagemant'))
  const StocksList = lazy(() => import('../modules/edition/stocks/StocksListPage'))
  const ProjectLists = lazy(() => import('../modules/edition/projects/ProjectsListPage'))

  return (
    <Routes>
      <Route element={<MasterLayout />}>

          {/* pages   */}
        <Route path='auth/*' element={<Navigate to='/dashboard' />} />
        <Route path='dashboard' element={<DashboardWrapper />} />
        <Route path='builder' element={<BuilderPageWrapper />} />
     
      

        {/* account route */}
        <Route
          path='crafted/account/*'
          element={
            <SuspensedView>
              <AccountPage />
            </SuspensedView>
          }
        />

          {/* customization routes   */}
        <Route
        path='/dashboard/hero'
        element={
          <SuspensedView>
            <HeroPage/>
          </SuspensedView>
        }
        />
     <Route
        path='/dashboard/main-data'
        element={
          <SuspensedView>
            <MainData/>
          </SuspensedView>
        }
        />

      <Route
        path='/dashboard/about'
        element={
          <SuspensedView>
            <AboutPage/>
          </SuspensedView>
        }
        />

      <Route
        path='/dashboard/contact/edit'
        element={
          <SuspensedView>
            <ContactFormPage/>
          </SuspensedView>
        }
        />

      <Route
          path='/dashboard/video'
          element={
            <SuspensedView>
              <VideoSection/>
            </SuspensedView>
          }
          />
        {/* creation routes  */}

      <Route
        path='/dashboard/advantages'
        element={
          <SuspensedView>
            <CreateAdvanatgesFormPage/>
          </SuspensedView>
        }
        />

    <Route
        path='/dashboard/projects'
        element={
          <SuspensedView>
            <CreateProjectFormPage/>
          </SuspensedView>
        }
        />

      <Route
        path='/dashboard/faq'
        element={
          <SuspensedView>
            <CreateFaqFormPage/>
          </SuspensedView>
        }
        />

      <Route
        path='/dashboard/reviews'
        element={
          <SuspensedView>
            <CreateReviewFormPage/>
          </SuspensedView>
        }
        />

      <Route
        path='/dashboard/services'
        element={
          <SuspensedView>
            <CreateServicesFormPage/>
          </SuspensedView>
        }
        />

        <Route
          path='/dashboard/stocks'
          element={
            <SuspensedView>
              <CreateStocksFormPage/>
            </SuspensedView>
          }
        />


  {/* tables  */}
        
      <Route
        path='/dashboard/stocks-list'
        element={
          <SuspensedView>
            <StocksList/>
          </SuspensedView>
        }
        />

      <Route
        path='/dashboard/projects-list'
        element={
          <SuspensedView>
            <ProjectLists/>
          </SuspensedView>
        }
        />

      <Route
        path='/dashboard/advantages-list'
        element={
          <SuspensedView>
            <AdvantagesList/>
          </SuspensedView>
        }
        />
     <Route
        path='/dashboard/reviews-list'
        element={
          <SuspensedView>
            <ReviewsList/>
          </SuspensedView>
        }
        />
         <Route
        path='/dashboard/faq-list'
        element={
          <SuspensedView>
            <FaqList/>
          </SuspensedView>
        }
        />

    <Route
        path='/dashboard/services-list'
        element={
          <SuspensedView>
            <ServicesList/>
          </SuspensedView>
        }
      />


    <Route
        path='/dashboard/contact-list'
        element={
          <SuspensedView>
            <ContactsList/>
          </SuspensedView>
        }
      />

  {/* edition routes  */}

    <Route
        path='/dashboard/edit-advantages/:id'
        element={
          <SuspensedView>
            <AdvantagesEditPage/>
          </SuspensedView>
        }
        />

      <Route
        path='/dashboard/edit-faq/:id'
        element={
          <SuspensedView>
            <FaqEditPage/>
          </SuspensedView>
        }
        />

      <Route
        path='/dashboard/edit-reviews/:id'
        element={
          <SuspensedView>
            <ReviewseEditPage/>
          </SuspensedView>
        }
        />

      <Route
        path='/dashboard/edit-services/:id'
        element={
          <SuspensedView>
            <ServicesEditPage/>
          </SuspensedView>
        }
        />

    <Route
        path='/dashboard/edit-stocks/:id'
        element={
          <SuspensedView>
            <StocksEditPage/>
          </SuspensedView>
        }
        />

    <Route
        path='/dashboard/edit-project/:id'
        element={
          <SuspensedView>
            <ProjectsEditPage/>
          </SuspensedView>
        }
        />
      {/* not foud page  */}

        <Route path='*' element={<Navigate to='/error/404' />} />
      </Route>
    </Routes>
  )
}

const SuspensedView: FC<WithChildren> = ({children}) => {
  const baseColor = getCSSVariableValue('--bs-primary')
  TopBarProgress.config({
    barColors: {
      '0': baseColor,
    },
    barThickness: 1,
    shadowBlur: 5,
  })
  return <Suspense fallback={<TopBarProgress />}>{children}</Suspense>
}

export {PrivateRoutes}
