import { apiSlice } from "../api/apiSlice";




export const advantagesApi = apiSlice.injectEndpoints({
  endpoints:(builder) => ({
    createAdvantages: builder.mutation({
      query: (data) => ({
        url:"create-advantages",
        method:"POST",
        body:data,
        headers:{
          token :JSON.parse( localStorage.getItem('token') as any)
        }
      })
    }),

    editAdvantages: builder.mutation({
      query: ({data, id}) => ({
        url:`/update-advantages/${id}`,
        method:"PUT",
        body:data,
        headers:{
          token :JSON.parse( localStorage.getItem('token') as any)
        }
      })
    }),

    deleteAdvantages: builder.mutation({
      query: (id) => ({
        url:`/advantages/${id}`,
        method:"DELETE",
        headers:{
          token :JSON.parse( localStorage.getItem('token') as any)
        }
      })
    }),

    getAllAdvantages: builder.query({   
      query: () => ({
        url:'advantages',
        method:'GET',
        headers:{
          token :JSON.parse( localStorage.getItem('token') as any)
        }
      }),
    }),

    getOneAdvantages: builder.query({   
      query: (id) => ({
        url:`advantages/${id}`,
        method:'GET',
        headers:{
          token :JSON.parse( localStorage.getItem('token') as any)
        }
      }),
    }),
  })
})


export const {useCreateAdvantagesMutation,useGetAllAdvantagesQuery, useGetOneAdvantagesQuery, useEditAdvantagesMutation, useDeleteAdvantagesMutation} = advantagesApi








