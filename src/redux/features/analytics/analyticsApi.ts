import { apiSlice } from "../api/apiSlice";




export const advantagesApi = apiSlice.injectEndpoints({
  endpoints:(builder) => ({
  

    getAnalytics: builder.query({   
      query: () => ({
        url:`analytics-contact`,
        method:'GET',
        headers:{
          token :JSON.parse( localStorage.getItem('token') as any)
        }
      }),
    }),
  })
})


export const {useGetAnalyticsQuery} = advantagesApi








