import { createApi, fetchBaseQuery  } from "@reduxjs/toolkit/query/react";

import { setUser } from "../auth/authSlice";
const baseUrl = process.env.REACT_APP_API_URL


export const apiSlice = createApi({
  reducerPath:'api',
  baseQuery:fetchBaseQuery({
    baseUrl,

  }),
  endpoints: (builder) => ({
   

      loadUser: builder.query({
        query: () => ({
          url:'get-profile',
          method:'GET',
          headers:{
            token :JSON.parse( localStorage.getItem('token') as any)
          }
        }),

        async onQueryStarted(arg, {queryFulfilled, dispatch}){
          try {
              const result = await queryFulfilled;
              
              dispatch(setUser(result?.data?.user))
          } catch (err) {
            console.log(err);
        }
        }

      }),



    
  }),
})



export const { useLoadUserQuery} =  apiSlice