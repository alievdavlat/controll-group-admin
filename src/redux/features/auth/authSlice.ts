import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  token : '',
  user:''  || localStorage.getItem('user')
}

const autSlice = createSlice({
  name:"auth",
  initialState,
  reducers:{
    userRegistration: (state, action) => {
      state.token = action.payload
      localStorage.setItem('token', JSON.stringify(action.payload))

    },
    userLoggedIn: (state, action) => {
      localStorage.setItem('token', JSON.stringify(action.payload))
      state.token = action?.payload;
    },
    setUser : (state, action) => {
      state.user = action.payload;
      localStorage.setItem('user', JSON.stringify(action.payload))
    },

    userLoggedOut: (state) => {
      state.token = '';
      state.user = '';
    },
    


  }
})


export const  { userRegistration, userLoggedIn, userLoggedOut, setUser } = autSlice.actions

export default autSlice.reducer