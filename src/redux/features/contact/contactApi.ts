import { apiSlice } from "../api/apiSlice";




export const contactApi = apiSlice.injectEndpoints({
  endpoints:(builder) => ({
    deleteContact: builder.mutation({
      query: (id) => ({
        url:`contact/${id}`,
        method:"DELETE",
        headers:{
          token :JSON.parse( localStorage.getItem('token') as any)
        }
      })
    }),


    getContacts: builder.query({   
      query: () => ({
        url:`contact`,
        method:'GET',
        headers:{
          token :JSON.parse( localStorage.getItem('token') as any)
        }
      }),
    }),
  })
})


export const {useDeleteContactMutation ,useGetContactsQuery } = contactApi








