import { apiSlice } from "../api/apiSlice";




export const faqApi = apiSlice.injectEndpoints({
  endpoints:(builder) => ({
    
    createFaq: builder.mutation({
      query: (data) => ({
        url:"faq",
        method:"POST",
        body:data,
        headers:{
          token :JSON.parse( localStorage.getItem('token') as any)
        }
      })
    }),
  editFaqs: builder.mutation({
      query: ({data, id}) => ({
        url:`faq/${id}`,
        method:"PUT",
        body:data,
        headers:{
          token :JSON.parse( localStorage.getItem('token') as any)
        }
      })
    }),

    deleteFaq: builder.mutation({
      query: (id) => ({
        url:`faq/${id}`,
        method:"DELETE",
        headers:{
          token :JSON.parse( localStorage.getItem('token') as any)
        }
      })
    }),

    getAllFaqs: builder.query({   
      query: () => ({
        url:'faqs',
        method:'GET',
        headers:{
          token :JSON.parse( localStorage.getItem('token') as any)
        }
      }),
    }),

    getOneFaq: builder.query({   
      query: (id) => ({
        url:`faq/${id}`,
        method:'GET',
        headers:{
          token :JSON.parse( localStorage.getItem('token') as any)
        }
      }),
    }),

  })
})


export const {useCreateFaqMutation, useDeleteFaqMutation, useEditFaqsMutation, useGetAllFaqsQuery, useGetOneFaqQuery} = faqApi








