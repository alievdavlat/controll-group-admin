import {apiSlice} from '../api/apiSlice'

export const layoutApi = apiSlice.injectEndpoints({
  endpoints: (builder) => ({
    getHeroLayoutByType: builder.query({
      query: () => ({
        url: 'layout/hero',
        method: 'GET',
        headers: {
          token: JSON.parse(localStorage.getItem('token') as any),
        },
      }),
    }),
    getMainDataLayoutByType: builder.query({
      query: () => ({
        url: 'layout/main',
        method: 'GET',
        headers: {
          token: JSON.parse(localStorage.getItem('token') as any),
        },
      }),
    }),
    getTitlesLayoutByType: builder.query({
      query: () => ({
        url: 'layout/titles',
        method: 'GET',
        headers: {
          token: JSON.parse(localStorage.getItem('token') as any),
        },
      }),
    }),
    getVideoLayoutByType: builder.query({
      query: () => ({
        url: 'layout/video',
        method: 'GET',
        headers: {
          token: JSON.parse(localStorage.getItem('token') as any),
        },
      }),
    }),
    getAboutData: builder.query({
      query: () => ({
        url: 'layout/about',
        method: 'GET',
        headers: {
          token: JSON.parse(localStorage.getItem('token') as any),
        },
      }),
    }),

    getContactData: builder.query({
      query: () => ({
        url: 'layout/contact',
        method: 'GET',
        headers: {
          token: JSON.parse(localStorage.getItem('token') as any),
        },
      }),
    }),

    editLayout: builder.mutation({
      query: ({data, type}) => ({
        url: `layout/${type}`,
        method: 'PUT',
        body: data,
        headers: {
          token: JSON.parse(localStorage.getItem('token') as any),
        },
      }),

      async onQueryStarted(arg, {queryFulfilled, dispatch}) {
        try {
          const result = await queryFulfilled
          console.log(result)
        } catch (err) {
          console.log(err)
        }
      },
    }),
    
    deleteLayout: builder.mutation({
      query: ({type}) => ({
        url: `layout/${type}`,
        method: 'DELETE',
        headers: {
          token: JSON.parse(localStorage.getItem('token') as any),
        },
      }),
    }),
  }),
})

export const {
  
  useGetHeroLayoutByTypeQuery,
  useGetAboutDataQuery,
  useGetContactDataQuery,
  useEditLayoutMutation,
  useGetVideoLayoutByTypeQuery,
  useDeleteLayoutMutation,
  useGetMainDataLayoutByTypeQuery,
  useGetTitlesLayoutByTypeQuery
} = layoutApi
