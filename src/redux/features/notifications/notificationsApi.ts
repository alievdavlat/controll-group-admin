import { apiSlice } from "../api/apiSlice";




export const noificationsApi = apiSlice.injectEndpoints({
  endpoints:(builder) => ({
    
  getAllNotifications:builder.query({
    query: () => ({
      url:"/notefications",
      method:"GET",
      headers:{
        token :JSON.parse( localStorage.getItem('token') as any)
      }
    })
  }),


  updateNotificationsStatus:builder.mutation({
    query: (id) => ({
      url:`notefications/${id}`,
      method:"PUT",
      headers:{
        token :JSON.parse( localStorage.getItem('token') as any)
      }
    })
  })

  })
})


export const { useGetAllNotificationsQuery,useUpdateNotificationsStatusMutation } = noificationsApi