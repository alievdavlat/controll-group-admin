import { apiSlice } from "../api/apiSlice";




export const projectApi = apiSlice.injectEndpoints({
  endpoints:(builder) => ({
    
  createProject: builder.mutation({
      query: (data) => ({
        url:"projects",
        method:"POST",
        body:data,
        headers:{
          token :JSON.parse( localStorage.getItem('token') as any)
        }
      })
    }),
  editProject: builder.mutation({
      query: ({data, id}) => ({
        url:`projects/${id}`,
        method:"PUT",
        body:data,
        headers:{
          token :JSON.parse( localStorage.getItem('token') as any)
        }
      })
    }),
    editProjectImage: builder.mutation({
      query: ({data, id}) => ({
        url:`update-projects-image/${id}`,
        method:"PUT",
        body:data,
        headers:{
          token :JSON.parse( localStorage.getItem('token') as any)
        }
      })
    }),

    deleteProject: builder.mutation({
      query: (id) => ({
        url:`projects/${id}`,
        method:"DELETE",
        headers:{
          token :JSON.parse( localStorage.getItem('token') as any)
        }
      })
    }),

    getAllProject: builder.query({   
      query: () => ({
        url:'projects',
        method:'GET',
        headers:{
          token :JSON.parse( localStorage.getItem('token') as any)
        }
      }),
    }),

    getOnePorject: builder.query({   
      query: (id) => ({
        url:`projects/${id}`,
        method:'GET',
        headers:{
          token :JSON.parse( localStorage.getItem('token') as any)
        }
      }),
    }),


  })
})


export const {useCreateProjectMutation,useEditProjectImageMutation, useDeleteProjectMutation, useEditProjectMutation, useGetAllProjectQuery, useGetOnePorjectQuery} = projectApi








