import { apiSlice } from "../api/apiSlice";




export const reviewApi = apiSlice.injectEndpoints({
  endpoints:(builder) => ({
    createReview: builder.mutation({
      query: (data) => ({
        url:"create-review",
        method:"POST",
        body:data,
        headers:{
          token :JSON.parse( localStorage.getItem('token') as any)
        }
      })
    }),
    editReview: builder.mutation({
      query: ({data, id}) => ({
        url:`/update-review/${id}`,
        method:"PUT",
        body:data,
        headers:{
          token :JSON.parse( localStorage.getItem('token') as any)
        }
      })
    }),
    deleteReview: builder.mutation({
      query: (id) => ({
        url:`delete-review/${id}`,
        method:"DELETE",
        headers:{
          token :JSON.parse( localStorage.getItem('token') as any)
        }
      })
    }),
    getAllReviews: builder.query({   
      query: () => ({
        url:'reviews',
        method:'GET',
        headers:{
          token :JSON.parse( localStorage.getItem('token') as any)
        }
      }),
    }),

    getOneReview: builder.query({   
      query: (id) => ({
        url:`reviews/${id}`,
        method:'GET',
        headers:{
          token :JSON.parse( localStorage.getItem('token') as any)
        }
      }),
    }),
   

   



  })
})


export const {useCreateReviewMutation, useDeleteReviewMutation, useEditReviewMutation, useGetAllReviewsQuery, useGetOneReviewQuery} = reviewApi








