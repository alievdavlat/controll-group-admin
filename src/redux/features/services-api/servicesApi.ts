import { apiSlice } from "../api/apiSlice";




export const servicesApi = apiSlice.injectEndpoints({
  endpoints:(builder) => ({
    
    createServices: builder.mutation({
      query: (data) => ({
        url:"create-service",
        method:"POST",
        body:data,
        headers:{
          token :JSON.parse( localStorage.getItem('token') as any)
        }
      })
    }),

    EditServicesInfo: builder.mutation({
      query: ({data , id}) => ({
        url:`update-services-info/${id}`,
        method:"PUT",
        body:data,
        headers:{
          token :JSON.parse( localStorage.getItem('token') as any)
        }
      })
    }),

    deleteServices: builder.mutation({
      query: (id) => ({
        url:`delete-service/${id}`,
        method:"DELETE",
        headers:{
          token :JSON.parse( localStorage.getItem('token') as any)
        }
      })
    }),

    editServicesImg: builder.mutation({
      query: ({data , id}) => ({
        url:`update-service-img/${id}`,
        method:"PUT",
        body:data,
        headers:{
          token :JSON.parse( localStorage.getItem('token') as any)
        }
      })
    }),

    getAllServices: builder.query({   
      query: () => ({
        url:'services',
        method:'GET',
        headers:{
          token :JSON.parse( localStorage.getItem('token') as any)
        }
      }),
    }),

    getOneServices: builder.query({   
      query: (id) => ({
        url:`service/${id}`,
        method:'GET',
        headers:{
          token :JSON.parse( localStorage.getItem('token') as any)
        }
      }),
    }),

  })
})


export const {useCreateServicesMutation, useDeleteServicesMutation, useEditServicesImgMutation, useEditServicesInfoMutation, useGetAllServicesQuery, useGetOneServicesQuery} = servicesApi








