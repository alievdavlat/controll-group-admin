import { apiSlice } from "../api/apiSlice";




export const reviewApi = apiSlice.injectEndpoints({
  endpoints:(builder) => ({
    createStocks: builder.mutation({
      query: (data) => ({
        url:"stocks",
        method:"POST",
        body:data,
        headers:{
          token :JSON.parse( localStorage.getItem('token') as any)
        }
      })
    }),
    editStock: builder.mutation({
      query: ({data, id}) => ({
        url:`stocks/${id}`,
        method:"PUT",
        body:data,
        headers:{
          token :JSON.parse( localStorage.getItem('token') as any)
        }
      })
    }),
    deleteStock: builder.mutation({
      query: (id) => ({
        url:`stocks/${id}`,
        method:"DELETE",
        headers:{
          token :JSON.parse( localStorage.getItem('token') as any)
        }
      })
    }),
    getAllStocks: builder.query({   
      query: () => ({
        url:'stocks',
        method:'GET',
        headers:{
          token :JSON.parse( localStorage.getItem('token') as any)
        }
      }),
    }),

    getOneStock: builder.query({   
      query: (id) => ({
        url:`stocks/${id}`,
        method:'GET',
        headers:{
          token :JSON.parse( localStorage.getItem('token') as any)
        }
      }),
    }),
   

   



  })
})


export const {useCreateStocksMutation, useDeleteStockMutation, useEditStockMutation, useGetAllStocksQuery, useGetOneStockQuery} = reviewApi








