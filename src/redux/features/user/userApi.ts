import { apiSlice } from "../api/apiSlice";


export const userApi = apiSlice.injectEndpoints({
  endpoints:(builder) => ({
    updateAvatar: builder.mutation({
      query: (data) => ({
         url:'update-avatar',
         method:'PUT',
         headers:{
          token :JSON.parse( localStorage.getItem('token') as any)
        },
         body:data,
      })
    }),

    editProfile: builder.mutation({
      query: ({name}) => ({
         url:'update-info',
         method:'PUT',
         headers:{
          token :JSON.parse( localStorage.getItem('token') as any)
        },
         body:{name},
      })
    }),

    editPhoneNumber: builder.mutation({
      query: ({oldNumber, newNumber}) => ({
         url:'update-number',
         method:'PUT',
         headers:{
          token :JSON.parse( localStorage.getItem('token') as any)
        },
         body:{oldNumber, newNumber},
      })
    }),




  })
})


export const {useUpdateAvatarMutation, useEditProfileMutation, useEditPhoneNumberMutation } = userApi